# 0.0.1

Released 2019-07-02

* Implemented basic hash-consing for `ValId`

# 0.0.0

Released 2019-07-01

* Initial release, basic skeleton of `rain` value system and graph
