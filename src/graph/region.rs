/*!
A region, containing scoped nodes, parameters and outputs.
*/
use super::{
    cons::{Cache, CacheBy},
    node::{Downgrade, Upgrade},
};
use crate::prettyprinter::DisplayValue;
use crate::util::AlwaysOk;
use crate::value::{
    data::ValueData,
    error::ValueError,
    eval::{EvalCtx, SubstituteToValId, SubstituteValue},
    typing::Typed,
    TypeId, ValId, ValueEnum,
};
use lazy_static::lazy_static;
use lazycell::AtomicLazyCell;
use rand::{self, Rng};
use smallvec::SmallVec;
use std::borrow::Cow;
use std::cmp::{Ordering, PartialOrd};
use std::convert::Infallible;
use std::fmt::{self, Display, Formatter};
use std::hash::{Hash, Hasher};
use std::ops::Deref;
use std::sync::{Arc, Weak};

/// A small vector of parameters
pub type ParamVec = SmallVec<[ValId; 2]>;
/// A small vector of parameter types
pub type ParamTyVec = SmallVec<[TypeId; 2]>;

/// A region into which nodes are scoped
#[derive(Debug, Clone)]
pub struct Region(Arc<RegionData>);

/// Create a new region with the given parameter types, and optionally parent
#[macro_export]
macro_rules! region {
    ( $( $x:expr ),*; parent = $parent:expr ) => {{
        use crate::graph::region::Region;
        use smallvec::smallvec;
        Region::new_in(smallvec![$($x.into()),*], $parent)
    }};
    ( $( $x:expr ),*) => {{
        use crate::graph::region::Region;
        use smallvec::smallvec;
        Region::new(smallvec![$($x.into()),*])
    }}
}

impl Region {
    /// Create a new, empty region
    pub fn empty() -> Region {
        Region::new(&[][..])
    }
    /// Create a new, empty region with a given parent
    pub fn empty_in(parent: WeakRegion) -> Region {
        Region::new_in(&[][..], parent)
    }
    /// Create a new region
    pub fn new<D>(param_types: D) -> Region
    where
        D: Into<ParamTyVec>,
    {
        Region::new_in(param_types, WeakRegion::default())
    }
    /// Create a new region with a given parent
    pub fn new_in<D>(param_types: D, parent: WeakRegion) -> Region
    where
        D: Into<ParamTyVec>,
    {
        RegionDesc {
            param_types: param_types.into(),
            parent,
        }
        .into()
    }
    /// Create a new region corresponding to a partial evaluation of this region.
    /// Return `None` if the parameters provided correspond to a complete evaluation
    /// of this region. Return an error in case of type mismatch. Place both substitution
    /// and new region parameters into the passed evaluation context.
    pub fn partial<I>(&self, args: I, ctx: &mut EvalCtx) -> Result<Option<Region>, ValueError>
    where
        I: Iterator<Item = ValId>,
    {
        match ctx.register_region(self, args)? {
            0 => Ok(None),
            n => {
                let params = self.params();
                let subparams = &params[(params.len() - n)..];
                let parent = self.parent.level(ctx.minimum_depth().saturating_sub(1));
                let param_types = subparams
                    .iter()
                    .map(|param| param.infer_ty().unwrap())
                    .collect();
                let region: Region = RegionDesc {
                    parent,
                    param_types,
                }
                .into();
                for (subparam, param) in subparams.iter().zip(region.params().iter()) {
                    //TODO: panic on error here?
                    ctx.register_substitute(subparam.clone(), param.clone())?;
                }
                Ok(Some(region))
            }
        }
    }
    /// Get the region containing this region at a given depth level. Returns null for depth level 0
    pub fn level(&self, level: usize) -> Option<Region> {
        if level == 0 {
            return None;
        }
        let mut acc = self.clone();
        while acc.depth > level {
            acc = acc.parent.upgrade()?;
        }
        Some(acc)
    }
}

impl Downgrade for Region {
    type Downgraded = WeakRegion;
    /// Downgrade a region to a weak reference
    #[inline]
    fn downgrade(&self) -> WeakRegion {
        WeakRegion(Arc::downgrade(&self.0))
    }
}

impl PartialEq for Region {
    fn eq(&self, other: &Region) -> bool {
        Arc::ptr_eq(&self.0, &other.0)
    }
}

impl Eq for Region {}

impl Hash for Region {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        Arc::as_ptr(&self.0).hash(hasher)
    }
}

impl PartialOrd for Region {
    fn partial_cmp(&self, other: &Region) -> Option<Ordering> {
        if self == other {
            return Some(Ordering::Equal);
        }
        let ordering = self.depth.cmp(&other.depth);
        let (deeper, shallower) = match ordering {
            Ordering::Equal => return None,
            Ordering::Greater => (self, other),
            Ordering::Less => (other, self),
        };
        let mut parent = deeper.parent.upgrade()?;
        while parent.depth > shallower.depth {
            parent = parent.parent.upgrade()?;
        }
        if &parent == shallower {
            Some(ordering.reverse())
        } else {
            None
        }
    }
}

impl PartialEq<WeakRegion> for Region {
    fn eq(&self, other: &WeakRegion) -> bool {
        if let Some(other) = other.upgrade() {
            other.eq(self)
        } else {
            false
        }
    }
}

impl PartialOrd<WeakRegion> for Region {
    fn partial_cmp(&self, other: &WeakRegion) -> Option<Ordering> {
        if other.is_null() {
            Some(Ordering::Less)
        } else {
            self.partial_cmp(&other.upgrade()?)
        }
    }
}

impl PartialEq<Region> for WeakRegion {
    fn eq(&self, other: &Region) -> bool {
        other.eq(self)
    }
}

impl PartialOrd<Region> for WeakRegion {
    fn partial_cmp(&self, other: &Region) -> Option<Ordering> {
        other.partial_cmp(self).map(Ordering::reverse)
    }
}

impl Deref for Region {
    type Target = RegionData;
    #[inline]
    fn deref(&self) -> &RegionData {
        self.0.deref()
    }
}

/// A weak handle on a region
#[derive(Debug, Clone, Default)]
pub struct WeakRegion(pub Weak<RegionData>);

impl Upgrade for WeakRegion {
    type Upgraded = Region;
    /// Attempt to upgrade a region to a strong reference
    #[inline]
    fn upgrade(&self) -> Option<Region> {
        self.0.upgrade().map(Region)
    }
}

impl WeakRegion {
    /// Get the region containing this region at a given depth level. Returns null for depth level 0
    pub fn level(&self, level: usize) -> WeakRegion {
        if level == 0 {
            WeakRegion::default()
        } else if let Some(region) = self.upgrade() {
            region
                .level(level)
                .map(|region| region.downgrade())
                .unwrap_or(WeakRegion::default())
        } else {
            WeakRegion::default()
        }
    }
    /// Attempt to upgrade a region to a strong reference
    #[inline]
    pub fn upgrade(&self) -> Option<Region> {
        self.0.upgrade().map(Region)
    }
    /// Check whether a weak region is null
    #[inline]
    pub fn is_null(&self) -> bool {
        self == &WeakRegion::default()
    }
    /// Get the outer region of nested regions, if either is
    #[inline]
    pub fn outer<'a>(&'a self, other: &'a WeakRegion) -> Option<&'a WeakRegion> {
        self.partial_cmp(other).map(|ord| match ord {
            Ordering::Greater => self,
            _ => other,
        })
    }
    /// Get the inner region of nested regions, if either is
    #[inline]
    pub fn inner<'a>(&'a self, other: &'a WeakRegion) -> Option<&'a WeakRegion> {
        self.partial_cmp(other).map(|ord| match ord {
            Ordering::Less => self,
            _ => other,
        })
    }
    /// Get the innermost region of a list of nested regions, assuming all are comparable.
    /// Return an error if not.
    #[inline]
    pub fn innermost<'a, I>(
        &'a self,
        mut regions: I,
    ) -> Result<&'a WeakRegion, (&'a WeakRegion, &'a WeakRegion)>
    where
        I: Iterator<Item = &'a WeakRegion>,
    {
        let mut res = self;
        while let Some(region) = regions.next() {
            match res.partial_cmp(region) {
                None => return Err((res, region)),
                Some(Ordering::Less) => res = region,
                _ => {}
            }
        }
        Ok(res)
    }
    /// Quickly get the depth of this weak region. Return `0` if it is null or deallocated.
    #[inline]
    pub fn depth(&self) -> usize {
        self.upgrade().map(|u| u.depth).unwrap_or(0)
    }
}

impl PartialEq for WeakRegion {
    fn eq(&self, other: &WeakRegion) -> bool {
        Weak::ptr_eq(&self.0, &other.0)
    }
}

impl Eq for WeakRegion {}

impl Hash for WeakRegion {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.0.as_ptr().hash(hasher)
    }
}

impl PartialOrd for WeakRegion {
    fn partial_cmp(&self, other: &WeakRegion) -> Option<Ordering> {
        if self == other {
            Some(Ordering::Equal)
        } else if self.is_null() {
            Some(Ordering::Greater)
        } else if other.is_null() {
            Some(Ordering::Less)
        } else {
            let left = self.upgrade()?;
            let right = other.upgrade()?;
            let ordering = left.partial_cmp(&right);
            debug_assert_ne!(ordering, Some(Ordering::Equal));
            ordering
        }
    }
}

/// A region descriptor
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct RegionDesc {
    /// The desired region parent
    pub parent: WeakRegion,
    /// The desired region parameter types
    pub param_types: ParamTyVec,
}

impl RegionDesc {
    /// Create a new, uncached region from the given descriptor
    pub fn new_uncached(mut self: RegionDesc) -> Region {
        let depth = self.parent.upgrade().map(|u| u.depth).unwrap_or(0) + 1;
        let data = RegionData {
            depth,
            parent: self.parent,
            params: AtomicLazyCell::new(),
        };
        let region = Region(Arc::new(data));
        let mut params: ParamVec = SmallVec::with_capacity(self.param_types.len());
        for (ix, ty) in self.param_types.iter_mut().enumerate() {
            let param = Parameter {
                ix,
                region: region.downgrade(),
                ty: ty.clone(),
            };
            params.push(param.into());
        }
        region.params.fill(params).unwrap();
        region
    }
}

impl PartialEq<RegionData> for RegionDesc {
    fn eq(&self, other: &RegionData) -> bool {
        for (param, ty) in other.params().iter().zip(self.param_types.iter()) {
            if &param.read().ty().expect("Impossible") != ty {
                return false;
            }
        }
        true
    }
}

impl PartialEq<RegionDesc> for RegionData {
    fn eq(&self, other: &RegionDesc) -> bool {
        other.eq(self)
    }
}

impl PartialEq<Region> for RegionDesc {
    fn eq(&self, other: &Region) -> bool {
        self.eq(other.deref())
    }
}

impl PartialEq<RegionDesc> for Region {
    fn eq(&self, other: &RegionDesc) -> bool {
        other.eq(self)
    }
}

lazy_static! {
    /// The region cache
    pub static ref REGION_CACHE: Cache<WeakRegion> = {
        let mut rng = rand::thread_rng();
        Cache::with_keys(rng.gen())
    };
}

impl CacheBy<RegionDesc> for WeakRegion {
    type CacheError = Infallible;
    #[inline]
    fn node_to_cache(key: RegionDesc) -> Result<Region, Infallible> {
        Ok(key.new_uncached())
    }
}

impl From<RegionDesc> for Region {
    #[inline]
    fn from(desc: RegionDesc) -> Region {
        REGION_CACHE.cached_desc(desc).aok()
    }
}

/// The data defining a region
#[derive(Debug)]
pub struct RegionData {
    /// The parent scope of this region
    pub parent: WeakRegion,
    /// The depth of this region
    pub depth: usize,
    /// The parameters of a region
    params: AtomicLazyCell<ParamVec>,
}

impl RegionData {
    /// Get the parameters of this region
    pub fn params(&self) -> &[ValId] {
        self.params.borrow().unwrap()
    }
}

/// A descriptor for a region parameter
#[derive(Debug, Clone, PartialEq)]
pub struct ParameterDesc {
    /// The type of this parameter
    pub ty: ValId,
}

/// A parameter for a region
#[derive(Debug, Clone, Hash)]
pub struct Parameter {
    /// The type of this parameter
    pub ty: TypeId,
    /// The region this parameter is associated to
    region: WeakRegion,
    /// The parameter index of this parameter
    ix: usize,
}

impl From<Parameter> for ValueEnum {
    fn from(param: Parameter) -> ValueEnum {
        ValueEnum::Parameter(param)
    }
}
impl From<Parameter> for ValueData {
    fn from(param: Parameter) -> ValueData {
        ValueData::new(param)
    }
}
impl From<Parameter> for ValId {
    fn from(param: Parameter) -> ValId {
        ValId::try_new(ValueData::from(param)).expect("Impossible")
    }
}

impl DisplayValue for Parameter {}

impl Display for Parameter {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "TODO")
    }
}

impl Parameter {
    /// Get the parameter index of this parameter
    pub fn ix(&self) -> usize {
        self.ix
    }
}

impl HasRegion for Parameter {
    type RegionError = Infallible;
    /// Get the region of this parameter
    #[inline]
    fn region(&self) -> Result<Cow<WeakRegion>, Infallible> {
        Ok(Cow::Borrowed(&self.region))
    }
}

impl Typed for Parameter {
    type TypeError = Infallible;
    type InferenceError = Infallible;
    #[inline]
    fn is_ty(&self) -> bool {
        true //TODO: this
    }
    #[inline]
    fn infer_ty(&self) -> Result<TypeId, Infallible> {
        self.ty()
    }
    #[inline]
    fn ty(&self) -> Result<TypeId, Infallible> {
        Ok(self.ty.clone().into())
    }
}

impl PartialEq for Parameter {
    fn eq(&self, other: &Parameter) -> bool {
        self.region == other.region && self.ix == other.ix
    }
}

impl Eq for Parameter {}

impl SubstituteValue<Parameter> for Parameter {
    fn substitute<F>(&self, _f: F) -> Result<Parameter, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        Ok(self.clone())
    }
}

impl SubstituteToValId for Parameter {}

/// A value which has a region
pub trait HasRegion {
    /// An error which is possible in assigning this value a region
    type RegionError;
    /// Get the region associated with this value
    fn region(&self) -> Result<Cow<WeakRegion>, Self::RegionError>;
}

/// Get a value's region by its dependencies
#[macro_export]
macro_rules! region_by_dependencies {
    ($t:ty) => {
        impl crate::graph::region::HasRegion for $t {
            type RegionError = crate::graph::error::IncomparableRegions;
            fn region(
                &self,
            ) -> Result<std::borrow::Cow<crate::graph::region::WeakRegion>, Self::RegionError> {
                use crate::graph::region::WeakRegion;
                use std::borrow::Cow::*;
                use std::cmp::Ordering;
                let mut acc = WeakRegion::default();
                for dependency in self.dependencies().map(|dep| dep.read()) {
                    let region = dependency.region()?;
                    let ord = acc.partial_cmp(region.deref());
                    match ord {
                        None => {
                            return Err(crate::graph::error::IncomparableRegions(SmallVec::from([
                                region.clone().into_owned(),
                                region.into_owned(),
                            ]))
                            .into())
                        }
                        Some(Ordering::Greater) => acc = region.into_owned(),
                        _ => {}
                    }
                }
                return Ok(Owned(acc));
            }
        }
    };
}

/// Set a value's region to null
#[macro_export]
macro_rules! null_region {
    ($t: ty) => {
        impl crate::graph::region::HasRegion for $t {
            type RegionError = std::convert::Infallible;
            #[inline]
            fn region(&self) -> Result<std::borrow::Cow<crate::graph::region::WeakRegion>, std::convert::Infallible> {
                Ok(std::borrow::Cow::Owned(
                    crate::graph::region::WeakRegion::default(),
                ))
            }
        }
    };
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::value::primitive::logical::Bool;
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne};
    use smallvec::smallvec;

    #[test]
    fn nested_region_construction() {
        let region = Region::empty();
        let nested_region = Region::empty_in(region.downgrade());
        let other_region = Region::empty();
        let null = WeakRegion::default();
        assert_eq!(nested_region.parent, region.downgrade());
        assert_eq!(region.parent, WeakRegion::default());
        assert_eq!(region.depth, 1);
        assert_eq!(other_region.parent, WeakRegion::default());
        assert_eq!(other_region.depth, 1);
        assert_eq!(nested_region.depth, 2);
        assert_eq!(region, region);
        assert_eq!(nested_region, nested_region);
        assert_eq!(other_region, other_region);
        assert_eq!(region, other_region);
        assert_ne!(region, nested_region);
        assert_ne!(other_region, nested_region);
        assert_eq!(region.partial_cmp(&nested_region), Some(Ordering::Greater));
        assert_eq!(region.partial_cmp(&null), Some(Ordering::Less));
        assert_eq!(region.partial_cmp(&other_region), Some(Ordering::Equal));
        assert_eq!(nested_region.partial_cmp(&region), Some(Ordering::Less));
        assert_eq!(
            nested_region.partial_cmp(&other_region),
            Some(Ordering::Less)
        );
    }

    #[test]
    fn region_macro_test() {
        let region = region![Bool, Bool];
        assert_eq!(region.parent, WeakRegion::default());
        assert_eq!(region.params().len(), 2);
        let nested_region = region![Bool; parent=region.downgrade()];
        assert_eq!(nested_region.parent, region.downgrade());
        assert_eq!(nested_region.params().len(), 1);
    }
}
