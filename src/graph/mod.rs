/*!
The `rain` graph, and associated utilities (e.g. hash-consing, region management, etc.)
*/

pub mod cons;
pub mod error;
pub mod node;
pub mod region;
