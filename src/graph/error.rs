/*!
Rain graph errors
*/
use super::region::WeakRegion;
use smallvec::SmallVec;

/// The size of a small set of incomparable regions
const SMALL_INCOMPARABLE_REGIONS: usize = 2;

/// Incomparable region error
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct IncomparableRegions(pub SmallVec<[WeakRegion; SMALL_INCOMPARABLE_REGIONS]>);