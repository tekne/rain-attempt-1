/*!
Nodes of the `rain` graph
*/
use super::cons::CacheAcceptor;
use either::Either;
use parking_lot::RwLock;
use std::borrow::Cow;
use std::convert::Infallible;
use std::fmt::{self, Debug, Display, Formatter};
use std::hash::{Hash, Hasher};
use std::ops::Deref;
use std::sync::{Arc, Weak};

/// A node in a `rain` graph
#[derive(Debug)]
pub struct Node<T: ?Sized> {
    /// A reference-counted pointer to the data held in this node
    data: Arc<RwLock<T>>,
}

/// A strong reference which can be downgraded
pub trait Downgrade: From<<<Self as Downgrade>::Downgraded as Upgrade>::Upgraded> {
    /// The type of a downgraded, weak reference
    type Downgraded: Upgrade;
    /// Downgrade this reference to a weak reference
    fn downgrade(&self) -> Self::Downgraded;
}

impl<T> Downgrade for Arc<T> {
    type Downgraded = Weak<T>;
    #[inline]
    fn downgrade(&self) -> Weak<T> {
        Arc::downgrade(self)
    }
}

/// A weak reference which can be upgraded
pub trait Upgrade: From<<<Self as Upgrade>::Upgraded as Downgrade>::Downgraded> {
    /// The type of an upgraded, strong reference
    type Upgraded: Downgrade;
    /// Attempt to upgrade this reference to a strong reference
    fn upgrade(&self) -> Option<Self::Upgraded>;
}

impl<T> Upgrade for Weak<T> {
    type Upgraded = Arc<T>;
    #[inline]
    fn upgrade(&self) -> Option<Arc<T>> {
        self.upgrade()
    }
}

impl<T: ?Sized> Deref for Node<T> {
    type Target = RwLock<T>;
    #[inline]
    fn deref(&self) -> &RwLock<T> {
        &self.data
    }
}

impl<T: ?Sized> Downgrade for Node<T> {
    type Downgraded = WeakNode<T>;
    /// Downgrade this node to a weak handle
    fn downgrade(&self) -> WeakNode<T> {
        WeakNode {
            data: Arc::downgrade(&self.data),
        }
    }
}

impl<T: ?Sized> Clone for Node<T> {
    #[inline]
    fn clone(&self) -> Node<T> {
        Node {
            data: self.data.clone(),
        }
    }
}

impl<T: ?Sized> PartialEq for Node<T> {
    #[inline]
    fn eq(&self, other: &Node<T>) -> bool {
        Arc::ptr_eq(&self.data, &other.data)
    }
}

impl<T: ?Sized> Eq for Node<T> {}

impl<T: ?Sized> Hash for Node<T> {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        (self.data.deref() as *const RwLock<_>).hash(hasher)
    }
}

/// A node which has not yet been backlinked
#[derive(Debug, Clone)]
pub struct NotBacklinked<T>(Node<T>);

impl<T: NodeData> NotBacklinked<T> {
    /// Backlink a given node
    pub fn backlink(self) -> Result<Node<T>, T::Error> {
        {
            let mut data = self.0.data.write();
            data.backlink(Backlink(&self.0))?;
        }
        Ok(self.0)
    }
}

/// A backlink to a node
#[derive(Debug, Clone)]
pub struct Backlink<'a, T>(&'a Node<T>);

impl<T> Deref for Backlink<'_, T> {
    type Target = Node<T>;
    #[inline]
    fn deref(&self) -> &Node<T> {
        self.0
    }
}

/// A trait implemented by items which can be placed into a node, but may require a node backlink
pub trait NodeData: Sized {
    /// A possible error in placing an item into a node *or* deduplicating
    type Error;
    /// An acceptor for cache entries
    type CacheAcceptor: CacheAcceptor<WeakNode<Self>>;
    /// Backlink this data to the given node.
    /// **Warning:**
    /// *Any* attempt to read the data of `backlink` in this function will lead to *deadlock*!
    /// Backlinking should *not* create new nodes if dedup obtains a lock on the cache table!
    fn backlink(&mut self, _backlink: Backlink<Self>) -> Result<(), Self::Error> {
        Ok(())
    }
    /// Attempt to deduplicate this `NodeData`, either succeeding or returning a cache entry
    /// to place the newly created node in *should backlinking succeed*.
    fn dedup(&mut self) -> Either<Node<Self>, Self::CacheAcceptor>;
}

impl<T: NodeData> Node<T> {
    /// Create a new, non-backlinked node from given data, *without deduplication!*
    fn not_backlinked(data: T) -> NotBacklinked<T> {
        NotBacklinked(Node {
            data: Arc::new(RwLock::new(data)),
        })
    }
    /// Try to create a new node from given data, which *can* fail
    pub fn try_new(mut data: T) -> Result<Node<T>, T::Error> {
        match data.dedup() {
            Either::Left(node) => Ok(node),
            Either::Right(acceptor) => {
                let node = Node::not_backlinked(data).backlink()?;
                acceptor.accept(node.downgrade());
                Ok(node)
            }
        }
    }
}

impl<T: NodeData<Error = Infallible>> Node<T> {
    /// Try to create a new node from given data, which cannot fail
    pub fn new(data: T) -> Node<T> {
        Node::try_new(data).unwrap_or_else(|err| match err {})
    }
}

impl<T: Display> Display for Node<T> {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "{}", self.read().deref())
    }
}

/// A weak handle on a node in a `rain` graph
#[derive(Debug)]
pub struct WeakNode<T: ?Sized> {
    /// A weak reference-counted pointer to the data held in this node
    data: Weak<RwLock<T>>,
}

impl<T: ?Sized> Clone for WeakNode<T> {
    #[inline]
    fn clone(&self) -> WeakNode<T> {
        WeakNode {
            data: self.data.clone(),
        }
    }
}

impl<T> Default for WeakNode<T> {
    #[inline]
    fn default() -> WeakNode<T> {
        WeakNode {
            data: Weak::default(),
        }
    }
}

impl<T: ?Sized> Upgrade for WeakNode<T> {
    type Upgraded = Node<T>;
    /// Attempt to upgrade this node to a strong handle
    fn upgrade(&self) -> Option<Node<T>> {
        self.data.upgrade().map(|data| Node { data })
    }
}

impl<T: ?Sized> PartialEq for WeakNode<T> {
    fn eq(&self, other: &WeakNode<T>) -> bool {
        self.data.ptr_eq(&other.data)
    }
}

impl<T: ?Sized> Eq for WeakNode<T> {}

impl<T> Hash for WeakNode<T> {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.data.as_ptr().hash(hasher)
    }
}

impl<T> WeakNode<T> {
    /// Check whether this weak node is null
    pub fn is_null(&self) -> bool {
        self == &WeakNode::default()
    }
}

impl<T> WeakNode<T> {
    /// Create a new, empty weak node
    pub fn new() -> WeakNode<T> {
        WeakNode {
            data: Weak::default(),
        }
    }
    /// Check whether this weak node points to the same allocation as another
    /// Returns false if either pointer points to no allocation.
    pub fn alloc_eq(&self, other: &WeakNode<T>) -> bool {
        !self.data.ptr_eq(&Weak::new()) && self == other
    }
}

/// A trait implemented by values which have a weak node address associated with them
pub trait HasThis<T = Self> {
    /// Get a `Cow` of the weak node address associated with this value.
    /// May be `null` if there is none.
    fn this(&self) -> Cow<WeakNode<T>>;
}

impl<T> HasThis<T> for WeakNode<T> {
    fn this(&self) -> Cow<WeakNode<T>> {
        Cow::Borrowed(&self)
    }
}

/// A trait implemented by values which have a strong node address associated with them
pub trait HasAddr<T = Self> {
    /// Get a `Cow` of the strong node address associated with this value
    fn addr(&self) -> Cow<Node<T>>;
}

impl<T> HasAddr<T> for Node<T> {
    #[inline]
    fn addr(&self) -> Cow<Node<T>> {
        Cow::Borrowed(&self)
    }
}

impl<T> HasThis<T> for Node<T> {
    #[inline]
    fn this(&self) -> Cow<WeakNode<T>> {
        Cow::Owned(self.downgrade())
    }
}

/// Transforms an iterator over `&Node`s into an iterator over `WeakNode`s
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct ToWeak<I>(pub I);

impl<'a, T: 'a, I> Iterator for ToWeak<I>
where
    I: Iterator<Item = &'a Node<T>>,
{
    type Item = WeakNode<T>;
    fn next(&mut self) -> Option<WeakNode<T>> {
        self.0.next().map(Node::downgrade)
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
    fn count(self) -> usize {
        self.0.count()
    }
    fn last(self) -> Option<WeakNode<T>> {
        self.0.last().map(Node::downgrade)
    }
    fn nth(&mut self, n: usize) -> Option<WeakNode<T>> {
        self.0.nth(n).map(Node::downgrade)
    }
}

impl<'a, T: 'a, I> ExactSizeIterator for ToWeak<I>
where
    I: ExactSizeIterator<Item = &'a Node<T>>,
{
    fn len(&self) -> usize {
        self.0.len()
    }
}

impl<'a, T: 'a, I> DoubleEndedIterator for ToWeak<I>
where
    I: DoubleEndedIterator<Item = &'a Node<T>>,
{
    fn next_back(&mut self) -> Option<WeakNode<T>> {
        self.0.next_back().map(Node::downgrade)
    }
}
