/*!
Hash-consing and associated utilities
*/
use super::node::{Downgrade, Upgrade, WeakNode};
use crate::util::PassThroughHasher;
use crate::value::judgement::JEq;
use ahash::AHasher;
use dashmap::{mapref::entry::Entry, DashMap};
use either::Either;
use std::convert::Infallible;
use std::hash::{BuildHasherDefault, Hash, Hasher};
use std::sync::{Arc, Weak};

/// A node cache type
pub trait CacheType<T> {
    /// The underlying map type for a given node cache type
    type UnderlyingMap;
}

/// A global node cache
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct GlobalCache;

impl<T> CacheType<T> for GlobalCache {
    type UnderlyingMap = DashMap<u64, T, BuildHasherDefault<PassThroughHasher>>;
}

/// A cache
#[derive(Debug, Clone)]
pub struct Cache<T, C: CacheType<T> = GlobalCache> {
    /// The underlying map of this
    pub map: C::UnderlyingMap,
    /// The keys of this node cache, for constructing `AHasher` instances
    pub keys: (u64, u64),
}

/// A node cache
pub type NodeCache<T, C = GlobalCache> = Cache<WeakNode<T>, C>;

/// An entry in a cache
pub type CacheEntry<'a, T> = Entry<'a, u64, T, BuildHasherDefault<PassThroughHasher>>;

/// An entry in a node cache
pub type NodeCacheEntry<'a, T> = CacheEntry<'a, WeakNode<T>>;

/// An object which can accept a cache entry
pub trait CacheAcceptor<T> {
    /// Accept a cache entry
    fn accept(self, weak: T);
}

impl<'a, T> CacheAcceptor<T> for CacheEntry<'a, T>
where
    T: Default,
{
    fn accept(self, value: T) {
        *(self.or_default()) = value;
    }
}

impl<T> CacheAcceptor<T> for () {
    /// Discard the accepted value
    fn accept(self, _value: T) {}
}

impl<T> Cache<T>
where
    T: Upgrade + Default,
{
    /// Create a new, empty node cache with the given keys
    pub fn with_keys(keys: (u64, u64)) -> Cache<T> {
        Cache {
            map: DashMap::default(),
            keys,
        }
    }
    /// Create a new, empty node cache
    pub fn new() -> Cache<T> {
        Self::with_keys((0, 0))
    }
    /// Get either the cached value for a key (if it exists), or the entry for the key
    /// (if it does not). Note that this locks the bucket for the key up until the handle
    /// for the entry is destroyed, so be careful!
    pub fn cached_entry<'a, K>(&'a self, key: &K) -> Either<T::Upgraded, CacheEntry<'a, T>>
    where
        K: Hash + JEq<T::Upgraded>
    {
        let hash = {
            let mut hasher = AHasher::new_with_keys(self.keys.0, self.keys.1);
            key.hash(&mut hasher);
            hasher.finish()
        };
        let entry = self.map.entry(hash);
        if let Entry::Occupied(occupied) = &entry {
            if let Some(node) = occupied.get().upgrade() {
                if key.jeq(&node) {
                    return Either::Left(node);
                }
            }
        }
        Either::Right(entry)
    }
    /// Try to get the node cached for a given key, with a given node creation function.
    /// If the node has been collected or otherwise does not exist,
    /// try to create a new one and cache it. If this fails, return an error.
    pub fn cached_constructor<D, E, F>(&self, data: D, constructor: F) -> Result<T::Upgraded, E>
    where
        D: Hash + JEq<T::Upgraded>,
        F: FnOnce(D) -> Result<T::Upgraded, E>,
    {
        let entry = match self.cached_entry(&data) {
            Either::Left(node) => return Ok(node),
            Either::Right(entry) => entry,
        };
        let node = constructor(data)?;
        *(entry.or_default()) = node.downgrade().into();
        Ok(node)
    }
    /// Remove a given cached value
    pub fn remove<K>(&self, key: &K) -> Option<(u64, T)>
    where
        K: Hash,
    {
        let mut hasher = AHasher::new_with_keys(self.keys.0, self.keys.1);
        key.hash(&mut hasher);
        let cache_key = hasher.finish();
        self.map.remove(&cache_key)
    }
    /// Try to get the node cached for a given descriptor.
    /// If the node has been collected or otherwise does not exist,
    /// try to create a new one and cache it. If this fails, return an error.
    #[inline]
    pub fn cached_desc<D>(&self, desc: D) -> Result<T::Upgraded, T::CacheError>
    where
        D: Hash + JEq<T::Upgraded>,
        T: CacheBy<D>,
    {
        self.cached_constructor(desc, T::node_to_cache)
    }
}

/// A node data-type which can be cached by a given key type
pub trait CacheBy<K: Hash>: Upgrade {
    /// Potential error when trying to cache a key type
    type CacheError;
    /// Try to get the node to cache for a given key
    fn node_to_cache(key: K) -> Result<Self::Upgraded, Self::CacheError>;
}

impl<T: Hash> CacheBy<T> for Weak<T> {
    type CacheError = Infallible;
    #[inline]
    fn node_to_cache(key: T) -> Result<Arc<T>, Infallible> {
        Ok(Arc::new(key))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::util::AlwaysOk;
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne};

    impl JEq<Arc<bool>> for bool {
        fn jeq(&self, a: &Arc<bool>) -> bool { self.eq(a) }
    }

    #[test]
    fn bools_cache_properly() {
        // Generate a new cache
        let cache = Cache::<Weak<bool>>::new();

        // Cache true, show the cached value is returned even if the generator always fails
        let nt = cache.cached_desc(true).aok();
        let pt = cache
            .cached_constructor(true, |_| Err(()))
            .expect("Valid key, should not generate");
        let ntp = Arc::as_ptr(&nt);
        let ptp = Arc::as_ptr(&pt);
        assert_eq!(ntp, ptp);

        // Show that caching false fails if the generator always fails
        cache
            .cached_constructor(false, |_| Err(()))
            .expect_err("Invalid key, generator always errors");

        // Now actually cache false
        let nf = cache.cached_desc(false).aok();
        let pf = cache
            .cached_constructor(false, |_| Err(()))
            .expect("Valid key, should not generate");
        let nfp = Arc::as_ptr(&nf);
        let pfp = Arc::as_ptr(&pf);
        assert_eq!(nfp, pfp);

        // Check newly cached values stay equal
        let nt2p = Arc::as_ptr(&cache.cached_desc(true).aok());
        let nf2p = Arc::as_ptr(&cache.cached_desc(false).aok());
        assert_eq!(ntp, nt2p);
        assert_eq!(nfp, nf2p);
        assert_ne!(ntp, nfp);
        let mntp = Arc::as_ptr(&Arc::new(true));
        let mnfp = Arc::as_ptr(&Arc::new(false));
        assert_ne!(ntp, mntp);
        assert_ne!(nfp, mntp);
        assert_ne!(ntp, mnfp);
        assert_ne!(nfp, mnfp);

        // Check removing true from the cache works
        let (_hash, ntr) = cache.remove(&true).expect("true was previously inserted");
        assert_eq!(ntr.upgrade().expect("We hold a reference to true"), nt);
        let nt3p = Arc::as_ptr(&cache.cached_desc(true).expect("Valid key"));
        let nf3p = Arc::as_ptr(&cache.cached_desc(false).expect("Valid key"));
        assert_ne!(ntp, nt3p);
        assert_eq!(nfp, nf3p);
    }
}
