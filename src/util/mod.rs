
/*!
Miscellaneous utilities
*/

use std::convert::Infallible;
use std::hash::Hasher;

pub mod symbol_table;

/// A type may generically hold errors but is guaranteed never to do so in a particular instance.
/// Stand-in for `#[feature(unwrap_infallible)]`
pub trait AlwaysOk<T> {
    /// Extract a value of a type from a generic error type statically guaranteed to be a non-error
    fn aok(self) -> T;
    /// Convert an infallible `Result` to a (potentially) fallible `Result`
    fn to_fall<E>(self) -> Result<T, E>
    where
        Self: Sized,
    {
        Ok(self.aok())
    }
}

impl<T> AlwaysOk<T> for Result<T, Infallible> {
    #[inline]
    fn aok(self) -> T {
        match self {
            Ok(ok) => ok,
            Err(e) => match e {},
        }
    }
}

/// A hash function which simply returns the last eight bytes of input.
#[derive(Debug, Copy, Clone, Default)]
pub struct PassThroughHasher {
    /// The starting value of the hasher
    pub starting_value: u64,
}

impl Hasher for PassThroughHasher {
    #[inline]
    fn finish(&self) -> u64 {
        self.starting_value
    }
    #[inline]
    fn write(&mut self, bytes: &[u8]) {
        let mut new_value = [0; 8];
        let mut i = 0;
        for byte in bytes.iter().rev() {
            if i >= 8 {
                break;
            }
            new_value[7 - i] = *byte;
            i += 1;
        }
        let old_value = self.starting_value.to_ne_bytes();
        for byte in old_value.iter().rev() {
            if i >= 8 {
                break;
            }
            new_value[7 - i] = *byte;
            i += 1;
        }
        self.starting_value = u64::from_ne_bytes(new_value);
    }
    #[inline]
    fn write_u64(&mut self, value: u64) {
        self.starting_value = value
    }
}

/// Implement `Debug` from a type's `Display` implementation
#[macro_export]
macro_rules! debug_from_display {
    ($d_ty:ty) => {
        impl std::fmt::Debug for $d_ty {
            fn fmt(&self, fmt: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
                <Self as std::fmt::Display>::fmt(self, fmt)
            }
        }
    };
}

#[cfg(test)]
mod tests {
    use super::*;
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne};
    use rand::Rng;
    use rand::SeedableRng;
    use rand_xoshiro::Xoshiro256StarStar;
    use std::hash::Hash;

    const RNG_SEED: u64 = 0xCAFEDECAF2832;

    #[test]
    fn pass_through_hasher_works() {
        let mut hasher = PassThroughHasher::default();
        assert_eq!(hasher.finish(), 0);
        hasher.write(&[1, 2, 3, 4]);
        assert_eq!(
            hasher.finish(),
            u64::from_ne_bytes([0, 0, 0, 0, 1, 2, 3, 4])
        );
        hasher.write(&[5, 6, 7, 8]);
        assert_eq!(
            hasher.finish(),
            u64::from_ne_bytes([1, 2, 3, 4, 5, 6, 7, 8])
        );

        let mut rng = Xoshiro256StarStar::seed_from_u64(RNG_SEED);
        for _ in 0..100 {
            let val = rng.gen();
            hasher.write_u64(val);
            assert_eq!(hasher.finish(), val);
            hasher.write(&val.to_ne_bytes());
            assert_eq!(hasher.finish(), val);
        }

        let bigint = u128::from_ne_bytes([
            0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF,
        ]);
        let last_half = u64::from_ne_bytes([0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF]);
        bigint.hash(&mut hasher);
        assert_eq!(hasher.finish(), last_half);
    }
}