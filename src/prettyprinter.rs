/*!
Pretty-printer for `rain` values.
*/
use crate::for_value_enum;
use crate::graph::region::{HasRegion, Region};
use crate::util::{symbol_table::SymbolTable, AlwaysOk, PassThroughHasher};
use crate::value::{typing::Typed, ValId};
use std::convert::Infallible;
use std::default::Default;
use std::fmt::{self, Display, Formatter};
use std::hash::BuildHasherDefault;

/// The default amount of maximum tabs for a prettyprinter
pub const DEFAULT_MAX_TABS: usize = 4;
/// The default starting index for a prettyprinter
pub const DEFAULT_STARTING_INDEX: usize = 0;

impl<N> Default for PrettyPrinter<N>
where
    N: From<usize> + Display,
{
    fn default() -> PrettyPrinter<N> {
        PrettyPrinter::new(DEFAULT_MAX_TABS, DEFAULT_STARTING_INDEX)
    }
}

/// A pretty-printer for `rain` values
#[derive(Debug, Clone)]
pub struct PrettyPrinter<N> {
    /// The current index for this pretty printer
    ix: usize,
    /// How many open scopes there are now
    tabs: usize,
    /// Whether each scope has been entered
    scopes: Vec<bool>,
    /// Value names
    pub names: SymbolTable<ValId, N, BuildHasherDefault<PassThroughHasher>>,
    /// The maximum tab-nesting level of this pretty printer
    pub max_tabs: usize,
}

/// An identifier for a `rain` value which either corresponds to an index or a name
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum PrinterId<N, I> {
    /// A named value
    Name(N),
    /// An indexed value
    Index(I),
}

impl<N, I> Display for PrinterId<N, I>
where
    N: Display,
    I: Display + std::fmt::LowerHex,
{
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        match self {
            PrinterId::Name(n) => n.fmt(fmt),
            PrinterId::Index(i) => write!(fmt, "%{:x}", i),
        }
    }
}

impl<N, I> From<usize> for PrinterId<N, I>
where
    usize: Into<I>,
{
    fn from(n: usize) -> PrinterId<N, I> {
        PrinterId::Index(n.into())
    }
}

/// A `rain` temporary register, written `%number`, e.g. `%123`
pub type TempReg = PrinterId<Infallible, usize>;

/// Display a value's components with a given prettyprinter
pub trait DisplayValue: Display {
    /// Display this value with a given recursive display function
    fn display_value<N>(
        &self,
        fmt: &mut Formatter,
        _printer: &mut PrettyPrinter<N>,
    ) -> Result<(), fmt::Error>
    where
        N: Display + From<usize>,
    {
        self.fmt(fmt)
    }
}

impl<N> PrettyPrinter<N>
where
    N: From<usize> + Display,
{
    /// Create a new pretty-printer with a given maximum tab count and a starting index
    pub fn new(max_tabs: usize, ix: usize) -> PrettyPrinter<N> {
        PrettyPrinter {
            ix,
            tabs: 0,
            scopes: Vec::new(),
            names: SymbolTable::default(),
            max_tabs,
        }
    }
    /// A helper to print tabs before a line, up to a maximum
    pub fn print_tabs(fmt: &mut Formatter, tabs: usize) -> Result<(), fmt::Error> {
        for _ in 0..tabs {
            write!(fmt, "\t")?
        }
        Ok(())
    }
    /// Check whether the current scope has been entered
    pub fn current_scope_entered(&self) -> bool {
        self.scopes.last().copied().unwrap_or(false)
    }
    /// Get the current number of tabs before a line
    pub fn get_tabs(&self) -> usize {
        self.tabs
    }
    /// Print current tabs before a line, up to a maximum
    pub fn print_curr_tabs(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        Self::print_tabs(fmt, self.tabs)
    }
    /// Increment the index counter. Get the old top value
    pub fn incr_ix(&mut self) -> usize {
        let old = self.ix;
        self.ix += 1;
        old
    }
    /// Enter the top scope, printing `{` as necessary
    pub fn enter_top_scope(&mut self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        let top = if let Some(top) = self.scopes.last_mut() {
            top
        } else {
            return Ok(());
        };
        let opened = *top;
        *top = true;
        if !opened {
            // Print opening brace for not-yet-opened non-base scope.
            write!(fmt, "{{\n")?;
            self.tabs += 1;
        }
        self.print_curr_tabs(fmt)?;
        Ok(())
    }
    /// Push a scope onto this pretty-printer
    pub fn push(&mut self) {
        self.scopes.push(false);
        self.names.push()
    }
    /// Pop a scope from this pretty printer. If it was open, close it.
    pub fn pop(&mut self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        self.names.pop();
        if let Some(top) = self.scopes.pop() {
            if top {
                // Print closing scope bracket
                self.tabs -= 1;
                write!(fmt, "\n")?;
                self.print_curr_tabs(fmt)?;
                write!(fmt, "}}\n")?;
            }
        }
        Ok(())
    }
    /// Pretty-print a ValId's level-dependencies, i.e. dependencies on the same or lower region than `level`. Open a scope, if necessary.
    /// On success, return how many new dependencies were named
    pub fn prettyprint_level_deps(
        &mut self,
        fmt: &mut Formatter,
        value: &ValId,
        level: usize,
    ) -> Result<usize, fmt::Error> {
        // Attempt a lookup for a name for the given value. If there is one, abort.
        if self.names.contains_key(value) {
            return Ok(0);
        }
        // Get a read-lock on the value
        let value_read = value.read();
        // Skip trivially printed values
        if value_read.trivially_printed() {
            return Ok(0);
        }
        // Print all dependencies *if their region is <= the current region*.
        // Note we check only depth, for simplicity, as the printing of malformed rain-graphs is undefined.
        let mut new_names = 0;
        for dependency in value_read.dependencies() {
            // First, filter on the dependency region
            let dep_read = dependency.read();
            if self.names.get(dependency).is_none() {
                // First, get a read lock on the dependency, and quickly check if it is trivially printed
                if dependency.read().trivially_printed() {
                    continue;
                }
                // Begin by naming all the dependencies of this type
                new_names += self.prettyprint_level_deps(fmt, dependency, level)?;
                // If this dependency has a type, pretty print all it's dependencies
                new_names += if let Ok(ty) = dependency.infer_ty() {
                    self.prettyprint_level_deps(fmt, &ty, level)?
                } else {
                    0
                };
                // If this dependency's level is too deep, skip naming it for now
                if dep_read.region().aok().depth() > level {
                    continue;
                }
                // Now, make a new name for the new dependency
                let new_name: N = self.incr_ix().into();
                // Print out the associated let-statement, after tabs. This also prints the value's dependencies, if any.
                new_names += self.print_let(fmt, new_name, dependency, true)?;
            }
        }
        Ok(new_names)
    }
    /// Pretty-print a ValId's level-dependencies, i.e. dependencies on the same or lower region than its level. Open a scope, if necessary.
    /// On success, return how many new dependencies were named
    pub fn prettyprint_deps(
        &mut self,
        fmt: &mut Formatter,
        value: &ValId,
    ) -> Result<usize, fmt::Error> {
        if self.names.contains_key(value) {
            return Ok(0);
        }
        let level = value.read().region().aok().depth();
        self.prettyprint_level_deps(fmt, value, level)
    }
    /// Deep pretty-print a ValId, using any names already defined, but defining no new dependencies at the same level
    pub fn prettyprint_deep(
        &mut self,
        fmt: &mut Formatter,
        value: &ValId,
    ) -> Result<(), fmt::Error> {
        if let Some(name) = self.names.get(value) {
            return write!(fmt, "{}", name);
        }
        for_value_enum!(value.read().value(), v => v.display_value(fmt, self))
    }
    /// Print a let-statement for a given `ValId`, deep-printing it's value and registering its new name.
    /// If `typed` is `true`, (normally, not deep!) print out the value's type if any is defined.
    /// Return any new dependencies printed, if any.
    pub fn print_let(
        &mut self,
        fmt: &mut Formatter,
        name: N,
        value: &ValId,
        typed: bool,
    ) -> Result<usize, fmt::Error> {
        let (mut count, ty) = if typed {
            if let Ok(ty) = value.infer_ty() {
                // Prettyprint type dependencies
                let count = self.prettyprint_deps(fmt, &ty)?;
                (count, Some(ty))
            } else {
                (0, None)
            }
        } else {
            (0, None)
        };
        // Prettyprint value dependencies
        count += self.prettyprint_deps(fmt, value)?;
        self.enter_top_scope(fmt)?;
        if let Some(ty) = ty {
            write!(fmt, "let {}: ", name)?;
            self.prettyprint_deep(fmt, &ty)?;
            write!(fmt, " = ")?;
        } else {
            write!(fmt, "let {} = ", name)?;
        }
        self.prettyprint_deep(fmt, value)?;
        write!(fmt, ";\n")?;
        self.names.def(value.clone(), name);
        Ok(count)
    }
    /// Pretty-print the parameters of a region, registering them.
    /// Pushes a new scope onto the stack
    pub fn prettyprint_params(
        &mut self,
        fmt: &mut Formatter,
        region: &Region,
    ) -> Result<usize, fmt::Error> {
        write!(fmt, "|")?;
        let mut first = true;
        let mut registered = 0;
        let params = region.params();
        for param in params.iter() {
            if let Ok(ty) = param.infer_ty() {
                registered += self.prettyprint_deps(fmt, &ty)?;
            }
        }
        self.push();
        for param in params.iter() {
            if !first {
                write!(fmt, ", ")?
            }
            first = false;
            if let Some(name) = self.names.get(param) {
                write!(fmt, "{}", name)?;
            } else {
                registered += 1;
                let new_name: N = self.incr_ix().into();
                write!(fmt, "{}", new_name)?;
                self.names.def(param.clone(), new_name);
            }

            if let Ok(ty) = param.infer_ty() {
                write!(fmt, ": ")?;
                self.prettyprint_deep(fmt, &ty)?;
            }
        }
        write!(fmt, "|")?;
        Ok(registered)
    }
}