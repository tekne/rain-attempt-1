/*!
Expression nodes
*/
use smallvec::{smallvec, SmallVec};
use std::convert::{TryFrom, TryInto};
use std::fmt::{self, Debug, Display, Formatter};
use std::ops::{Deref, DerefMut};
use std::slice::Iter;

use super::{
    error::ValueError,
    eval::{SubstituteToValId, SubstituteValue},
    primitive::Unit,
    typing::Typed,
    TypeId, ValId, ValueData, ValueEnum,
};
use crate::prettyprinter::{DisplayValue, PrettyPrinter, TempReg};
use crate::{debug_from_display, region_by_dependencies};

/// The size of a small S-expression
pub const SMALL_SEXPR_SIZE: usize = 3;

/// Potentially un-normalized S-expression arguments
#[derive(Clone, PartialEq, Eq, Hash)]
pub struct SexprArgs(pub SmallVec<[ValId; SMALL_SEXPR_SIZE]>);

region_by_dependencies!(Sexpr);

/// Create a set of S-expression arguments
#[macro_export]
macro_rules! sargs {
    ( $( $x:expr ),* ) => {
        SexprArgs(smallvec![ $( $x.into() ),* ])
    };
}

impl Deref for SexprArgs {
    type Target = SmallVec<[ValId; SMALL_SEXPR_SIZE]>;
    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for SexprArgs {
    #[inline]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl From<SmallVec<[ValId; SMALL_SEXPR_SIZE]>> for SexprArgs {
    #[inline]
    fn from(v: SmallVec<[ValId; SMALL_SEXPR_SIZE]>) -> SexprArgs {
        SexprArgs(v)
    }
}

impl SubstituteValue<SexprArgs> for SexprArgs {
    #[inline]
    fn substitute<F>(&self, mut f: F) -> Result<SexprArgs, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        let result: Result<_, _> = self.0.iter().map(|val| f(val)).collect();
        result.map(SexprArgs)
    }
}

impl SubstituteValue<Sexpr> for SexprArgs {
    #[inline]
    fn substitute<F>(&self, f: F) -> Result<Sexpr, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        let args: SexprArgs = self.substitute(f)?;
        args.normalize()
    }
}

impl SubstituteValue<Sexpr> for Sexpr {
    #[inline]
    fn substitute<F>(&self, f: F) -> Result<Sexpr, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        self.args.substitute(f)
    }
}

impl SubstituteToValId for SexprArgs {}

impl SubstituteToValId for Sexpr {}

impl SexprArgs {
    /// Normalize this S-expression
    #[inline]
    pub fn normalize(self) -> Result<Sexpr, ValueError> {
        Sexpr::build(self)
    }
    /// Get the dependencies of this argument set
    #[inline]
    pub fn dependencies(&self) -> Iter<ValId> {
        self.iter()
    }
}

debug_from_display!(SexprArgs);

impl TryFrom<SexprArgs> for Sexpr {
    type Error = ValueError;
    fn try_from(args: SexprArgs) -> Result<Sexpr, ValueError> {
        args.normalize()
    }
}
impl TryFrom<SexprArgs> for ValueEnum {
    type Error = ValueError;
    fn try_from(args: SexprArgs) -> Result<ValueEnum, ValueError> {
        let sexpr: Sexpr = args.try_into()?;
        Ok(sexpr.into())
    }
}
impl TryFrom<SexprArgs> for ValueData {
    type Error = ValueError;
    fn try_from(args: SexprArgs) -> Result<ValueData, ValueError> {
        let sexpr: Sexpr = args.try_into()?;
        Ok(sexpr.try_into()?)
    }
}
impl TryFrom<SexprArgs> for ValId {
    type Error = ValueError;
    fn try_from(args: SexprArgs) -> Result<ValId, ValueError> {
        let sexpr: Sexpr = args.try_into()?;
        sexpr.try_into()
    }
}

impl From<Sexpr> for ValueEnum {
    fn from(mut sexpr: Sexpr) -> ValueEnum {
        match sexpr.args.len() {
            0 => ().into(),
            1 => sexpr.args.swap_remove(0).into(), //TODO: think about this...
            _ => ValueEnum::Sexpr(sexpr)
        }
    }
}
impl TryFrom<Sexpr> for ValueData {
    type Error = ValueError;
    fn try_from(sexpr: Sexpr) -> Result<ValueData, ValueError> {
        ValueData::try_new(sexpr)
    }
}
impl TryFrom<Sexpr> for ValId {
    type Error = ValueError;
    fn try_from(mut sexpr: Sexpr) -> Result<ValId, ValueError> {
        match sexpr.args.len() {
            0 => Ok(().into()),
            1 => Ok(sexpr.args.swap_remove(0)),
            _ => ValId::try_new(ValueData::try_new(sexpr)?).map_err(|err| err.into())
        }
    }
}

impl Typed for Sexpr {
    type TypeError = ValueError;
    type InferenceError = ValueError;
    fn infer_ty(&self) -> Result<TypeId, ValueError> {
        match self.len() {
            0 => Ok(Unit.into()),
            1 => Ok(self[0].infer_ty()?),
            n => {
                let i = n - 1;
                self[i].apply_ty(self[..i].iter().rev())
            }
        }
    }
    fn ty(&self) -> Result<TypeId, ValueError> {
        match self.len() {
            0 => Ok(Unit.into()),
            1 => self[0].ty(),
            n => {
                let i = n - 1;
                self[i].apply_ty(self[..i].iter().rev())
            }
        }
    }
}

impl Display for SexprArgs {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        self.display_value(fmt, &mut PrettyPrinter::<TempReg>::default())
    }
}

impl DisplayValue for SexprArgs {
    fn display_value<N>(
        &self,
        fmt: &mut Formatter,
        printer: &mut PrettyPrinter<N>,
    ) -> Result<(), fmt::Error>
    where
        N: From<usize> + Display,
    {
        write!(fmt, "(")?;
        let mut first = true;
        for element in self.iter().rev() {
            if !first {
                write!(fmt, " ")?
            }
            first = false;
            printer.prettyprint_deep(fmt, element)?;
        }
        write!(fmt, ")")
    }
}

/// An S-expression node
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Sexpr {
    args: SexprArgs,
}

/// Create an S-expression, and evaluate it. Panic on failure.
#[macro_export]
macro_rules! sexpr {
    ( $( $x:expr ),*; expect = $msg:expr ) => {
        ValId::try_from(sargs!($( $x ),*)).expect($msg)
    };
    ( $( $x:expr ),*) => { sexpr!($( $x ),*; expect = "Impossible") }
}

impl Sexpr {
    /// Build a sexpr containing nothing, representing the unit type
    pub fn unit() -> Sexpr {
        Sexpr {
            args: SexprArgs(SmallVec::new()),
        }
    }
    /// Build a sexpr contaning a single node
    pub fn node<N: Into<ValId>>(node: N) -> Sexpr {
        Self::build_normalized(SexprArgs(smallvec![node.into()]))
    }
    /// Build an S-expression from a vector of arguments, asserting they are normalized.
    /// It is a logic error if they are not.
    pub fn build_normalized<V>(args: V) -> Sexpr
    where
        V: Into<SexprArgs>,
    {
        Sexpr { args: args.into() }
    }
    /// Build an S-expression from a vector of arguments
    pub fn build<V>(args: V) -> Result<Sexpr, ValueError>
    where
        V: Into<SexprArgs>,
    {
        let mut args: SexprArgs = args.into();
        args.try_normalize()?;
        Ok(Self::build_normalized(args))
    }
    /// Get the dependencies of this argument set
    #[inline]
    pub fn dependencies(&self) -> Iter<ValId> {
        self.args.dependencies()
    }
}

impl Display for Sexpr {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "{}", self.args)
    }
}

impl DisplayValue for Sexpr {
    fn display_value<N>(
        &self,
        fmt: &mut Formatter,
        printer: &mut PrettyPrinter<N>,
    ) -> Result<(), fmt::Error>
    where
        N: Display + From<usize>,
    {
        self.args.display_value(fmt, printer)
    }
}

impl Deref for Sexpr {
    type Target = SexprArgs;
    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.args
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::lambda;
    use crate::value::primitive::logical::{Binary::*, Bool, Unary::*, BINARY_OPS, UNARY_OPS};
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne};
    use smallvec::smallvec;
    #[test]
    fn fully_evaluated_binary_logical_operations_normalize() {
        for op in BINARY_OPS {
            for i in 0b00..=0b11 {
                let l = i & 0b01 != 0;
                let r = i & 0b11 != 0;
                let unnormalized = sargs!(r, l, *op);
                let mut sexpr = unnormalized.clone();
                match sexpr.try_normalize() {
                    Ok(_) => {}
                    Err(err) => panic!(
                        "Normalization error for {} [current state = {}]: {:#?}",
                        unnormalized, sexpr, err
                    ),
                }
                let sexpr = Sexpr::build_normalized(sexpr);
                let res = sargs!(op.apply(l, r));
                assert!(sexpr.args_jeq(&res), "Invalid result for {}", unnormalized)
            }
        }
    }
    #[test]
    fn partially_evaluated_binary_logical_operations_normalize() {
        for op in BINARY_OPS {
            for arg in &[true, false] {
                let unnormalized = sargs!(*arg, *op);
                let mut sexpr = unnormalized.clone();
                match sexpr.try_normalize() {
                    Ok(_) => {}
                    Err(err) => panic!(
                        "Normalization error for {} [current state = {}]: {:#?}",
                        unnormalized, sexpr, err
                    ),
                }
                let sexpr = Sexpr::build_normalized(sexpr);
                let res = sargs!(op.partial_apply(*arg));
                assert!(sexpr.args_jeq(&res), "Invalid result for {}", unnormalized);
            }
        }
    }
    #[test]
    fn unary_logical_operations_normalize() {
        for op in UNARY_OPS {
            for arg in &[true, false] {
                let unnormalized = sargs!(*arg, *op);
                let mut sexpr = unnormalized.clone();
                match sexpr.try_normalize() {
                    Ok(_) => {}
                    Err(err) => {
                        panic!(
                            "Normalization error for {} [current state = {}]: {:#?}",
                            unnormalized, sexpr, err
                        );
                    }
                }
                let sexpr = Sexpr::build_normalized(sexpr);
                let res = sargs!(op.apply(*arg));
                assert!(sexpr.args_jeq(&res), "Invalid result for {}", unnormalized);
            }
        }
    }
    #[test]
    fn identity_on_bools_normalizes() {
        let id = lambda![Bool => |x: &Region| x.params()[0].clone()];
        for arg in &[true, false] {
            let arg = ValId::from(*arg);
            assert_eq!(sexpr![arg.clone(), id.clone()], arg);
        }
    }
    #[test]
    fn const_bool_functions_normalize() {
        for c in &[true, false] {
            let cf = lambda![Bool => |_| *c];
            for arg in &[true, false] {
                let arg = ValId::from(*arg);
                let c = ValId::from(*c);
                assert_eq!(sexpr![arg, cf.clone()], c);
            }
        }
    }

    #[test]
    fn basic_sexpr_type_is_correct() {
        let n1 = lambda![value Bool, Bool => |x: &Region| sexpr![x.params()[0].clone(), Not]]
            .expect("Valid Lambda");
        assert_eq!(n1.result().ty().expect("Boolean type"), ValId::from(Bool));
    }

    #[test]
    fn basic_logical_expression_lambda_normalizes() {
        let mux = lambda![Bool, Bool, Bool => |x: &Region| {
            let params = x.params();
            let select = params[0].clone();
            let high = params[1].clone();
            let low = params[2].clone();
            sexpr![
                sexpr![low, sexpr![select.clone(), Not], And],
                sexpr![high, select.clone(), And],
                Or
            ]
        }];
        for select in &[true, false] {
            for high in &[true, false] {
                for low in &[true, false] {
                    let select_v = ValId::from(*select);
                    let high_v = ValId::from(*high);
                    let low_v = ValId::from(*low);
                    assert_eq!(
                        sexpr![low_v.clone(), high_v.clone(), select_v.clone(), mux.clone()],
                        if *select {
                            high_v.clone()
                        } else {
                            low_v.clone()
                        },
                        "Expected if {} {{{}}} else {{{}}} == {}",
                        select_v.read().value(),
                        high_v.read().value(),
                        low_v.read().value(),
                        if *select { high } else { low }
                    )
                }
            }
        }
    }
}
