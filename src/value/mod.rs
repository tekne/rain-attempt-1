/*!
The `rain` intermediate representation directed acyclic graph
*/
use std::borrow::Cow;
use std::cmp::Ordering;
use std::convert::{Infallible, TryFrom, TryInto};
use std::fmt::{self, Debug, Display, Formatter};
use std::hash::{Hash, Hasher};
use std::iter::Peekable;
use std::ops::Deref;

use crate::graph::{
    error::IncomparableRegions,
    node::{Downgrade, Node, Upgrade, WeakNode},
    region::{HasRegion, Parameter, WeakRegion},
};

use crate::prettyprinter::{DisplayValue, PrettyPrinter, TempReg};

pub mod primitive;
use primitive::{
    finite::{Finite, Index},
    logical::{self, Bool, LogicalOp, BOOL, FALSE, TRUE},
    Empty, Unit, EMPTY, UNIT,
};

pub mod expr;
use expr::Sexpr;

pub mod lambda;
use lambda::Lambda;

pub mod pi;
use pi::Pi;

pub mod sigma;

pub mod gamma;

pub mod phi;

pub mod record;

pub mod error;
use error::{NotAFunction, NotAType, NotEnoughInformation, ValueError};

pub mod judgement;
use judgement::JEq;

pub mod eval;
use eval::{SubstituteToValId, SubstituteValue};

pub mod cons;

pub mod parametrized;

pub mod universe;
use universe::Universe;

pub mod typing;
use typing::Typed;

pub mod data;
use data::ValueData;

pub mod sum;
use sum::Sum;

pub mod tuple;
use tuple::{Product, Tuple};

pub mod identity;

/// A `rain` value
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum ValueEnum {
    /// An S-expression
    Sexpr(Sexpr),
    /// Boolean values
    Bool(bool),
    /// Logical operations
    LogicalOp(LogicalOp),
    /// A lambda function
    Lambda(Lambda),
    /// A pi-type
    Pi(Pi),
    /// The boolean type
    BoolTy(Bool),
    /// A parameter to a region
    Parameter(Parameter),
    /// A typing universe
    Universe(Universe),
    /// A tuple of values
    Tuple(Tuple),
    /// A product type
    Product(Product),
    /// A sum type
    Sum(Sum),
    /// A finite type
    Finite(Finite),
    /// An element of a finite type
    Index(Index),
}

/// An iterator over the dependencies of a rain value
#[derive(Debug, Clone)]
pub enum Dependencies<'a> {
    /// A slice of dependencies
    Slice(std::slice::Iter<'a, ValId>),
}

impl<'a> Iterator for Dependencies<'a> {
    type Item = &'a ValId;
    fn next(&mut self) -> Option<&'a ValId> {
        match self {
            Dependencies::Slice(s) => s.next(),
        }
    }
}

impl ValueEnum {
    /// Check whether this value enum variant can potentially be applied
    pub fn applicable(&self) -> bool {
        use ValueEnum::*;
        match self {
            Sexpr(_) | LogicalOp(_) | Parameter(_) | Lambda(_) => true,
            _ => false,
        }
    }
    /// Check whether this value enum variant can be trivially printed
    pub fn trivially_printed(&self) -> bool {
        use ValueEnum::*;
        match self {
            Bool(_) | LogicalOp(_) | BoolTy(_) | Universe(_) => true,
            Sexpr(s) => s.len() == 0,   // Unit
            Tuple(t) => t.len() == 0,   // Unit
            Product(p) => p.len() == 0, // Unit
            Sum(s) => s.len() == 0,     // Empty
            _ => false,
        }
    }
    /// Print a value with a name
    pub fn name_print(
        &self,
        fmt: &mut fmt::Formatter,
        name: Option<&str>,
    ) -> Result<(), fmt::Error> {
        use ValueEnum::*;
        match (self, name) {
            (Parameter(parameter), Some(name)) => write!(fmt, "({} : {})", name, parameter.ty),
            (value, _) => write!(fmt, "{}", value),
        }
    }
    /// Get the canonical representation of the unit value
    pub fn unit() -> ValueEnum {
        ValueEnum::Tuple(Tuple::unit())
    }
    /// Get the canonical representation of the unit type
    pub fn unit_ty() -> ValueEnum {
        ValueEnum::Product(Product::unit_ty())
    }
    /// Get the canonical representation of the empty type
    pub fn empty_ty() -> ValueEnum {
        ValueEnum::Sum(Sum::empty_ty())
    }
}

/// Perform an operation on each variant of `ValueEnum`
#[macro_export]
macro_rules! for_value_enum {
    ($val:expr, $i:ident => $op:expr $(,$ps:path [$is:ident] => $ms:expr)*) => {{
        use crate::value::ValueEnum;
        #[allow(unreachable_patterns)]
        match $val {
            $($ps($is) => $ms,)*
            ValueEnum::Bool($i) => $op,
            ValueEnum::BoolTy($i) => $op,
            ValueEnum::LogicalOp($i) => $op,
            ValueEnum::Sexpr($i) => $op,
            ValueEnum::Lambda($i) => $op,
            ValueEnum::Pi($i) => $op,
            ValueEnum::Parameter($i) => $op,
            ValueEnum::Universe($i) => $op,
            ValueEnum::Tuple($i) => $op,
            ValueEnum::Product($i) => $op,
            ValueEnum::Sum($i) => $op,
            ValueEnum::Finite($i) => $op,
            ValueEnum::Index($i) => $op
        }
    }};
}

impl SubstituteValue<ValueEnum> for ValueEnum {
    /// Transform the values in this `ValueEnum` by a given function, returning another
    fn substitute<F>(&self, f: F) -> Result<ValueEnum, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        for_value_enum!(self, v => v.substitute(f))
    }
}

impl SubstituteValue<ValId> for ValueEnum {
    /// Transform the values in this `ValueEnum` by a given function, returning another
    fn substitute<F>(&self, f: F) -> Result<ValId, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        for_value_enum!(self, v => v.substitute(f))
    }
}

impl HasRegion for ValueEnum {
    type RegionError = IncomparableRegions;
    #[inline]
    fn region(&self) -> Result<Cow<WeakRegion>, IncomparableRegions> {
        for_value_enum!(self, v => v.region().map_err(|err| err.into()))
    }
}

impl Typed for ValueEnum {
    type TypeError = ValueError;
    type InferenceError = ValueError;
    #[inline]
    fn cheap_universe(&self) -> Result<Option<Universe>, ()> {
        for_value_enum!(self, v => v.cheap_universe())
    }
    #[inline]
    fn universe(&self) -> Option<Universe> {
        for_value_enum!(self, v => v.universe())
    }
    #[inline]
    fn is_ty(&self) -> bool {
        for_value_enum!(self, v => v.is_ty())
    }
    #[inline]
    fn infer_ty(&self) -> Result<TypeId, ValueError> {
        for_value_enum!(self, v => Ok(v.infer_ty()?))
    }
    #[inline]
    fn ty(&self) -> Result<TypeId, ValueError> {
        for_value_enum!(self, v => v.ty().map_err(|err| err.into()))
    }
    #[inline]
    fn cheap_apply_ty(&self) -> bool {
        for_value_enum!(self, v => v.cheap_apply_ty())
    }
    #[inline]
    fn apply_ty<'a, I>(&self, args: I) -> Result<TypeId, ValueError>
    where
        I: Iterator<Item = &'a ValId>,
    {
        for_value_enum!(self, v => v.apply_ty(args))
    }
}

/// A trait implemented by representations of `rain` values
pub trait ValueDesc {
    /// An error which can occur transforming this type into a node
    type ValueError;
    /// Try to make this type into a node
    fn to_node(self) -> Result<ValId, Self::ValueError>;
}

impl<E, T> ValueDesc for T
where
    T: TryInto<ValId, Error = E>,
{
    type ValueError = E;
    fn to_node(self) -> Result<ValId, E> {
        self.try_into()
    }
}

/// A trait implemented by `rain` values
pub trait Value:
    Sized + ValueDesc + JEq<ValueEnum> + JEq<Self> + JEq<ValId> + HasRegion + Typed
{
    /// Try to make this type into a value
    fn to_value(self) -> Result<ValueEnum, Self::ValueError>;
}

impl<T, E, V> Value for T
where
    T: TryInto<ValueEnum, Error = E>
        + ValueDesc<ValueError = V>
        + JEq<ValueEnum>
        + JEq<T>
        + JEq<ValId>
        + HasRegion
        + Typed,
    E: Into<V>,
{
    #[inline]
    fn to_value(self) -> Result<ValueEnum, V> {
        self.try_into().map_err(|err| err.into())
    }
}

impl TryFrom<ValueEnum> for ValId {
    type Error = ValueError;
    fn try_from(value: ValueEnum) -> Result<ValId, ValueError> {
        match value {
            ValueEnum::Sexpr(s) => s.to_node(),
            v => Node::try_new(ValueData::try_new(v)?).map_err(|err| err.into()),
        }
    }
}

/// A trait implmeneted by primitive `rain` values
pub trait PrimitiveValue: Value + Into<ValId> + Into<ValueEnum> + Clone {}

impl<P: PrimitiveValue> SubstituteValue<Self> for P {
    fn substitute<F>(&self, _f: F) -> Result<Self, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        Ok(self.clone())
    }
}

impl<P: PrimitiveValue> SubstituteToValId for P {}

macro_rules! primitive_value {
    ($p_ty:ty, $e_ty:expr, $n_ty:expr) => {
        impl From<$p_ty> for ValueEnum {
            #[inline]
            fn from(p: $p_ty) -> ValueEnum {
                $e_ty(p)
            }
        }
        impl From<$p_ty> for ValueData {
            #[inline]
            fn from(p: $p_ty) -> ValueData {
                ValueData::new(p)
            }
        }
        impl From<$p_ty> for ValId {
            #[inline]
            fn from(p: $p_ty) -> ValId {
                $n_ty(p)
            }
        }
        impl PrimitiveValue for $p_ty {}
    };
    ($p_ty:ty, $e_ty:expr) => {
        primitive_value!($p_ty, $e_ty, |p| ValId::try_construct(p)
            .expect("Impossible"));
    };
}

macro_rules! primitive_type {
    ($p_ty:ty, $e_ty:expr, $n_ty:expr) => {
        primitive_value!($p_ty, $e_ty, $n_ty);
        impl From<$p_ty> for TypeId {
            #[inline]
            fn from(p: $p_ty) -> TypeId {
                TypeId(ValId::from(p))
            }
        }
    };
    ($p_ty:ty, $e_ty:expr) => {
        primitive_value!($p_ty, $e_ty);
        impl From<$p_ty> for TypeId {
            #[inline]
            fn from(p: $p_ty) -> TypeId {
                TypeId(ValId::from(p))
            }
        }
    };
}

primitive_value!((), |_| ValueEnum::unit());
primitive_value!(bool, ValueEnum::Bool, |b| if b {
    TRUE.clone()
} else {
    FALSE.clone()
});
primitive_type!(Bool, ValueEnum::BoolTy, |_| BOOL.clone());
primitive_type!(Unit, |_| ValueEnum::unit_ty(), |_| UNIT.clone());
primitive_type!(Empty, |_| ValueEnum::empty_ty(), |_| EMPTY.clone());
primitive_value!(LogicalOp, ValueEnum::LogicalOp);
primitive_type!(Universe, ValueEnum::Universe);
primitive_value!(logical::Binary, |b| ValueEnum::LogicalOp(
    LogicalOp::Binary(b)
));
primitive_value!(logical::Unary, |u| ValueEnum::LogicalOp(LogicalOp::Unary(
    u
)));

impl Display for ValueEnum {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        self.display_value(fmt, &mut PrettyPrinter::<TempReg>::default())
    }
}

impl DisplayValue for ValueEnum {
    fn display_value<N>(
        &self,
        fmt: &mut Formatter,
        printer: &mut PrettyPrinter<N>,
    ) -> Result<(), fmt::Error>
    where
        N: From<usize> + Display,
    {
        for_value_enum!(self, v => v.display_value(fmt, printer))
    }
}

/// A value ID: a node pointing to value data
pub type ValId = Node<ValueData>;

impl AsRef<ValId> for ValId {
    #[inline]
    fn as_ref(&self) -> &ValId {
        self
    }
}

impl From<ValId> for ValueEnum {
    fn from(val: ValId) -> ValueEnum {
        val.read().value().clone()
    }
}

impl HasRegion for ValId {
    type RegionError = Infallible;
    fn region(&self) -> Result<Cow<WeakRegion>, Infallible> {
        Ok(Cow::Owned(self.read().region()?.into_owned()))
    }
}

/// A weak value ID: a weak node optionally pointing to value data
pub type WeakId = WeakNode<ValueData>;

/// A trait implemented by value descriptors known to describe types
pub trait TypeDesc: ValueDesc {
    /// Try to make this type into a type-node
    fn to_type(self) -> Result<TypeId, Self::ValueError>;
}

impl<T> TypeDesc for T
where
    T: TryInto<TypeId> + ValueDesc,
    T::Error: Into<T::ValueError>,
{
    #[inline]
    fn to_type(self) -> Result<TypeId, T::ValueError> {
        self.try_into().map_err(|err| err.into())
    }
}

/// A trait implemented by values which are always types
pub trait Type: Value + TypeDesc {
    /// Attempt to apply this type as a function to a given argument-type iterator.
    /// Application is curried. Non-type application yields a `NotAFunction` error.
    fn apply_to_value<'a, I>(&self, mut arg_tys: Peekable<I>) -> Result<TypeId, ValueError>
    where
        I: Iterator<Item = &'a ValId>,
        Self::ValueError: Into<ValueError>,
        Self: Clone,
    {
        if let Some(_next) = arg_tys.next() {
            Err(NotAFunction {
                applied: None,
                argument: None,
            }
            .into())
        } else {
            self.clone().to_type().map_err(|err| err.into())
        }
    }
}

/// A value ID known to correspond to a type
#[derive(Debug, Clone, Eq, PartialEq)]
#[repr(transparent)]
pub struct TypeId(ValId);

impl PartialOrd for TypeId {
    fn partial_cmp(&self, other: &TypeId) -> Option<Ordering> {
        if self == other {
            Some(Ordering::Equal)
        } else {
            //TODO: row types and such
            match (self.read().value(), other.read().value()) {
                (ValueEnum::Universe(lu), ValueEnum::Universe(ru)) => lu.partial_cmp(ru),
                _ => None,
            }
        }
    }
}

impl Hash for TypeId {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.0.hash(hasher)
    }
}

impl Display for TypeId {
    #[inline]
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "{}", self.0)
    }
}

impl PartialEq<ValId> for TypeId {
    #[inline]
    fn eq(&self, other: &ValId) -> bool {
        &self.0 == other
    }
}

impl PartialEq<TypeId> for ValId {
    #[inline]
    fn eq(&self, other: &TypeId) -> bool {
        other == self
    }
}

impl Deref for TypeId {
    type Target = ValId;
    #[inline]
    fn deref(&self) -> &ValId {
        &self.0
    }
}

impl AsRef<ValId> for TypeId {
    #[inline]
    fn as_ref(&self) -> &ValId {
        &self.0
    }
}

impl TryFrom<ValId> for TypeId {
    type Error = NotAType;
    fn try_from(val: ValId) -> Result<TypeId, NotAType> {
        if val.is_ty() {
            Ok(TypeId(val))
        } else {
            Err(NotAType(val))
        }
    }
}

impl From<TypeId> for ValId {
    #[inline]
    fn from(ty: TypeId) -> ValId {
        ty.0
    }
}

impl From<TypeId> for ValueEnum {
    #[inline]
    fn from(ty: TypeId) -> ValueEnum {
        ty.0.into()
    }
}

impl HasRegion for TypeId {
    type RegionError = Infallible;
    #[inline]
    fn region(&self) -> Result<Cow<WeakRegion>, Infallible> {
        self.0.region()
    }
}

impl Typed for TypeId {
    type TypeError = ValueError;
    type InferenceError = NotEnoughInformation;
    #[inline]
    fn is_ty(&self) -> bool {
        self.0.is_ty()
    }
    #[inline]
    fn universe(&self) -> Option<Universe> {
        self.0.universe()
    }
    #[inline]
    fn infer_ty(&self) -> Result<TypeId, NotEnoughInformation> {
        self.0.infer_ty()
    }
    #[inline]
    fn ty(&self) -> Result<TypeId, ValueError> {
        self.0.ty()
    }
}

impl Type for TypeId {
    fn apply_to_value<'a, I>(&self, mut arg_tys: Peekable<I>) -> Result<TypeId, ValueError>
    where
        I: Iterator<Item = &'a ValId>,
    {
        if arg_tys.peek().is_none() {
            return Ok(self.clone());
        }
        match self.read().value() {
            ValueEnum::Pi(p) => p.apply_to_value(arg_tys),
            ValueEnum::BoolTy(_) => Err(NotAFunction::default().into()),
            ValueEnum::Sum(_) => Err(NotAFunction::default().into()),
            ValueEnum::Sexpr(_s) => unimplemented!(),
            ValueEnum::Parameter(_p) => unimplemented!(),
            ValueEnum::Product(p) => p.apply_to_value(arg_tys),
            v => panic!("Internal error: value {} is not a valid type!", v),
        }
    }
}

impl Downgrade for TypeId {
    type Downgraded = WeakType;
    #[inline]
    fn downgrade(&self) -> WeakType {
        WeakType(self.0.downgrade())
    }
}

/// A weak value ID known to correspond to a type
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct WeakType(WeakId);

impl Deref for WeakType {
    type Target = WeakId;
    #[inline]
    fn deref(&self) -> &WeakId {
        &self.0
    }
}

impl Hash for WeakType {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.0.hash(hasher)
    }
}

impl Upgrade for WeakType {
    type Upgraded = TypeId;
    #[inline]
    fn upgrade(&self) -> Option<TypeId> {
        self.0.upgrade().map(TypeId)
    }
}
