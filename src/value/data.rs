/*!
`rain` value data
*/
use super::{
    cons::VALUE_CACHE,
    error::{MaybeMissing, MaybeMissingError, NotEnoughInformation, ValueError},
    typing::Typed,
    universe::Universe,
    Dependencies, Type, TypeId, ValId, Value, ValueEnum, WeakId,
};
use either::Either;
use smallvec::SmallVec;
use std::borrow::Cow;
use std::convert::{Infallible, TryFrom};
use std::fmt::{self, Debug, Display, Formatter};
use std::hash::{Hash, Hasher};
use std::ops::Deref;

use crate::graph::{
    cons::NodeCacheEntry,
    node::{Backlink, Downgrade, NodeData, WeakNode},
    region::{HasRegion, WeakRegion},
};
use crate::util::AlwaysOk;

/// The size of a small list of dependents
const SMALL_DEPENDENTS: usize = 2;

/// The data associated with a rain value
#[derive(Debug, Clone)]
pub struct ValueData {
    /// The value itself
    value: ValueEnum,
    /// The name associated with this value, if any
    pub name: Option<String>,
    /// The region this value is defined in
    region: WeakRegion,
    /// The type of this value, as currently inferred
    ty: Option<TypeId>,
    /// The values dependent on this value
    dependents: SmallVec<[WeakId; SMALL_DEPENDENTS]>,
    /*
    /// A self-link
    pub this: WeakId
    */
}

impl Deref for ValueData {
    type Target = ValueEnum;
    #[inline]
    fn deref(&self) -> &ValueEnum {
        &self.value
    }
}

impl ValueData {
    /// Try to create `ValueData` from a `Value` which may be invalid
    #[inline]
    pub fn try_new<V>(value: V) -> Result<ValueData, V::ValueError>
    where
        V: Value,
        V::ValueError: From<V::RegionError>
            + From<<V::InferenceError as MaybeMissingError<TypeId>>::MissingError>,
    {
        let region = value.region()?.into_owned();
        let ty = value.infer_ty().missing()?;
        let value = value.to_value()?;
        Ok(ValueData {
            value,
            ty,
            region,
            name: None,
            dependents: SmallVec::new(),
        })
    }
    /// Create `ValueData` from a `Value`
    pub fn new<V>(value: V) -> ValueData
    where
        V: Value<ValueError = Infallible> + HasRegion<RegionError = Infallible> + Typed,
        Infallible: From<<V::InferenceError as MaybeMissingError<TypeId>>::MissingError>,
    {
        Self::try_new(value).aok()
    }
    /// Get the value associated with this data
    #[inline]
    pub fn value(&self) -> &ValueEnum {
        &self.value
    }
    /// Get the region associated with this data
    #[inline]
    pub fn get_region(&self) -> &WeakRegion {
        &self.region
    }
    /// Add a dependent to a value
    #[inline]
    pub fn add_dependent(&mut self, dependent: WeakNode<Self>) {
        self.dependents.push(dependent)
    }
    /// Iterate over the direct dependencies for the given node
    pub fn dependencies(&self) -> Dependencies {
        match &self.value {
            ValueEnum::Sexpr(s) => Dependencies::Slice(s.dependencies()),
            ValueEnum::Lambda(l) => Dependencies::Slice(l.dependencies()),
            // ValueEnum::Pi(p) => Dependencies::Slice(p.dependencies()),
            _ => Dependencies::Slice([].iter()),
        }
    }
    /// Get the direct dependents of a given node
    pub fn dependents(&self) -> &[WeakId] {
        self.dependents.deref()
    }
    /// Get the type of this value, as inferred
    pub fn get_ty(&self) -> Option<&TypeId> {
        self.ty.as_ref()
    }
}

impl Typed for ValueData {
    type TypeError = ValueError;
    type InferenceError = NotEnoughInformation;
    #[inline]
    fn cheap_universe(&self) -> Result<Option<Universe>, ()> {
        self.value.cheap_universe()
    }
    #[inline]
    fn universe(&self) -> Option<Universe> {
        if let Ok(universe) = self.value.cheap_universe() {
            universe
        } else if let Some(ty) = &self.ty {
            ty.universe()?.enclosed()
        } else {
            None
        }
    }
    #[inline]
    fn is_ty(&self) -> bool {
        self.value.is_ty()
    }
    #[inline]
    fn infer_ty(&self) -> Result<TypeId, NotEnoughInformation> {
        if let Some(ty) = &self.ty {
            Ok(ty.clone())
        } else {
            Err(NotEnoughInformation)
        }
    }
    #[inline]
    fn ty(&self) -> Result<TypeId, ValueError> {
        if let Some(ty) = &self.ty {
            Ok(ty.clone())
        } else {
            self.value.ty()
        }
    }
    #[inline]
    fn apply_ty<'a, I>(&self, args: I) -> Result<TypeId, ValueError>
    where
        I: Iterator<Item = &'a ValId>,
    {
        if let Some(ty) = &self.ty {
            if !self.value.cheap_apply_ty() {
                return ty.apply_to_value(args.peekable());
            }
        }
        self.value.apply_ty(args)
    }
}

impl Typed for ValId {
    type TypeError = ValueError;
    type InferenceError = NotEnoughInformation;
    #[inline]
    fn cheap_universe(&self) -> Result<Option<Universe>, ()> {
        self.read().cheap_universe()
    }
    #[inline]
    fn universe(&self) -> Option<Universe> {
        self.read().universe()
    }
    #[inline]
    fn is_ty(&self) -> bool {
        self.read().is_ty()
    }
    #[inline]
    fn infer_ty(&self) -> Result<TypeId, NotEnoughInformation> {
        self.read().infer_ty()
    }
    #[inline]
    fn ty(&self) -> Result<TypeId, Self::TypeError> {
        let ty = {
            let read = self.read();
            if let Some(ty) = read.get_ty() {
                return Ok(ty.clone());
            }
            read.ty()?
        };
        if let Some(mut write) = self.try_write() {
            write.ty = Some(ty.clone());
        }
        Ok(ty)
    }
    #[inline]
    fn apply_ty<'a, I>(&self, args: I) -> Result<TypeId, ValueError>
    where
        I: Iterator<Item = &'a ValId>,
    {
        self.read().apply_ty(args)
    }
}

impl Display for ValueData {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        self.value
            .name_print(fmt, self.name.as_ref().map(|s| s.as_str()))
    }
}

impl TryFrom<ValueEnum> for ValueData {
    type Error = ValueError;
    fn try_from(value: ValueEnum) -> Result<ValueData, ValueError> {
        ValueData::try_new(value)
    }
}

impl PartialEq for ValueData {
    fn eq(&self, other: &ValueData) -> bool {
        /*self.alloc_eq(other) ||*/
        self.value == other.value
    }
}

impl Hash for ValueData {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.value.hash(hasher)
    }
}

impl HasRegion for ValueData {
    type RegionError = Infallible;
    fn region(&self) -> Result<Cow<WeakRegion>, Infallible> {
        Ok(Cow::Borrowed(&self.region))
    }
}

impl NodeData for ValueData {
    type Error = ValueError;
    type CacheAcceptor = NodeCacheEntry<'static, ValueData>;
    #[inline]
    fn backlink(&mut self, backlink: Backlink<ValueData>) -> Result<(), ValueError> {
        for dependency in self.dependencies() {
            let mut dependency = dependency.write();
            dependency.add_dependent(backlink.downgrade());
        }
        Ok(())
    }
    #[inline]
    fn dedup(&mut self) -> Either<ValId, NodeCacheEntry<'static, ValueData>> {
        VALUE_CACHE.deref().cached_entry(&self.value)
    }
}

impl ValId {
    /// Try to construct a *new* ValId from a `Value`.
    /// It's often more efficient to use `From`/`Into`, but they may be implemented in terms of this.
    pub fn try_construct<V>(value: V) -> Result<ValId, ValueError>
    where
        V: Value,
        V::ValueError: From<V::RegionError>
            + From<<V::InferenceError as MaybeMissingError<TypeId>>::MissingError>
            + Into<ValueError>,
    {
        ValId::try_new(ValueData::try_new(value).map_err(|err| -> ValueError { err.into() })?)
    }
    /// Register a value dependent on this value
    #[inline]
    pub fn add_dependent(&self, dependent: WeakId) {
        self.write().add_dependent(dependent)
    }
}
