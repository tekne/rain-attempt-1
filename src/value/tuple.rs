/*!
Tuples and associated types (finite Cartesian products)
*/
use super::{
    error::ValueError,
    eval::{SubstituteToValId, SubstituteValue},
    primitive::Unit,
    typing::Typed,
    universe::Universe,
    Type, TypeId, ValId, ValueData, ValueEnum,
    primitive::finite::{Finite, Index},
};
use crate::graph::error::IncomparableRegions;
use crate::prettyprinter::{DisplayValue, PrettyPrinter, TempReg};
use crate::region_by_dependencies;
use smallvec::SmallVec;
use std::convert::{Infallible, TryFrom, TryInto};
use std::default::Default;
use std::fmt::{self, Display, Formatter};
use std::iter::Peekable;
use std::ops::{Deref, DerefMut};
use std::slice::Iter;

/// The size of a small tuple, which will avoid allocating
pub const SMALL_TUPLE_SIZE: usize = 3;

/// A `SmallVec` of `ValId`s composing a tuple.
pub type TupleVec = SmallVec<[ValId; SMALL_TUPLE_SIZE]>;

/// Create a tuple of `rain` values
#[macro_export]
macro_rules! tuple {
    ($($val:expr),*) =>  {{
        use smallvec::smallvec;
        Tuple(smallvec![$($val.into()),*])
    }}
}

/// A tuple of `rain` values
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Tuple(pub TupleVec);

impl Tuple {
    /// Get the dependencies of this tuple
    pub fn dependencies(&self) -> Iter<ValId> {
        self.0.iter()
    }
    /// Get a value of the unit type
    pub fn unit() -> Tuple {
        Tuple(SmallVec::new())
    }
}

region_by_dependencies!(Tuple);

impl Deref for Tuple {
    type Target = TupleVec;
    #[inline]
    fn deref(&self) -> &TupleVec {
        &self.0
    }
}

impl DerefMut for Tuple {
    #[inline]
    fn deref_mut(&mut self) -> &mut TupleVec {
        &mut self.0
    }
}

impl SubstituteValue<Tuple> for Tuple {
    fn substitute<F>(&self, f: F) -> Result<Tuple, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        let new_elems: Result<_, _> = self.0.iter().map(f).collect();
        new_elems.map(Tuple)
    }
}

impl SubstituteToValId for Tuple {}

impl Typed for Tuple {
    type TypeError = ValueError;
    type InferenceError = ValueError;
    fn universe(&self) -> Option<Universe> {
        None
    }
    fn is_ty(&self) -> bool {
        false
    }
    fn infer_ty(&self) -> Result<TypeId, ValueError> {
        let component_tys: Result<_, _> = self.0.iter().map(|v| v.infer_ty()).collect();
        Ok(TypeId::try_from(Product(component_tys?))?)
    }
    fn ty(&self) -> Result<TypeId, ValueError> {
        let component_tys: Result<_, _> = self.0.iter().map(|v| v.ty()).collect();
        TypeId::try_from(Product(component_tys?)).map_err(|err| err.into())
    }
}

impl From<Tuple> for ValueEnum {
    fn from(t: Tuple) -> ValueEnum {
        if t.len() == 0 {
            ().into()
        } else {
            ValueEnum::Tuple(t)
        }
    }
}

impl TryFrom<Tuple> for ValId {
    type Error = ValueError;
    fn try_from(t: Tuple) -> Result<ValId, ValueError> {
        ValId::try_new(ValueData::try_new(t)?).map_err(|err| err.try_into().expect("Impossible"))
    }
}

impl DisplayValue for Tuple {
    fn display_value<N>(
        &self,
        fmt: &mut Formatter,
        printer: &mut PrettyPrinter<N>,
    ) -> Result<(), fmt::Error>
    where
        N: From<usize> + Display,
    {
        write!(fmt, "[")?;
        let mut first = true;
        for element in self.iter() {
            if !first {
                write!(fmt, " ")?
            }
            first = false;
            printer.prettyprint_deep(fmt, element)?;
        }
        write!(fmt, "]")
    }
}

impl Display for Tuple {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        self.display_value(fmt, &mut PrettyPrinter::<TempReg>::default())
    }
}

/// The size of a small product type, which will avoid allocating
pub const SMALL_PRODUCT_SIZE: usize = 3;

/// A `SmallVec` of `TypeId`s composing a product type
pub type ProductVec = SmallVec<[TypeId; SMALL_PRODUCT_SIZE]>;

/// A product type
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Product(pub ProductVec);

impl Product {
    /// Get the dependencies of this product
    pub fn dependencies(&self) -> Iter<TypeId> {
        self.0.iter()
    }
    /// Get the unit type
    pub fn unit_ty() -> Product {
        Product(SmallVec::new())
    }
}

region_by_dependencies!(Product);

impl Deref for Product {
    type Target = ProductVec;
    #[inline]
    fn deref(&self) -> &ProductVec {
        &self.0
    }
}

impl DerefMut for Product {
    #[inline]
    fn deref_mut(&mut self) -> &mut ProductVec {
        &mut self.0
    }
}

impl Typed for Product {
    type TypeError = Infallible;
    type InferenceError = Infallible;
    fn universe(&self) -> Option<Universe> {
        Universe::union_all(self.dependencies().map(|dep| dep.universe()))
            .map(|universe| universe.base_level())
            .unwrap_or(Universe::finite())
            .into()
    }
    fn is_ty(&self) -> bool {
        true
    }
    fn infer_ty(&self) -> Result<TypeId, Infallible> {
        Ok(self.universe().unwrap().into())
    }
    fn ty(&self) -> Result<TypeId, Infallible> {
        self.universe().unwrap().try_into()
    }
}

impl From<Product> for ValueEnum {
    fn from(product: Product) -> ValueEnum {
        if product.len() == 1 {
            Unit.into()
        } else {
            ValueEnum::Product(product)
        }
    }
}

impl TryFrom<Product> for TypeId {
    type Error = IncomparableRegions;
    fn try_from(product: Product) -> Result<TypeId, IncomparableRegions> {
        Ok(TypeId::try_from(ValId::try_from(product)?).expect("Product is always a type"))
    }
}

impl TryFrom<Product> for ValId {
    type Error = IncomparableRegions;
    fn try_from(product: Product) -> Result<ValId, IncomparableRegions> {
        ValId::try_new(ValueData::try_new(product)?)
            .map_err(|err| err.try_into().expect("Impossible"))
    }
}

impl SubstituteValue<Product> for Product {
    fn substitute<F>(&self, mut f: F) -> Result<Product, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        let new_elems: Result<_, _> = self
            .0
            .iter()
            .map(|t| Ok(TypeId::try_from(f(t)?)?))
            .collect();
        new_elems.map(Product)
    }
}

impl SubstituteToValId for Product {}

impl DisplayValue for Product {
    fn display_value<N>(
        &self,
        fmt: &mut Formatter,
        printer: &mut PrettyPrinter<N>,
    ) -> Result<(), fmt::Error>
    where
        N: Display + From<usize>,
    {
        if self.len() == 0 {
            return write!(fmt, "#unit");
        }
        write!(fmt, "(#product")?;
        for element in self.iter() {
            write!(fmt, " ")?;
            printer.prettyprint_deep(fmt, element)?;
        }
        write!(fmt, ")")
    }
}

impl Display for Product {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        self.display_value(fmt, &mut PrettyPrinter::<TempReg>::default())
    }
}

impl Type for Product {
    fn apply_to_value<'a, I>(&self, mut arg_tys: Peekable<I>) -> Result<TypeId, ValueError>
    where
        I: Iterator<Item = &'a ValId>,
    {
        let arg = if let Some(first) = arg_tys.next() {
            first
        } else {
            return Ok(self.clone().try_into()?);
        };
        let arg_read = arg.read();
        let fin_ty = Finite(self.len() as u64);
        let arg_ix = if arg_read.value() == &ValueEnum::unit() { // Special case: the unit type
            Index::unit()
        } else {
            match arg_read.value() {
                ValueEnum::Index(ix) => {
                    *ix
                },
                _ => {
                    let v_ty = arg.ty()?;
                    let fin_ty = TypeId::from(fin_ty);
                    if v_ty == fin_ty {
                        unimplemented!() // Valid index
                    } else {
                        unimplemented!() // Type error
                    }
                }
            }
        };
        if arg_ix.get_ty() != fin_ty { // Type error
            unimplemented!()
        }
        self[arg_ix.ix() as usize].apply_to_value(arg_tys) // Curry
    }
}
