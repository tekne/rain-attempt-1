/*!
Rain graph errors and error handling
*/

use super::{TypeId, ValId};
use crate::graph::error::IncomparableRegions;
use std::convert::{Infallible, TryFrom};

/// A `rain` value error
#[derive(Debug, Clone, Eq, PartialEq)]
#[non_exhaustive]
pub enum ValueError {
    /// A non-function value was applied in an S-expression
    NotAFunction(NotAFunction),
    /// A value was used as a type, but is not one
    NotAType(NotAType),
    /// A type mismatch between two values
    TypeMismatch(TypeMismatch),
    /// Incomparable regions
    IncomparableRegions(IncomparableRegions),
    /// Not enough information to determine the type of a value
    NotEnoughInformation(NotEnoughInformation),
    /// Not implemented
    NotImplementedError,
}

macro_rules! value_error_variant {
    ($p:path) => {
        impl From<Infallible> for $p {
            fn from(_err: Infallible) -> $p {
                match _err {}
            }
        }
        impl From<$p> for ValueError {
            fn from(err: $p) -> ValueError {
                use ValueError::*;
                $p(err)
            }
        }
        impl TryFrom<ValueError> for $p {
            type Error = ValueError;
            fn try_from(err: ValueError) -> Result<$p, ValueError> {
                use ValueError::*;
                match err {
                    $p(err) => Ok(err),
                    err => Err(err),
                }
            }
        }
    };
}

value_error_variant!(IncomparableRegions);
value_error_variant!(NotAFunction);
value_error_variant!(NotAType);
value_error_variant!(TypeMismatch);
value_error_variant!(NotEnoughInformation);

/// A non-function value was applied in an S-expression
#[derive(Debug, Clone, Default, Eq, PartialEq)]
pub struct NotAFunction {
    /// The value applied
    pub applied: Option<ValId>,
    /// The argument
    pub argument: Option<ValId>,
}

/// A value was used as a type, but is not one
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct NotAType(pub ValId);

impl From<Infallible> for ValueError {
    fn from(i: Infallible) -> ValueError {
        match i {}
    }
}

/// A type mismatch between two values
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct TypeMismatch {
    /// The value which had an incorrect type
    pub value: ValId,
    /// The expected type
    pub ty: TypeId,
    /// The context of the type check
    pub ctx: TypeMismatchCtx,
}

/// The context of a type mismatch
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum TypeMismatchCtx {
    /// Failed substitution for a parameter
    FailedSubstitution(ValId),
    /// Failed partial substitution check for a parameter
    FailedPartialSubstitution(ValId),
    /// Failed type application
    ApplyTy(ValId),
    /// A tuple index mismatch
    TupleIndexMismatch(usize),
    /// An error constructing an identity type
    IdentityConstruction(ValId)
}

/// There was not enough information to determine the type of a value
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct NotEnoughInformation;

/// A trait implemented by values which can be converted to a `Result<Option<T>, E>`
pub trait MaybeMissing<T> {
    /// The error type obtained after filtering missing values 
    type MissingError;
    /// Filter missing values from this `Result`-like
    fn missing(self) -> Result<Option<T>, Self::MissingError>;
}

/// A trait implemented by types `E` such that `Result<T, E>: MaybeMissing<T>`
pub trait MaybeMissingError<T> {
    /// The error type obtained after filtering missing values 
    type MissingError;
    /// Whether this error is a missing value or not
    fn to_missing(self) -> Result<Option<T>, Self::MissingError>;
}

impl<T> MaybeMissingError<T> for Infallible {
    type MissingError = Infallible;
    fn to_missing(self) -> Result<Option<T>, Infallible> { Err(self) }
}

impl<T> MaybeMissingError<T> for NotEnoughInformation {
    type MissingError = Infallible;
    fn to_missing(self) -> Result<Option<T>, Infallible> { Ok(None) }
}

impl<T> MaybeMissingError<T> for ValueError {
    type MissingError = ValueError;
    fn to_missing(self) -> Result<Option<T>, ValueError> {
        match self {
            ValueError::NotEnoughInformation(_) => Ok(None),
            err => Err(err)
        }
    }
}

impl<T, E> MaybeMissing<T> for Result<T, E> where E: MaybeMissingError<T> {
    type MissingError = E::MissingError;
    fn missing(self) -> Result<Option<T>, Self::MissingError> {
        match self {
            Ok(val) => Ok(Some(val)),
            Err(err) => err.to_missing() 
        }
    }
}