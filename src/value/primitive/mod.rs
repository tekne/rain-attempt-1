/*!
Primitive `rain` types and values
*/

pub mod binary;
pub mod finite;
pub mod logical;

use super::ValId;
use crate::prettyprinter::DisplayValue;
use crate::{debug_from_display, null_region};
use lazy_static::lazy_static;
use std::fmt::{self, Display, Formatter};

macro_rules! unit_primitive {
    ($u_ty:ty, $name:expr) => {
        impl Display for $u_ty {
            fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
                write!(fmt, "{}", $name)
            }
        }
        impl DisplayValue for $u_ty {}
        debug_from_display!($u_ty);
        null_region!($u_ty);
    };
}

/// The unit type
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Unit;
unit_primitive!(Unit, Unit::UNIT);
null_region!(());

impl Unit {
    /// The token associated with the unit type
    pub const UNIT: &'static str = "#unit";
}

/// The empty type
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Empty;
unit_primitive!(Empty, Empty::EMPTY);

impl Empty {
    /// The token associated with the empty type
    pub const EMPTY: &'static str = "#empty";
}

lazy_static! {
    /// A ValId containing the unit type
    pub static ref UNIT: ValId = ValId::try_construct(Unit).expect("Unit type is valid");
    /// A ValId containing the empty type
    pub static ref EMPTY: ValId = ValId::try_construct(Empty).expect("Empty type is valid");
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::value::{judgement::JEq, typing::Typed, ValId};

    #[test]
    fn unit_value_works_properly() {
        let unit: ValId = ().into();
        let unit_ty: ValId = Unit.into();
        assert!(().jeq(&()));
        assert!(!true.jeq(&unit));
        assert!(!true.jeq(&unit_ty));
        assert!(unit.jeq(&()));
        assert!(().jeq(&unit));
        assert!(!().jeq(&unit_ty));
        assert!(!unit_ty.jeq(&()));
        assert_eq!(unit.infer_ty(), Ok(Unit.into()));
        assert_eq!(format!("{}", unit), "[]");
    }
}
