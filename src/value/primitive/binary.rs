/*!
Binary constants
*/
use super::logical::LogicalOp;
use num::{BigInt, BigUint};
use std::fmt::{self, Debug, Display, Formatter};

/// The maximum number of bits a small binary constant can hold
pub const MAX_SMALL_BITS: u8 = 128;

/// A single bit
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[repr(transparent)]
pub struct Bit(pub bool);

/// A binary bitwise logical operation
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Bitwise {
    /// The operation to carry out
    op: LogicalOp,
    /// The number of bits to carry it out on
    bits: u16,
}

/// A small amount of bits
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct SmallBits(u8);

/// Display mode for a binary constant
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum BinaryDisplay {
    /// Binary display, (bits)'b(value) for binary, 0b(value) for integer
    Bin,
    /// Octal display, (bits)'o(value) for binary, 0o(value) for integer
    Oct,
    /// Decimal display (bits)'d(value) for binary, 0d(value) for integer
    Dec,
    /// Hexadecimal display, (bits)'h(value) for binary, 0x(value) for integer
    Hex,
}

/// A small binary constant
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Binary {
    bits: SmallBits,
    display: BinaryDisplay,
    value: u128,
}

/// A big natural number
#[derive(Clone, Eq, PartialEq, Hash)]
pub struct Natural(pub BigUint, pub BinaryDisplay);

impl Debug for Natural {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        <Self as Display>::fmt(self, fmt)
    }
}

impl Display for Natural {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        use BinaryDisplay::*;
        match self.1 {
            Bin => write!(fmt, "{:#b}", self.0),
            Oct => write!(fmt, "{:#o}", self.0),
            Dec => write!(fmt, "{}", self.0),
            Hex => write!(fmt, "{:#x}", self.0),
        }
    }
}

/// A big integer
#[derive(Clone, Eq, PartialEq, Hash)]
pub struct Integer(pub BigInt, pub BinaryDisplay);

impl Debug for Integer {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        <Self as Display>::fmt(self, fmt)
    }
}

impl Display for Integer {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        use BinaryDisplay::*;
        match self.1 {
            Bin => write!(fmt, "{:#b}", self.0),
            Oct => write!(fmt, "{:#o}", self.0),
            Dec => write!(fmt, "{}", self.0),
            Hex => write!(fmt, "{:#x}", self.0),
        }
    }
}
