/*!
Finite `rain` types
*/
use crate::debug_from_display;
use crate::null_region;
use crate::value::{
    primitive::{Empty, Unit},
    typing::Typed,
    universe::Universe,
    PrimitiveValue, TypeId, ValId, ValueData, ValueEnum,
};
use crate::prettyprinter::DisplayValue;
use std::cmp::Ordering;
use std::convert::{Infallible, TryFrom, TryInto};
use std::fmt::{self, Display, Formatter};

/// The finite type with `n` distinct values (up to `2^{128}`)
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Finite(pub u64);

impl Finite {
    /// The token associated with finite `rain` types
    pub const FINITE: &'static str = "#finite";
    /// Get a given index into this finite type. Return an error if the index is out of bounds.
    pub fn ix(self, ix: u64) -> Result<Index, ()> { Index::try_new(self, ix) }
    /// Iterate over the indices in this type
    pub fn iter(self) -> impl Iterator<Item=Index> {
        (0..self.0).map(move |ix| Index { ty: self, ix })
    }
}

debug_from_display!(Finite);
null_region!(Finite);

impl Display for Finite {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "{}({})", Self::FINITE, self.0)
    }
}

impl Typed for Finite {
    type TypeError = Infallible;
    type InferenceError = Infallible;
    fn universe(&self) -> Option<Universe> {
        Some(Universe::finite())
    }
    fn infer_ty(&self) -> Result<TypeId, Infallible> {
        Universe::finite().try_into()
    }
    fn ty(&self) -> Result<TypeId, Infallible> {
        Universe::finite().try_into()
    }
}

impl From<Finite> for ValueEnum {
    fn from(finite: Finite) -> ValueEnum {
        match finite.0 {
            0 => Empty.into(),
            1 => Unit.into(),
            _ => ValueEnum::Finite(finite),
        }
    }
}

impl From<Finite> for ValId {
    fn from(finite: Finite) -> ValId {
        ValId::try_new(ValueData::new(finite)).expect("Impossible")
    }
}

impl From<Finite> for TypeId {
    fn from(finite: Finite) -> TypeId {
        TypeId::try_from(ValId::from(finite)).expect("Impossible")
    }
}

impl PrimitiveValue for Finite {}

impl DisplayValue for Finite {}

/// An element of a finite type
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Index {
    /// The type of this element
    ty: Finite,
    /// The index of this element in the type
    ix: u64,
}

impl Index {
    /// The token assoiated with a member of a finite `rain` type
    pub const INDEX: &'static str = "#ix";
    /// Attempt to create a new element of a finite type. Return an error if the index is too large to fit in the specified type.
    pub fn try_new(ty: Finite, ix: u64) -> Result<Index, ()> {
        if ty.0 <= ix {
            Err(())
        } else {
            Ok(Index { ty, ix })
        }
    }
    /// Get the (finite) type of this element
    pub fn get_ty(&self) -> Finite {
        self.ty
    }
    /// Get the index of this element in its type
    pub fn ix(&self) -> u64 {
        self.ix
    }
    /// Get the unit index
    pub fn unit() -> Index { Index { ty: Finite(1), ix: 0 } }
}

impl PartialOrd for Index {
    fn partial_cmp(&self, other: &Index) -> Option<Ordering> {
        if self.ty == other.ty {
            Some(self.ix.cmp(&other.ix))
        } else {
            None
        }
    }
}

null_region!(Index);
debug_from_display!(Index);

impl Display for Index {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        write!(fmt, "{}({}, {})", Self::INDEX, self.ix, self.ty.0)
    }
}

impl Typed for Index {
    type TypeError = Infallible;
    type InferenceError = Infallible;
    fn universe(&self) -> Option<Universe> {
        None
    }
    fn infer_ty(&self) -> Result<TypeId, Infallible> {
        self.ty.try_into()
    }
    fn ty(&self) -> Result<TypeId, Infallible> {
        self.ty.try_into()
    }
}

impl From<Index> for ValueEnum {
    fn from(ix: Index) -> ValueEnum {
        match ix.ty.0 {
            1 => ().into(),
            _ => ValueEnum::Index(ix),
        }
    }
}

impl From<Index> for ValId {
    fn from(ix: Index) -> ValId {
        ValId::try_new(ValueData::new(ix)).expect("Impossible")
    }
}

impl From<Index> for TypeId {
    fn from(ix: Index) -> TypeId {
        TypeId::try_from(ValId::from(ix)).expect("Impossible")
    }
}

impl PrimitiveValue for Index {}

impl DisplayValue for Index {}