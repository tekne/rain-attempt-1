/*!
Gamma nodes
*/
use super::{
    error::{MaybeMissing, ValueError},
    judgement::JEq,
    parametrized::Parametrized,
    primitive::{
        finite::{Finite, Index},
        logical::{BOOL, FALSE, TRUE},
    },
    typing::Typed,
    TypeId, ValId, ValueEnum,
};
use smallvec::{smallvec, SmallVec};
use std::ops::Deref;

/// A gamma node, corresponding to a recursor
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Gamma {
    /// The branches of this gamma node
    branches: Branches,
    /// The type this gamma node acts as recursor for
    branch_ty: TypeId,
}

impl Gamma {
    /// Get the branches of this gamma node
    pub fn branches(&self) -> &Branches { &self.branches }
    /// Get the branch type of this gamma node
    pub fn branch_ty(&self) -> &TypeId { &self.branch_ty }
}

/// An exhaustive set of gamma node branches
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Branches(Vec<Branch>);

impl Branches {
    /// Attempt to create a new branch vector
    pub fn try_new<I>(branches: I) -> Result<Branches, ValueError> where I: Into<Vec<Branch>> {
        //TODO: exhaustiveness check
        Ok(Branches(branches.into()))
    }
}

impl Deref for Branches {
    type Target = [Branch];
    fn deref(&self) -> &[Branch] { &self.0 }
}

/// A branch of a gamma node
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Branch {
    /// The pattern of this branch
    pattern: Pattern,
    /// The value of this branch
    value: Parametrized<ValId>,
}

/// A pattern for a gamma node branch
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Pattern {
    /// The ignore pattern: accept anything, and ignore it
    Ignore(Ignore),
    /// The wildcard pattern: accept any value of the type
    Wildcard(Wildcard),
    /// Recognize a pattern: if it matches, return nothing, if it doesn't match, fail
    Recognize(Recognize),
    /// A boolean pattern: accept a given boolean value
    Bool(BoolPattern),
    /// A finite range pattern: accept a range of indices within a finite type
    Finite(FiniteRange),
    /// A tuple of patterns to match
    Tuple(TuplePattern),
}

/// A boolean pattern for a gamma node branch
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct BoolPattern(pub bool);

impl BoolPattern {
    /// Attempt to match a pattern, yielding a vector of pattern parameters on success
    pub fn try_match(&self, input: ValId) -> MatchResult {
        let compare_to: &ValId = if self.0 { &TRUE } else { &FALSE };
        let compare_not: &ValId = if self.0 { &FALSE } else { &TRUE };
        if &input == compare_to {
            // Quick pointer comparison
            Ok(smallvec![])
        } else if &input == compare_not {
            // Quick pointer comparison
            Err(MatchError::Failure)
        } else if let Some(ty) = input.ty().missing()? {
            if BOOL.eq(&ty) {
                // Quick pointer comparison
                Err(MatchError::Incomplete)
            } else {
                //TODO: return a full-on error here, instead of a failure?
                Err(MatchError::Failure)
            }
        } else {
            Err(MatchError::Incomplete)
        }
    }
}

/// Recognize a pattern: if it matches, return nothing, if it doesn't match, fail
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Recognize(Box<Pattern>);

impl Recognize {
    /// Attempt to match a pattern, yielding a vector of pattern parameters on success
    pub fn try_match(&self, input: ValId) -> MatchResult {
        self.0.try_match(input).map(|_| smallvec![])
    }
}

/// A tuple pattern for a gamma node branch
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct TuplePattern(Vec<Pattern>);

impl TuplePattern {
    /// Attempt to match a pattern, yielding a vector of pattern parameters on success
    pub fn try_match(&self, _input: ValId) -> MatchResult {
        unimplemented!()
    }
}

/// A vector of parameters to a gamma-region
pub type PatternParams = SmallVec<[ValId; Pattern::SMALL_PATTERN_PARAMS]>;

/// The result of trying to match a pattern
pub type MatchResult<T = PatternParams, E = ValueError> = Result<T, MatchError<E>>;

/// A failed pattern match
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum MatchError<E> {
    /// The pattern match failed
    Failure,
    /// The pattern match could not be determined to fail or succeed due to incomplete information
    Incomplete,
    /// An error occured in computing the pattern match
    Error(E),
}

impl<E> From<E> for MatchError<E> {
    fn from(err: E) -> MatchError<E> {
        MatchError::Error(err)
    }
}

impl Pattern {
    const SMALL_PATTERN_PARAMS: usize = 2;
    /// Attempt to match a pattern, yielding a vector of pattern parameters on success
    pub fn try_match(&self, input: ValId) -> MatchResult {
        match self {
            Pattern::Ignore(i) => i.try_match(input),
            Pattern::Wildcard(w) => w.try_match(input),
            Pattern::Recognize(r) => r.try_match(input),
            Pattern::Bool(b) => b.try_match(input),
            Pattern::Finite(f) => f.try_match(input),
            Pattern::Tuple(t) => t.try_match(input),
        }
    }
}

/// A finite range pattern: accepts a range of indices within a finite type.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct FiniteRange {
    /// The finite type which this is a range over
    ty: Finite,
    /// The lower end of this range
    lo: u64,
    /// The higher end of this range (inclusive).
    hi: u64,
}

impl FiniteRange {
    /// Try to make a new finite range. Return an error if either index is out of bounds. Return an *empty* range if `lo > hi` *and* both are in bounds.
    pub fn try_new(lo: u64, hi: u64, ty: Finite) -> Result<FiniteRange, ()> {
        let ty_s = ty.0 + 1;
        if lo >= ty_s || hi >= ty_s {
            Err(())
        } else if lo > hi {
            Ok(FiniteRange { ty, lo: 1, hi: 0 })
        } else {
            Ok(FiniteRange { ty, lo, hi })
        }
    }
    /// Check if a range is empty
    pub fn empty(&self) -> bool {
        self.lo > self.hi
    }
    /// Try to make a new finite range between two indices. Return an error if they have different types
    /// Note that the range between the two indices is here taken as *inclusive*
    pub fn between(lo: Index, hi: Index) -> Result<FiniteRange, ()> {
        if lo.get_ty() != hi.get_ty() {
            Err(())
        } else {
            FiniteRange::try_new(lo.ix(), hi.ix(), hi.get_ty())
        }
    }
    /// Return whether an index matches
    pub fn try_match_finite(&self, ix: Index) -> bool {
        if ix.get_ty() != self.ty {
            return false;
        }
        ix.ix() >= self.lo && ix.ix() <= self.hi
    }
    /// Attempt to match a pattern, yielding a vector of pattern parameters on success
    pub fn try_match(&self, input: ValId) -> MatchResult {
        // Quickcheck: nothing matches an empty range.
        if self.empty() {
            return Err(MatchError::Failure);
        }
        {
            // Get a read-lock on the input
            let input_read = input.read();
            // Finite matching
            match input_read.value() {
                ValueEnum::Index(ix) => {
                    if !self.try_match_finite(*ix) {
                        return Err(MatchError::Failure);
                    }
                }
                // General case: type checking
                _ => {
                    return if let Some(ty) = input_read.ty().missing()? {
                        if ty.jeq(&self.ty) {
                            Err(MatchError::Incomplete)
                        } else {
                            Err(MatchError::Failure) //TODO; return an error here?
                        }
                    } else {
                        Err(MatchError::Incomplete)
                    };
                }
            }
        }
        Ok(smallvec![input])
    }
}

/// The wildcard pattern. Matches everything.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Wildcard;

impl Wildcard {
    /// Attempt to match a pattern, yielding a vector of pattern parameters on success
    pub fn try_match(&self, input: ValId) -> MatchResult {
        Ok(smallvec![input])
    }
}

/// The ignore pattern. Matches everything, yielding nothing
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Ignore;

impl Ignore {
    /// Attempt to match a pattern, yielding a vector of pattern parameters on success
    pub fn try_match(&self, _input: ValId) -> MatchResult {
        Ok(smallvec![])
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne};
    #[test]
    fn finite_ranges_match_properly() {
        let ty = Finite(100);
        let range = FiniteRange::between(
            Index::try_new(ty, 53).unwrap(),
            Index::try_new(ty, 97).unwrap(),
        )
        .unwrap();
        let manual_range = FiniteRange::try_new(66, 77, ty).unwrap();
        for ix in ty.iter() {
            match ix.ix() {
                53..=97 => {
                    assert!(range.try_match_finite(ix));
                    let value = ValId::from(ix);
                    assert_eq!(range.try_match(ix.into()), Ok(smallvec![value.clone()]));
                    assert_eq!(Wildcard.try_match(ix.into()), Ok(smallvec![value.clone()]));
                    assert_eq!(Ignore.try_match(ix.into()), Ok(smallvec![]));
                    assert_eq!(
                        Pattern::Finite(range).try_match(ix.into()),
                        Ok(smallvec![value.clone()])
                    );
                    assert_eq!(
                        Pattern::Wildcard(Wildcard).try_match(ix.into()),
                        Ok(smallvec![value.clone()])
                    );
                    assert_eq!(
                        Pattern::Ignore(Ignore).try_match(ix.into()),
                        Ok(smallvec![])
                    );
                }
                _ => {
                    assert!(!range.try_match_finite(ix));
                    let value = ValId::from(ix);
                    assert_eq!(range.try_match(ix.into()), Err(MatchError::Failure));
                    assert_eq!(Wildcard.try_match(ix.into()), Ok(smallvec![value.clone()]));
                    assert_eq!(Ignore.try_match(ix.into()), Ok(smallvec![]));
                    assert_eq!(
                        Pattern::Finite(range).try_match(ix.into()),
                        Err(MatchError::Failure)
                    );
                    assert_eq!(
                        Pattern::Wildcard(Wildcard).try_match(ix.into()),
                        Ok(smallvec![value.clone()])
                    );
                    assert_eq!(
                        Pattern::Ignore(Ignore).try_match(ix.into()),
                        Ok(smallvec![])
                    );
                }
            }
            match ix.ix() {
                66..=77 => {
                    assert!(manual_range.try_match_finite(ix));
                    let value = ValId::from(ix);
                    assert_eq!(
                        manual_range.try_match(ix.into()),
                        Ok(smallvec![value.clone()])
                    );
                    assert_eq!(Wildcard.try_match(ix.into()), Ok(smallvec![value.clone()]));
                    assert_eq!(Ignore.try_match(ix.into()), Ok(smallvec![]));
                    assert_eq!(
                        Pattern::Finite(range).try_match(ix.into()),
                        Ok(smallvec![value.clone()])
                    );
                    assert_eq!(
                        Pattern::Wildcard(Wildcard).try_match(ix.into()),
                        Ok(smallvec![value.clone()])
                    );
                    assert_eq!(
                        Pattern::Ignore(Ignore).try_match(ix.into()),
                        Ok(smallvec![])
                    );
                }
                _ => {
                    assert!(!manual_range.try_match_finite(ix));
                    let value = ValId::from(ix);
                    assert_eq!(manual_range.try_match(ix.into()), Err(MatchError::Failure));
                    assert_eq!(Wildcard.try_match(ix.into()), Ok(smallvec![value.clone()]));
                    assert_eq!(Ignore.try_match(ix.into()), Ok(smallvec![]));
                    assert_eq!(
                        Pattern::Finite(manual_range).try_match(ix.into()),
                        Err(MatchError::Failure)
                    );
                    assert_eq!(
                        Pattern::Wildcard(Wildcard).try_match(ix.into()),
                        Ok(smallvec![value.clone()])
                    );
                    assert_eq!(
                        Pattern::Ignore(Ignore).try_match(ix.into()),
                        Ok(smallvec![])
                    );
                }
            }
        }
    }
}
