/*!
Parametrized `rain` values
*/

use super::{eval::SubstituteValue, ValId, ValueError};
use crate::graph::{
    error::IncomparableRegions,
    node::Downgrade,
    region::{HasRegion, Region, WeakRegion},
};
use smallvec::smallvec;
use std::borrow::Cow;
use std::convert::{Infallible, TryInto};
use std::ops::Deref;

/// A parametrized set of values
#[derive(Debug, Clone, PartialEq, Hash, Eq)]
pub struct Parametrized<T> {
    /// The region owned by this parametrized function
    region: Region,
    /// The values of this parametrized function
    value: [T; 1],
}

impl<T> HasRegion for Parametrized<T> {
    type RegionError = Infallible;
    fn region(&self) -> Result<Cow<WeakRegion>, Infallible> {
        Ok(Cow::Borrowed(&self.region.parent))
    }
}

/// Create a parametrized set of values
#[macro_export]
macro_rules! parametrized {
    ( $( $var:expr ),* => $val:expr ; parent = $parent:expr ) => {{
        use crate::graph::region::Region;
        use crate::value::parametrized::Parametrized;
        use smallvec::smallvec;
        let region: Region = Region::new_in(smallvec![$($var.into()),*], $parent);
        let value = $val(&region).into();
        Parametrized::new(region, value)
    }};
    ( $( $var:expr ),* => $val:expr ) => {{
        use crate::graph::region::WeakRegion;
        parametrized!( $($var),* => $val ; parent = WeakRegion::default() )
    }};
}

impl<T, R> Parametrized<T>
where
    T: HasRegion<RegionError = R>,
    R: Into<IncomparableRegions>,
{
    /// Create a new parametrized set of values with the given values from the given `Region`
    /// Return an error if the given values are not contained in the given `Region`.
    pub fn new(region: Region, value: T) -> Result<Parametrized<T>, IncomparableRegions> {
        // Check all values are in the given region
        let value_region = value.region().map_err(|err| err.into())?;
        if !(value_region.deref() >= &region) {
            return Err(IncomparableRegions(smallvec![
                region.downgrade(),
                value_region.into_owned()
            ])
            .into());
        }
        Ok(Parametrized { region, value: [value] })
    }
}

impl<T> Parametrized<T> {
    /// Get the region in which this parametrized set of values is defined
    pub fn def_region(&self) -> &Region {
        &self.region
    }
    /// Get the value array of this parametrized set of values
    pub fn value(&self) -> &T {
        &self.value[0]
    }
    /// Get the dependencies of this parametrized set of values
    pub fn dependencies(&self) -> std::slice::Iter<T> {
        self.value.iter()
    }
}

impl<T> SubstituteValue<Parametrized<T>> for Parametrized<T>
where
    T: Clone,
    T: AsRef<ValId>,
    ValId: TryInto<T>,
    <ValId as TryInto<T>>::Error: Into<ValueError>,
{
    fn substitute<F>(&self, mut f: F) -> Result<Parametrized<T>, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        //TODO: check or elide?
        Ok(Parametrized {
            region: self.region.clone(),
            value: [f(self.value().as_ref())?
                .try_into()
                .map_err(|err| err.into())?],
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::graph::region::WeakRegion;
    use crate::value::{
        primitive::{logical::Bool, Unit},
        ValId,
    };
    use crate::{assert_jeq, region};
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne};

    #[test]
    fn invalid_nested_parametrized_result_fails() {
        let region = Region::empty();
        let nested_region = region![Bool; parent = region.downgrade()];
        let y = &nested_region.params()[0];
        let _err = Parametrized::new(region.clone(), y.clone())
            .expect_err("This is an invalid function");
    }

    #[test]
    fn constant_bool_parametrized() {
        let region = Region::empty();
        let t = ValId::from(true);
        let parametrized = Parametrized::new(region.clone(), t.clone())
            .expect("This is a valid function");
        assert_eq!(parametrized.def_region(), &region);
        assert_jeq!(parametrized.value(), &t);
    }

    #[test]
    fn identity_bool_parametrized() {
        let region = region![Bool];
        let x = region.params()[0].clone();
        let parametrized = Parametrized::new(region.clone(), x.clone())
            .expect("This is a valid function");
        assert_eq!(parametrized.def_region(), &region);
        assert_eq!(parametrized.value(), &x);
    }

    #[test]
    fn parametrized_macro_bool_test() {
        let param: Parametrized<ValId> = parametrized![Bool => |r: &Region| r.params()[0].clone()]
            .expect("Valid parametrized value");
        assert_eq!(param.def_region().params().len(), 1);
        assert_eq!(param.def_region().parent, WeakRegion::default());
        let const_param: Parametrized<ValId> = parametrized![Unit, Bool => |_| true].unwrap();
        assert_eq!(const_param.def_region().params().len(), 2);
        assert_eq!(const_param.def_region().parent, WeakRegion::default());
        let region = Region::empty();
        let param_region: Parametrized<ValId> =
            parametrized![Bool, Bool => |r: &Region| r.params()[0].clone(); parent = region.downgrade()]
            .expect("Valid parametrized value");
        assert_eq!(param_region.def_region().params().len(), 2);
        assert_eq!(param_region.def_region().parent, region.downgrade());
        let const_region: Parametrized<ValId> = parametrized![
            Unit => |_| false; parent = region.downgrade()
        ].unwrap();
        assert_eq!(const_region.def_region().params().len(), 1);
        assert_eq!(const_region.def_region().parent, region.downgrade());
    }
}
