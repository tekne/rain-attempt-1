/*!
Hash-consing of `rain` values
*/

use super::ValueData;
use crate::graph::cons::{GlobalCache, NodeCache};
use lazy_static::lazy_static;
use rand::{self, Rng};

/// A cache for `rain` values, for hash-consing
pub type ValueCache<C = GlobalCache> = NodeCache<ValueData, C>;

lazy_static! {
    /// A lazily-initialized global cache for `rain` values, for hash-consing
    pub static ref VALUE_CACHE: ValueCache = {
        let mut rng = rand::thread_rng();
        ValueCache::with_keys(rng.gen())
    };
}

#[cfg(test)]
mod tests {
    use crate::value::ValId;
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne};

    #[test]
    fn boolean_values_are_hash_consed() {
        let vt = ValId::from(true);
        let vf = ValId::from(false);
        let vt2 = ValId::from(true);
        let vf2 = ValId::from(false);
        assert_eq!(vt, vt2);
        assert_eq!(vf, vf2);
        assert_ne!(vt, vf);
    }
}
