/*!
Implementation for value normalization
 */
use crate::value::{
    error::{NotAFunction, ValueError},
    expr::SexprArgs,
    ValueEnum
};

 impl SexprArgs {
     /// Attempt to normalize these arguments
     pub fn try_normalize(&mut self) -> Result<(), ValueError> {
        while self.len() > 1 {
            let arg = self.pop().unwrap();
            match {
                let data = arg.read();
                match data.value() {
                    ValueEnum::LogicalOp(l) => self.apply_logical(*l),
                    ValueEnum::Lambda(l) => self.apply_lambda(l),
                    ValueEnum::Tuple(t) => self.apply_tuple(t),
                    //TODO: sexpr flattening?
                    _ => {
                        if !data.value().applicable() {
                            // This cannot be a function, giving an error
                            Err(ValueError::NotAFunction(NotAFunction {
                                applied: Some(arg.clone()),
                                argument: self.last().cloned(),
                            }))
                        } else {
                            Ok(false)
                        }
                    }
                }
            } {
                Ok(true) => {}
                Ok(false) => {
                    self.push(arg);
                    return Ok(());
                }
                Err(err) => {
                    self.push(arg);
                    return Err(err);
                }
            }
        }
        Ok(())
    }
 }