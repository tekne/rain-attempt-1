/*!
 * Implementation for tuple members
 */
use crate::value::{
    error::{TypeMismatch, TypeMismatchCtx, ValueError, NotAFunction},
    expr::SexprArgs,
    primitive::finite::Finite,
    tuple::Tuple,
    typing::Typed,
    ValueEnum
};

impl SexprArgs {
    /// Get a member of a tuple, as determined by a finite index.
    /// Return whether any computation occured.
    pub(super) fn apply_tuple(&mut self, tuple: &Tuple) -> Result<bool, ValueError> {
        let f_ty = Finite(tuple.len() as u64);
        if let Some(valid) = self.pop() {

            if tuple.len() == 0 { // Special case: the unit type is not a function!
                self.push(valid.clone());
                return Err(NotAFunction {
                    argument: Some(valid),
                    applied: None //TODO: this...
                }.into())
            }

            let value_read = valid.read();
            match value_read.value() { // Otherwise, look for indices
                ValueEnum::Index(ix) => {
                    if ix.get_ty() == f_ty {
                        self.push(tuple[ix.ix() as usize].clone());
                        Ok(true)
                    } else {
                        Err(TypeMismatch {
                            value: valid.clone(),
                            ty: f_ty.into(),
                            ctx: TypeMismatchCtx::TupleIndexMismatch(tuple.len()),
                        }
                        .into())
                    }
                },
                v => {
                    if tuple.len() == 1 && v == &ValueEnum::unit() { // Special case: unit indexing
                        self.push(tuple[0].clone());
                        return Ok(true);
                    }
                    let f_ty = f_ty.into();
                    if value_read.ty()? == f_ty {
                        self.push(valid.clone());
                        Ok(false)
                    } else {
                        Err(TypeMismatch {
                            value: valid.clone(),
                            ty: f_ty,
                            ctx: TypeMismatchCtx::TupleIndexMismatch(tuple.len()),
                        }
                        .into())
                    }
                }
            }

        } else {
            Ok(false)
        }
    }
}
