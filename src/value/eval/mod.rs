/*!
Evaluation and normalization of `rain` values
*/
use crate::graph::region::{HasRegion, Region};
use crate::util::PassThroughHasher;
use crate::value::{
    data::ValueData,
    error::{MaybeMissing, TypeMismatch, TypeMismatchCtx, ValueError},
    typing::Typed,
    TypeId, ValId, Value, ValueDesc, ValueEnum,
};
use std::collections::HashMap;
use std::hash::BuildHasherDefault;

mod lambda_impl;
mod logical_impl;
mod normalize_impl;
mod tuple_impl;

/// An evaluation context for `rain` values.
#[derive(Debug, Clone)]
pub struct EvalCtx {
    /// Cached values for this evaluation context
    cached: HashMap<ValId, ValId, BuildHasherDefault<PassThroughHasher>>,
    /// The minimum depth-of-substitution. Any `ValId`s in shallower depths will not be substituted.
    minimum_depth: usize,
}

/// Increase the starting capacity of an evaluation context by this many elements
/// (above the number of region arguments plus the number of results)
pub const DEFAULT_EVAL_CTX_BOOST: usize = 16;

/// A value which can be substituted to yield another or an error
pub trait SubstituteValue<T> {
    /// Substitute the components of this value
    fn substitute<F>(&self, f: F) -> Result<T, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>;
}

/// For a trait implementing `SubstituteValue<Self>`, implement `
pub trait SubstituteToValId: Sized + SubstituteValue<Self> {}

impl<T> SubstituteValue<ValueEnum> for T
where
    T: SubstituteToValId + Value,
    T::ValueError: Into<ValueError>,
{
    fn substitute<F>(&self, f: F) -> Result<ValueEnum, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        self.substitute(f)?.to_value().map_err(|err| err.into())
    }
}

impl<T> SubstituteValue<ValId> for T
where
    T: SubstituteToValId + ValueDesc,
    T::ValueError: Into<ValueError>,
{
    fn substitute<F>(&self, f: F) -> Result<ValId, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        self.substitute(f)?.to_node().map_err(|err| err.into())
    }
}

impl EvalCtx {
    /// Create a new evaluation context with the given capacity
    pub fn with_capacity(n: usize) -> EvalCtx {
        EvalCtx {
            cached: HashMap::with_capacity_and_hasher(n, BuildHasherDefault::default()),
            minimum_depth: usize::MAX,
        }
    }
    /// Create a new evaluation context with heuristic capacity for a given region
    pub fn with_capacity_for(region: &Region) -> EvalCtx {
        Self::with_capacity(region.params().len() + DEFAULT_EVAL_CTX_BOOST)
    }
    /// Substitute a value for another with a given type.
    /// Do not perform any region checking.
    fn register_substitute_with_ty(
        &mut self,
        param: ValId,
        value: ValId,
        got: Option<TypeId>,
    ) -> Result<Option<ValId>, ValueError> {
        // Check types
        if let Some(got) = got {
            if let Some(ty) = param.infer_ty().missing()? {
                if !(got <= ty) {
                    return Err(TypeMismatch {
                        value,
                        ty,
                        ctx: TypeMismatchCtx::FailedSubstitution(param),
                    }
                    .into());
                }
            }
        }
        // Place in table
        Ok(self.cached.insert(param, value))
    }
    /// Get the minimum depth of this evaluation context
    pub fn minimum_depth(&self) -> usize {
        self.minimum_depth
    }
    /// Check if a region is compatible with this `EvalCtx`. If so, deepen this context if
    /// necessary. If not, return an `IncomparableRegions` error.
    pub fn set_region(&mut self, region: &Region) {
        self.minimum_depth = self.minimum_depth.min(region.depth);
    }
    /// Substitute a value for another. If types do not match, raise a `TypeMismatch` error.
    /// If region is incomparable, raise an `IncomparableRegions` error.
    /// On success, return the old substitution value, if any
    pub fn register_substitute(
        &mut self,
        parameter: ValId,
        value: ValId,
    ) -> Result<Option<ValId>, ValueError> {
        if let Some(region) = parameter.region()?.upgrade() {
            self.set_region(&region)
        }
        let ty = value.ty()?;
        self.register_substitute_with_ty(parameter, value, Some(ty))
    }
    /// Substitute an array of values for the parameters of a region.
    /// Perform a partial substitution, consuming values from the privded iterator.
    /// Return how many values in the region were remaining, or an error.
    pub fn register_region<'a, I>(
        &mut self,
        region: &Region,
        mut values: I,
    ) -> Result<usize, ValueError>
    where
        I: Iterator<Item = ValId>,
    {
        // Check region first
        self.set_region(region);
        let params = region.params();
        for (i, param) in params.iter().enumerate() {
            let value = if let Some(value) = values.next() {
                value
            } else {
                return Ok(params.len() - i);
            };
            let ty = value.infer_ty();
            self.register_substitute_with_ty(param.clone(), value, ty.ok())?;
        }
        Ok(0)
    }
    /// Forcibly evaluates value data in a given context
    pub fn force_eval(&mut self, data: &ValueData) -> Result<ValId, ValueError> {
        let sub_val: ValId = data.value().substitute(|val: &ValId| self.eval(val))?;
        let sub_data = ValueData::try_new(sub_val)?;
        ValId::try_new(sub_data)
    }
    /// Evaluates a value in the given context
    pub fn eval(&mut self, value: &ValId) -> Result<ValId, ValueError> {
        // Check if this value has already been evaluated
        if let Some(value) = self.cached.get(&value) {
            return Ok(value.clone());
        }
        // Otherwise, check if the region of this value is too low
        let data = value.read();
        let region = data.region()?.upgrade();
        let region_depth = if let Some(region) = region {
            region.depth
        } else {
            0
        };
        if region_depth >= self.minimum_depth {
            let result = self.force_eval(&data)?;
            self.cached.insert(value.clone(), result.clone());
            Ok(result)
        } else {
            Ok(value.clone())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::graph::node::Downgrade;
    use crate::value::{
        primitive::{logical::Bool, Unit},
        universe::Universe,
    };
    use pretty_assertions::assert_eq;
    use smallvec::smallvec;

    #[test]
    fn parameters_evaluate() {
        // Create a new, empty evaluation context and check its members
        let mut eval_ctx = EvalCtx::with_capacity(3);
        assert_eq!(eval_ctx.minimum_depth(), usize::MAX);

        // Such a context should just copy values without changing them
        assert_eq!(
            eval_ctx.eval(&true.into()).expect("Valid"),
            ValId::from(true)
        );

        let params: [ValId; 4] = [false.into(), Unit.into(), Bool.into(), true.into()];
        let mut param_iter = params.iter().cloned().rev();

        // Substitute a region in this evaluation context, after checking it first leaves parameters unchanged.
        let region = Region::new(smallvec![
            Bool.into(),
            Universe::finite().into(),
            Universe::finite().into()
        ]);
        assert_eq!(
            eval_ctx.eval(&region.params()[0]).unwrap(),
            region.params()[0]
        );
        let leftovers = eval_ctx
            .register_region(&region, &mut param_iter)
            .expect("This is a valid parameter assignment");
        assert_eq!(leftovers, 0);
        assert_eq!(eval_ctx.minimum_depth(), 1);
        for (i, param) in region.params().iter().enumerate() {
            assert_eq!(eval_ctx.eval(&param).unwrap(), params[3 - i])
        }
        // Substitute a nested region into this evaluation context, after checking it first leaves parameters unchanged.
        let nested_region = Region::new_in(smallvec![Bool.into()], region.downgrade());
        assert_eq!(
            eval_ctx.eval(&nested_region.params()[0]).unwrap(),
            nested_region.params()[0]
        );
        let leftovers = eval_ctx
            .register_region(&nested_region, &mut param_iter)
            .expect("This is a valid parameter assignment");
        assert_eq!(leftovers, 0);
        assert_eq!(eval_ctx.minimum_depth(), 1);
        for (i, param) in region.params().iter().enumerate() {
            assert_eq!(eval_ctx.eval(&param).unwrap(), params[3 - i])
        }
        assert_eq!(
            eval_ctx.eval(&nested_region.params()[0]).unwrap(),
            params[0]
        );

        // Now substitute a nested, empty region into the context
        let nested_empty_region = Region::new_in(smallvec![], nested_region.downgrade());
        let leftovers = eval_ctx
            .register_region(&nested_empty_region, &mut param_iter)
            .expect("Empty parameter assignments are valid");
        assert_eq!(leftovers, 0);
        assert_eq!(eval_ctx.minimum_depth(), 1);
        for (i, param) in region.params().iter().enumerate() {
            assert_eq!(eval_ctx.eval(&param).unwrap(), params[3 - i])
        }
        assert_eq!(
            eval_ctx.eval(&nested_region.params()[0]).unwrap(),
            params[0]
        );

        // Now partially substitute a region with two additional parameters
        let nested_param_region = Region::new_in(
            smallvec![Bool.into(), Bool.into(), Bool.into()],
            nested_empty_region.downgrade(),
        );
        for param in nested_param_region.params().iter() {
            assert_eq!(&eval_ctx.eval(param).unwrap(), param);
        }
        let more_params: [ValId; 1] = [ValId::from(true)];

        let leftovers = eval_ctx
            .register_region(&nested_param_region, &mut more_params.iter().cloned())
            .expect("Sub-assignments are valid");
        assert_eq!(leftovers, 2);

        for (i, param) in region.params().iter().enumerate() {
            assert_eq!(eval_ctx.eval(&param).unwrap(), params[3 - i])
        }
        assert_eq!(
            eval_ctx.eval(&nested_region.params()[0]).unwrap(),
            params[0]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[0]).unwrap(),
            more_params[0]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[1]).unwrap(),
            nested_param_region.params()[1]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[2]).unwrap(),
            nested_param_region.params()[2]
        );

        // Now substitute the other two parameters

        // Try a failed substitution:
        eval_ctx
            .register_substitute(
                nested_param_region.params()[1].clone(),
                Universe::finite().into(),
            )
            .expect_err("Invalid substitution!");

        for (i, param) in region.params().iter().enumerate() {
            assert_eq!(eval_ctx.eval(&param).unwrap(), params[3 - i])
        }
        assert_eq!(
            eval_ctx.eval(&nested_region.params()[0]).unwrap(),
            params[0]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[0]).unwrap(),
            more_params[0]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[1]).unwrap(),
            nested_param_region.params()[1]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[2]).unwrap(),
            nested_param_region.params()[2]
        );

        // Try a failed type-substitution
        eval_ctx
            .register_substitute(
                nested_param_region.params()[1].clone(),
                Universe::finite().into(),
            )
            .expect_err("Invalid substitution!");
        for (i, param) in region.params().iter().enumerate() {
            assert_eq!(eval_ctx.eval(&param).unwrap(), params[3 - i])
        }
        assert_eq!(
            eval_ctx.eval(&nested_region.params()[0]).unwrap(),
            params[0]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[0]).unwrap(),
            more_params[0]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[1]).unwrap(),
            nested_param_region.params()[1]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[2]).unwrap(),
            nested_param_region.params()[2]
        );

        // Passed inference
        eval_ctx
            .register_substitute(nested_param_region.params()[1].clone(), ValId::from(false))
            .expect("Valid substitution");
        for (i, param) in region.params().iter().enumerate() {
            assert_eq!(eval_ctx.eval(&param).unwrap(), params[3 - i])
        }
        assert_eq!(
            eval_ctx.eval(&nested_region.params()[0]).unwrap(),
            params[0]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[0]).unwrap(),
            more_params[0]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[1]).unwrap(),
            ValId::from(false)
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[2]).unwrap(),
            nested_param_region.params()[2]
        );

        // Passed type
        eval_ctx
            .register_substitute(nested_param_region.params()[2].clone(), true.into())
            .expect("Valid substitution");

        for (i, param) in region.params().iter().enumerate() {
            assert_eq!(eval_ctx.eval(&param).unwrap(), params[3 - i])
        }
        assert_eq!(
            eval_ctx.eval(&nested_region.params()[0]).unwrap(),
            params[0]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[0]).unwrap(),
            more_params[0]
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[1]).unwrap(),
            ValId::from(false)
        );
        assert_eq!(
            eval_ctx.eval(&nested_param_region.params()[2]).unwrap(),
            ValId::from(true)
        );
    }
}
