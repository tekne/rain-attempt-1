/*!
 * Implementation for logical operation
 */

use crate::value::{
    error::ValueError,
    expr::SexprArgs,
    primitive::logical::{Binary, LogicalOp, Unary},
    ValId, ValueEnum,
};

impl SexprArgs {
    /// Apply a unary logical operation to an argument stack.
    /// Return whether any computation occured.
    pub(super) fn apply_binary_logical(&mut self, b: Binary) -> Result<bool, ValueError> {
        let res = Ok(false);
        let left = if let Some(left) = self.pop() {
            left
        } else {
            return res;
        };
        {
            let data = left.read();
            match data.value() {
                ValueEnum::Bool(l) => {
                    if let Some(right) = self.pop() {
                        {
                            let data = right.read();
                            match data.value() {
                                ValueEnum::Bool(r) => {
                                    self.push(ValId::from(b.apply(*l, *r)));
                                    return Ok(true);
                                }
                                //TODO: type errors and such
                                _ => {}
                            }
                        }
                        self.push(right);
                    }
                    self.push(ValId::from(b.partial_apply(*l)));
                    return Ok(true);
                }
                //TODO: type errors and such
                _ => {}
            }
        }
        self.push(left);
        res
    }
    /// Apply a unary logical operation to an argument stack.
    /// Return whether any computation occured.
    pub(super) fn apply_unary_logical(&mut self, u: Unary) -> Result<bool, ValueError> {
        let res = Ok(false);
        let arg = if let Some(arg) = self.pop() {
            arg
        } else {
            return res;
        };
        {
            let data = arg.read();
            match data.value() {
                ValueEnum::Bool(b) => {
                    self.push(ValId::from(u.apply(*b)));
                    return Ok(true);
                }
                //TODO: type errors and such
                _ => {}
            }
        }
        // Cleanup
        self.push(arg);
        res
    }
    /// Apply a logical operation to an argument stack.
    /// Return whether any computation occured.
    pub(super) fn apply_logical(&mut self, l: LogicalOp) -> Result<bool, ValueError> {
        match l {
            LogicalOp::Binary(b) => self.apply_binary_logical(b),
            LogicalOp::Unary(u) => self.apply_unary_logical(u),
        }
    }
}
