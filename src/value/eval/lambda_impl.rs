/*!
 * Implementation for lambda evaluation
 */
use crate::value::{
    error::{TypeMismatch, TypeMismatchCtx, ValueError},
    eval::EvalCtx,
    expr::SexprArgs,
    lambda::Lambda,
    typing::Typed,
};

impl SexprArgs {
    /// Apply a lambda function to an argument stack in a given evaluation context.
    /// Return whether any computation occured.
    pub(super) fn apply_lambda_in_ctx(
        &mut self,
        lambda: &Lambda,
        ctx: &mut EvalCtx,
    ) -> Result<bool, ValueError> {
        let mut nargs = self.len();
        let mut args = self.iter().cloned().rev();
        let rest = ctx
            .register_region(lambda.def_region(), &mut args)?;
        // Ignore partial evaluations
        if rest != 0 {
            return Ok(false)
        }
        // Push a single evaluated lambda result. The case of multiple results or no results is not yet handled.
        let result = ctx.eval(lambda.result())?;
        // Cut off used values, push result and return
        nargs -= lambda.def_region().params().len();
        self.truncate(nargs);
        self.push(result);
        Ok(true)
    }
    /// Apply a lambda function with a given evaluation context additional capacity
    pub(super) fn apply_lambda_with_capacity(
        &mut self,
        lambda: &Lambda,
        additional_capacity: usize,
    ) -> Result<bool, ValueError> {
        let lambda_params = lambda.def_region().params();
        // If there are too few parameters, check their types but leave them unevaluated
        if lambda_params.len() > self.len() {
            for (lambda_param, arg) in lambda_params.iter().zip(self.iter().rev()) {
                if let Some(ty) = lambda_param.infer_ty().ok() {
                    if let Some(got) = arg.infer_ty().ok() {
                        if !(got <= ty) {
                            return Err(TypeMismatch {
                                ty: ty.clone(),
                                value: arg.clone(),
                                ctx: TypeMismatchCtx::FailedPartialSubstitution(
                                    lambda_param.clone(),
                                ),
                            }
                            .into());
                        }
                    }
                }
            }
            // No computation occured, but the types are good.
            //TODO: use this fact when flattening S-expressions in normalization...
            return Ok(false);
        }
        // If there are enough parameters, create the context and proceed with in-context evaluation
        let capacity = lambda_params.len() + additional_capacity;
        let mut ctx = EvalCtx::with_capacity(capacity);
        self.apply_lambda_in_ctx(lambda, &mut ctx)
    }
    /// Apply a lambda function to an argument stack.
    /// Return whether any computation occured.
    pub(super) fn apply_lambda(&mut self, lambda: &Lambda) -> Result<bool, ValueError> {
        self.apply_lambda_with_capacity(lambda, super::DEFAULT_EVAL_CTX_BOOST)
    }
}
