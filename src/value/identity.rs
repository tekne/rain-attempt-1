/*!
Identity types and `refl` constructor
*/

use smallvec::SmallVec;
use super::{
    error::{TypeMismatch, TypeMismatchCtx::IdentityConstruction, ValueError},
    typing::Typed,
    TypeId, ValId, ValueDesc,
};

/// The identity type between two `ValId`s, sitting in a given `TypeId`.
//TODO: poly-identity types?
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Id {
    /// The left member of this identity type
    left: ValId,
    /// The right member of this identity type
    right: ValId,
    /// The type this identity type is within
    //TODO: does this *need* to be stored? Does it introduce ambiguity in it's own way (e.g. universes, later row typing)?
    ty: TypeId,
}

impl Id {
    /// Try to create a new identity type for two values sitting within a given type
    pub fn try_new(ty: TypeId, left: ValId, right: ValId) -> Result<Id, ValueError> {
        let left_ty = left.ty()?;
        if left_ty <= ty {
            let right_ty = right.ty()?;
            if right_ty <= ty {
                Ok(Id { left, right, ty })
            } else {
                Err(TypeMismatch {
                    ty,
                    value: right.clone(),
                    ctx: IdentityConstruction(left.clone()),
                }
                .into())
            }
        } else {
            Err(TypeMismatch {
                ty,
                value: left.clone(),
                ctx: IdentityConstruction(right.clone()),
            }
            .into())
        }
    }
    /// Try to create a new identity type for two values
    pub fn id(left: ValId, right: ValId) -> Result<Id, ValueError> {
        let left_ty = left.ty()?;
        let right_ty = right.ty()?;
        if left_ty <= right_ty {
            Ok(Id {
                left,
                right,
                ty: right_ty,
            })
        } else if right_ty <= left_ty {
            Ok(Id {
                left,
                right,
                ty: left_ty,
            })
        } else {
            Err(TypeMismatch {
                ty: left_ty,
                value: right.clone(),
                ctx: IdentityConstruction(left.clone()),
            }
            .into())
        }
    }
    /// Try to create a new identity type for a single value
    pub fn refl<V>(v: V) -> Result<Id, V::ValueError>
    where
        V: ValueDesc,
        V: Typed,
        V::TypeError: Into<V::ValueError>,
    {
        let ty = v.ty().map_err(|err| err.into())?;
        let left = v.to_node()?;
        let right = left.clone();
        Ok(Id { ty, left, right })
    }
}

/// The `refl` type constructor which, given a value `x`, constructs an element `x =_{#typeof(x)} x`
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Refl(pub ValId);

/// Transport across an identity type
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Transport(ValId);

/// The size of a small instance of function applicativity
pub const SMALL_AP_SIZE: usize = 2;

/// A small vector of identity types to use to create an instance of function applicativity
pub type ApArgs = SmallVec<[ValId; SMALL_AP_SIZE]>;

/// Function applicativity: `\forall f, \forall i, a_i = b_i \implies f(a_1,...,a_n) = f(b_1,...,b_n)`
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Ap(ApArgs);