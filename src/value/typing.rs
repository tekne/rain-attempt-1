/*!
`rain` value typing and associated utilities
*/
use super::{
    error::{NotEnoughInformation, ValueError, MaybeMissingError},
    primitive::{
        logical::{Binary, Bool, LogicalOp, Unary},
        Empty, Unit,
    },
    universe::Universe,
    Type, TypeId, ValId,
};
use crate::pi;
use crate::util::AlwaysOk;
use std::convert::Infallible;

/// A value descriptor with an inferrable type
pub trait Typed {
    /// A potential error that can occur when trying to compute a value's type
    type TypeError: Into<ValueError>;
    /// A potential error that can occur when trying to infer a value's type
    type InferenceError: Into<ValueError> + MaybeMissingError<TypeId>;
    /// Try to quickly get the universe of this value, if it is a type. Returns an error if it is cheaper to
    /// compute the universe by cached type, if possible
    fn cheap_universe(&self) -> Result<Option<Universe>, ()> {
        Err(())
    }
    /// Get the universe of this value, if it is a type. May return `None` for a type if not enough information
    /// is present, but will never return the wrong universe.
    fn universe(&self) -> Option<Universe> {
        match self.ty() {
            Ok(t) => t.universe()?.enclosed(),
            Err(_) => None,
        }
    }
    /// Check whether this value is a type. Is never a false positive, but may be a false negative.
    fn is_ty(&self) -> bool {
        self.universe().is_some()
    }
    /// Infer the type of a given value. Avoid expensive computations, recursive calls and universes.
    fn infer_ty(&self) -> Result<TypeId, Self::InferenceError>;
    /// Compute the type of a given value
    fn ty(&self) -> Result<TypeId, Self::TypeError>;
    /// Check if computing `apply_ty` on this value is generally quicker than computing `apply_to_value` on it's cached type
    #[inline]
    fn cheap_apply_ty(&self) -> bool {
        true
    }
    /// Get the type of applying this value to a set of values, if any
    fn apply_ty<'a, I>(&self, args: I) -> Result<TypeId, ValueError>
    where
        I: Iterator<Item = &'a ValId>,
    {
        self.ty().map_err(|err| err.into())?.apply_to_value(args.peekable())
    }
}

macro_rules! def_ty {
    ($v:ty, $t:expr, $universe:expr, $apply_ty:expr) => {
        impl Typed for $v {
            type TypeError = Infallible;
            type InferenceError = Infallible;
            #[inline]
            fn cheap_universe(&self) -> Result<Option<Universe>, ()> {
                Ok(self.universe())
            }
            #[inline]
            fn universe(&self) -> Option<Universe> {
                $universe(self).into()
            }
            #[inline]
            fn infer_ty(&self) -> Result<TypeId, Infallible> {
                Ok($t(self).into())
            }
            #[inline]
            fn ty(&self) -> Result<TypeId, Infallible> {
                Ok($t(self).into())
            }
            #[inline]
            fn apply_ty<'a, I>(&self, args: I) -> Result<TypeId, ValueError>
            where
                I: Iterator<Item = &'a ValId>,
            {
                $apply_ty(self, args)
            }
        }
    };
    ($v:ty, $t:expr, type) => {
        def_ty!($v, $t, $t, |s: &Self, args: I| s.ty()?.apply_to_value(args.peekable()));
    };
    ($v:ty, $t:expr, $apply_ty:expr) => {
        def_ty!($v, $t, |_| None, $apply_ty);
    };
    ($v:ty, $t:expr) => {
        def_ty!($v, $t, |s: &Self, args: I| s.ty()?.apply_to_value(args.peekable()));
    };
}

def_ty!((), |_| Unit);
def_ty!(bool, |_| Bool);
def_ty!(Unary, |_| pi![Bool => |_| Bool]);
def_ty!(Binary, |_| pi![Bool, Bool => |_| Bool]);

def_ty!(
    LogicalOp,
    |l: &LogicalOp| match *l {
        LogicalOp::Unary(u) => u.ty().aok(),
        LogicalOp::Binary(b) => b.ty().aok(),
    },
    |l: &LogicalOp, args| match *l {
        LogicalOp::Unary(u) => u.apply_ty(args),
        LogicalOp::Binary(b) => b.apply_ty(args),
    }
);
def_ty!(Bool, |_| Universe::finite(), type);
def_ty!(Empty, |_| Universe::finite(), type);
def_ty!(Unit, |_| Universe::finite(), type);

impl Typed for Universe {
    type TypeError = Infallible;
    type InferenceError = NotEnoughInformation;
    fn cheap_universe(&self) -> Result<Option<Universe>, ()> {
        Ok(Some(self.enclosing_ty()))
    }
    fn universe(&self) -> Option<Universe> {
        Some(self.enclosing_ty())
    }
    fn is_ty(&self) -> bool {
        self.universe().is_some()
    }
    fn infer_ty(&self) -> Result<TypeId, NotEnoughInformation> {
        Err(NotEnoughInformation)
    }
    fn ty(&self) -> Result<TypeId, Self::TypeError> {
        Ok(self.enclosing_ty().into())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne};
    #[test]
    fn bools_have_proper_type() {
        assert_eq!(false.ty(), Ok(Bool.into()));
        assert_eq!(true.ty(), Ok(Bool.into()));
        assert_eq!(Bool.ty(), Ok(Universe::finite().into()));
        assert_eq!(false.infer_ty(), Ok(Bool.into()));
    }

    #[test]
    fn bools_are_not_a_type() {
        assert!(!false.is_ty());
        assert!(!true.is_ty());
    }

    #[test]
    fn bool_is_a_type() {
        assert!(Bool.is_ty());
        assert_eq!(Bool.universe(), Some(Universe::finite()));
        assert_eq!(Bool.infer_ty(), Ok(Universe::finite().into()));
        assert_eq!(Bool.ty(), Ok(Universe::finite().into()));
    }
    #[test]
    fn unit_is_a_type() {
        assert!(Unit.is_ty());
        assert_eq!(Unit.universe(), Some(Universe::finite()));
        assert_eq!(Unit.infer_ty(), Ok(Universe::finite().into()));
        assert_eq!(Unit.ty(), Ok(Universe::finite().into()));
    }

    #[test]
    fn logical_ops_have_proper_type() {
        let unary = pi![Bool => |_| Bool];
        let binary = pi![Bool, Bool => |_| Bool];
        assert_eq!(Unary::Id.ty(), Ok(unary.clone()));
        assert_eq!(Binary::And.ty(), Ok(binary.clone()));
        assert_eq!(LogicalOp::Unary(Unary::Not).ty(), Ok(unary));
        assert_eq!(LogicalOp::Binary(Binary::Or).ty(), Ok(binary));
    }

    #[test]
    fn primitive_universes_have_proper_type() {
        let pairs = [
            (Universe::finite(), Universe::finite().enclosing_ty()),
            (
                Universe::try_new(7, 5).unwrap(),
                Universe::try_new(8, 6).unwrap(),
            ),
        ];
        for (lower, higher) in pairs.iter().copied() {
            assert!(lower.is_ty());
            assert!(higher.is_ty());
            assert_eq!(lower.ty(), Ok(higher.into()));
            assert_eq!(Universe::from(lower).ty(), Ok(higher.into()))
        }
    }
}
