/*!
Phi nodes
*/

use crate::graph::region::Region;
use super::ValId;
use smallvec::SmallVec;

/// The size of a small phi node
pub const SMALL_PHI_NODE: usize = 2;

/// A phi node, corresponding to a vector of recursively defined values
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Phi {
    /// The region in which this phi node's values are defined in
    region: Region,
    /// The set of recursively defined values in this node
    values: SmallVec<[ValId; SMALL_PHI_NODE]>
}

impl Phi {
    /// Get the region in which this node's values are defined
    pub fn def_region(&self) -> &Region { &self.region }
}