/*!
Lambda functions and associated utilities
*/

use super::{
    error::{NotEnoughInformation, ValueError},
    eval::{SubstituteToValId, SubstituteValue},
    parametrized::Parametrized,
    pi::Pi,
    typing::Typed,
    TypeDesc, TypeId, ValId, ValueData, ValueEnum,
};
use crate::graph::{
    error::IncomparableRegions,
    region::{HasRegion, Region, WeakRegion},
};
use crate::prettyprinter::{DisplayValue, PrettyPrinter, TempReg};
use crate::util::AlwaysOk;
use std::borrow::Cow;
use std::convert::Infallible;
use std::fmt::{self, Display, Formatter};

/// Create a lambda type
#[macro_export]
macro_rules! lambda {
    ( value $( $var:expr ),* => $result:expr ; parent = $parent:expr ) => {{
        use crate::parametrized;
        use crate::value::lambda::Lambda;
        parametrized![$( $var ),* => $result; parent = $parent].map(Lambda)
    }};
    ( value $( $var:expr ),* => $result:expr ) => {{
        use crate::parametrized;
        use crate::value::lambda::Lambda;
        parametrized![$( $var ),* => $result].map(Lambda)
    }};
    ( $( $var:expr ),* => $value:expr ; parent = $parent:expr ) => {{
        use crate::value::ValId;
        ValId::from(lambda![value $( $var ),* => $value; parent = $parent].unwrap())
    }};
    ( $( $var:expr ),* => $value:expr ) => {{
        use crate::value::ValId;
        ValId::from(lambda![value $( $var ),* => $value].unwrap())
    }};
}

/// A lambda function
#[derive(Debug, Clone, PartialEq, Hash, Eq)]
pub struct Lambda(pub Parametrized<ValId>);

impl HasRegion for Lambda {
    type RegionError = Infallible;
    fn region(&self) -> Result<Cow<WeakRegion>, Infallible> {
        self.0.region()
    }
}

impl Lambda {
    /// Create a new lambda function with the given results from the given `Region`
    pub fn new(region: Region, result: ValId) -> Result<Lambda, IncomparableRegions> {
        Parametrized::new(region, result).map(Lambda)
    }
    /// Get the region in which this lambda function's results are defined
    pub fn def_region(&self) -> &Region {
        self.0.def_region()
    }
    /// Get the results of this lambda function
    pub fn result(&self) -> &ValId {
        self.0.value()
    }
    /// Get the dependencies of this lambda function
    pub fn dependencies(&self) -> std::slice::Iter<ValId> {
        self.0.dependencies()
    }
}

impl From<Lambda> for ValueEnum {
    fn from(lambda: Lambda) -> ValueEnum {
        ValueEnum::Lambda(lambda)
    }
}

impl From<Lambda> for ValueData {
    fn from(lambda: Lambda) -> ValueData {
        ValueData::new(lambda)
    }
}

impl From<Lambda> for ValId {
    fn from(lambda: Lambda) -> ValId {
        ValId::try_new(ValueData::from(lambda)).expect("Impossible")
    }
}

impl Typed for Lambda {
    type TypeError = ValueError;
    type InferenceError = NotEnoughInformation;
    #[inline]
    fn is_ty(&self) -> bool {
        false
    }
    fn infer_ty(&self) -> Result<TypeId, NotEnoughInformation> {
        let inferred = self.result().infer_ty()?;
        let parametrized = Parametrized::new(self.def_region().clone(), inferred)
            .expect("Impossible: type cannot be in subregion!");
        Ok(Pi(parametrized).to_type().aok())
    }
    fn ty(&self) -> Result<TypeId, ValueError> {
        let typed = self.result().ty()?;
        let parametrized = Parametrized::new(self.def_region().clone(), typed)
            .map_err(|err| ValueError::from(err))?; //TODO: panic?
        Pi(parametrized).to_type().to_fall()
    }
}

impl SubstituteValue<Lambda> for Lambda {
    fn substitute<F>(&self, f: F) -> Result<Lambda, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        self.0.substitute(f).map(Lambda)
    }
}

impl SubstituteToValId for Lambda {}

impl DisplayValue for Lambda {
    fn display_value<N>(
        &self,
        fmt: &mut Formatter,
        printer: &mut PrettyPrinter<N>,
    ) -> Result<(), fmt::Error>
    where
        N: Display + From<usize>,
    {
        // Print lambda external dependencies first
        printer.prettyprint_level_deps(fmt, self.result(), self.def_region().depth - 1)?;
        write!(fmt, "#lambda")?;
        printer.prettyprint_params(fmt, self.def_region())?; // Print the parameters of this lambda
        write!(fmt, " ")?;
        // Register the dependencies of this lambda's result
        printer.prettyprint_deps(fmt, self.result())?;
        if printer.current_scope_entered() { // If the current scope is entered, print tabs before printing value
            printer.print_curr_tabs(fmt)?;
        }
        printer.prettyprint_deep(fmt, self.result())?;
        printer.pop(fmt)?; // Pop this lambda's scope
        Ok(())
    }
}

impl Display for Lambda {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        self.display_value(fmt, &mut PrettyPrinter::<TempReg>::default())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::graph::node::Downgrade;
    use crate::value::primitive::logical::Bool;
    use crate::{assert_jeq, region};
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne};
    use smallvec::smallvec;

    #[test]
    fn invalid_nested_lambda_result_fails() {
        let region = Region::new(&[][..]);
        let nested_region = region![Bool; parent = region.downgrade()];
        let y = &nested_region.params()[0];
        let _err = Lambda::new(region.clone(), y.clone()).expect_err("This is an invalid function");
    }

    #[test]
    fn constant_bool_lambda() {
        let region = Region::empty();
        let t = ValId::from(true);
        let lambda = Lambda::new(region.clone(), t.clone()).expect("This is a valid function");
        assert_eq!(lambda.def_region(), &region);
        assert_jeq!(lambda.result(), &t);
        let lambda = ValId::from(lambda);
        //TODO: application
        let _ = lambda;
    }

    #[test]
    fn identity_bool_lambda() {
        let region = region![Bool];
        let x = &region.params()[0];
        let lambda = Lambda::new(region.clone(), x.clone()).expect("This is a valid function");
        assert_eq!(lambda.def_region(), &region);
        assert_eq!(lambda.result(), x);
        let lambda = ValId::from(lambda);
        //TODO: application
        let _ = lambda;
    }

    #[test]
    fn lambda_macro_bool_test() {
        let _binary = lambda![Bool, Bool => |_| true];
        let _unary = lambda![Bool => |_| Bool];
        let region = Region::empty();
        let _binary_r = lambda![Bool, Bool => |_| false; parent = region.downgrade()];
        let _unary_r = lambda![Bool => |_| true; parent = region.downgrade()];
    }
}
