/*!
Sum types
*/
use super::{
    error::ValueError,
    eval::{SubstituteToValId, SubstituteValue},
    typing::Typed,
    universe::Universe,
    TypeId, ValId, ValueData, ValueEnum,
};
use crate::graph::error::IncomparableRegions;
use crate::prettyprinter::{DisplayValue, PrettyPrinter, TempReg};
use crate::region_by_dependencies;
use smallvec::SmallVec;
use std::convert::{Infallible, TryFrom, TryInto};
use std::fmt::{self, Display, Formatter};
use std::ops::{Deref, DerefMut};
use std::slice::Iter;

/// An injection into a sum-like type
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Injection {
    /// The type being injected into
    target: TypeId,
    /// The value of the injection
    value: ValId,
    /// The index of the injection
    //TODO: label injection, just like label projection.
    ix: usize,
}

/// The size of a small sum type, which will avoid allocating
pub const SMALL_SUM_SIZE: usize = 3;

/// A `SmallVec` of `TypeId`s composing a sum type
pub type SumVec = SmallVec<[TypeId; SMALL_SUM_SIZE]>;

/// A sum type
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Sum(pub SumVec);

impl Sum {
    /// Get the dependencies of this sum
    pub fn dependencies(&self) -> Iter<TypeId> {
        self.0.iter()
    }
    /// Get the empty type
    pub fn empty_ty() -> Sum { Sum(SmallVec::new()) }
}

region_by_dependencies!(Sum);

impl Deref for Sum {
    type Target = SumVec;
    #[inline]
    fn deref(&self) -> &SumVec {
        &self.0
    }
}

impl DerefMut for Sum {
    #[inline]
    fn deref_mut(&mut self) -> &mut SumVec {
        &mut self.0
    }
}

impl SubstituteValue<Sum> for Sum {
    fn substitute<F>(&self, mut f: F) -> Result<Sum, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        let new_elems: Result<_, _> = self
            .0
            .iter()
            .map(|t| Ok(TypeId::try_from(f(t)?)?))
            .collect();
        new_elems.map(Sum)
    }
}

impl SubstituteToValId for Sum {}

impl From<Sum> for ValueEnum {
    fn from(sum: Sum) -> ValueEnum {
        ValueEnum::Sum(sum)
    }
}

impl TryFrom<Sum> for TypeId {
    type Error = IncomparableRegions;
    fn try_from(sum: Sum) -> Result<TypeId, IncomparableRegions> {
        Ok(TypeId::try_from(ValId::try_from(sum)?).expect("Sum is always a type"))
    }
}

impl TryFrom<Sum> for ValId {
    type Error = IncomparableRegions;
    fn try_from(sum: Sum) -> Result<ValId, IncomparableRegions> {
        ValId::try_new(ValueData::try_new(sum)?).map_err(|err| err.try_into().expect("Impossible"))
    }
}

impl Typed for Sum {
    type TypeError = Infallible;
    type InferenceError = Infallible;
    fn universe(&self) -> Option<Universe> {
        Universe::union_all(self.dependencies().map(|dep| dep.universe()))
            .map(|universe| universe.base_level())
            .unwrap_or(Universe::finite())
            .into()
    }
    fn is_ty(&self) -> bool {
        true
    }
    fn infer_ty(&self) -> Result<TypeId, Infallible> {
        Ok(self.universe().unwrap().into())
    }
    fn ty(&self) -> Result<TypeId, Infallible> {
        self.universe().unwrap().try_into()
    }
}

impl DisplayValue for Sum {
    fn display_value<N>(
        &self,
        fmt: &mut Formatter,
        printer: &mut PrettyPrinter<N>,
    ) -> Result<(), fmt::Error>
    where
        N: Display + From<usize>,
    {
        if self.len() == 0 {
            return write!(fmt, "#empty")
        }
        write!(fmt, "(#sum")?;
        for element in self.iter() {
            write!(fmt, " ")?;
            printer.prettyprint_deep(fmt, element)?;
        }
        write!(fmt, ")")
    }
}

impl Display for Sum {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        self.display_value(fmt, &mut PrettyPrinter::<TempReg>::default())
    }
}
