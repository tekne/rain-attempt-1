/*!
Pi types
*/

use super::{
    error::ValueError,
    eval::{EvalCtx, SubstituteToValId, SubstituteValue},
    parametrized::Parametrized,
    typing::Typed,
    universe::Universe,
    Type, TypeId, ValId, ValueData, ValueEnum,
};
use crate::graph::{
    error::IncomparableRegions,
    region::{HasRegion, Region, WeakRegion},
};
use crate::prettyprinter::{DisplayValue, PrettyPrinter, TempReg};
use std::borrow::Cow;
use std::convert::{Infallible, TryFrom};
use std::fmt::{self, Display, Formatter};
use std::iter::Peekable;

/// A pi function
#[derive(Debug, Clone, PartialEq, Hash, Eq)]
pub struct Pi(pub Parametrized<TypeId>);

impl HasRegion for Pi {
    type RegionError = Infallible;
    fn region(&self) -> Result<Cow<WeakRegion>, Infallible> {
        self.0.region()
    }
}

/// Create a pi type
#[macro_export]
macro_rules! pi {
    ( value $( $var:expr ),* => $result:expr ; parent = $parent:expr ) => {{
        use crate::parametrized;
        use crate::value::pi::Pi;
        parametrized![$( $var ),* => $result ; parent = $parent ].map(Pi)
    }};
    ( value $( $var:expr ),* => $result:expr ) => {{
        use crate::parametrized;
        use crate::value::pi::Pi;
        parametrized![$( $var ),* => $result].map(Pi)
    }};
    ( $( $var:expr ),* => $val:expr ; parent = $parent:expr ) => {{
        use crate::value::TypeId;
        TypeId::from(pi![value $( $var ),* => $val; parent = $parent ].unwrap())
    }};
    ( $( $var:expr ),* => $val:expr ) => {{
        use crate::value::TypeId;
        TypeId::from(pi![value $( $var ),* => $val].unwrap())
    }};
}

impl Pi {
    /// Create a new pi type with the given results from the given `Region`
    pub fn new(region: Region, result: TypeId) -> Result<Pi, IncomparableRegions> {
        Parametrized::new(region, result).map(Pi)
    }
    /// Get the result types of this pi type
    pub fn result(&self) -> &TypeId {
        self.0.value()
    }
    /// Get the region in which this pi type's results are defined
    pub fn def_region(&self) -> &Region {
        self.0.def_region()
    }
    /// Get the dependencies of this pi type
    pub fn dependencies(&self) -> std::slice::Iter<TypeId> {
        self.0.dependencies()
    }
    /// Partially apply a pi type
    pub fn partial<'a, I>(&self, args: I) -> Result<TypeId, ValueError>
    where
        I: Iterator<Item = &'a ValId>,
    {
        self.apply_to_value(args.peekable())
    }
}

impl From<Pi> for ValueEnum {
    fn from(pi: Pi) -> ValueEnum {
        ValueEnum::Pi(pi)
    }
}

impl From<Pi> for ValueData {
    fn from(pi: Pi) -> ValueData {
        ValueData::new(pi)
    }
}

impl From<Pi> for ValId {
    fn from(pi: Pi) -> ValId {
        ValId::try_new(ValueData::from(pi)).expect("Impossible")
    }
}

impl From<Pi> for TypeId {
    fn from(pi: Pi) -> TypeId {
        TypeId::try_from(ValId::from(pi)).expect("Impossible")
    }
}

impl Typed for Pi {
    type TypeError = Infallible;
    type InferenceError = Infallible;
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    fn infer_ty(&self) -> Result<TypeId, Infallible> {
        self.ty()
    }
    fn ty(&self) -> Result<TypeId, Infallible> {
        Ok(
            Universe::union_all(self.dependencies().map(|dep| dep.universe()))
                .map(|universe| universe.base_level())
                .unwrap_or(Universe::finite())
                .into(),
        )
    }
}

impl Type for Pi {
    fn apply_to_value<'a, I>(&self, mut args: Peekable<I>) -> Result<TypeId, ValueError>
    where
        I: Iterator<Item = &'a ValId>,
    {
        let region = self.def_region();
        let mut ctx = EvalCtx::with_capacity_for(&region);
        let partial = region.partial((&mut args).cloned(), &mut ctx)?;
        let result = TypeId::try_from(ctx.eval(self.result())?)?;
        if let Some(region) = partial {
            Ok(Pi::new(region, result)?.into())
        } else {
            result.apply_to_value(args)
        }
    }
}

impl SubstituteValue<Pi> for Pi {
    fn substitute<F>(&self, f: F) -> Result<Pi, ValueError>
    where
        F: FnMut(&ValId) -> Result<ValId, ValueError>,
    {
        self.0.substitute(f).map(Pi)
    }
}

impl SubstituteToValId for Pi {}

impl DisplayValue for Pi {
    fn display_value<N>(
        &self,
        fmt: &mut Formatter,
        printer: &mut PrettyPrinter<N>,
    ) -> Result<(), fmt::Error>
    where
        N: Display + From<usize>,
    {
        write!(fmt, "#pi")?;
        printer.prettyprint_params(fmt, self.def_region())?; // Print the parameters of this lambda
        write!(fmt, " ")?;
        // Register the dependencies of this lambda's result
        printer.prettyprint_deps(fmt, self.result())?;
        if printer.current_scope_entered() { // If the current scope is entered, print tabs before printing value
            printer.print_curr_tabs(fmt)?;
        }
        printer.prettyprint_deep(fmt, self.result())?;
        printer.pop(fmt)?; // Pop this lambda's scope
        Ok(())
    }
}

impl Display for Pi {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        self.display_value(fmt, &mut PrettyPrinter::<TempReg>::default())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::graph::node::Downgrade;
    use crate::value::{primitive::logical::Bool, universe::Universe};
    use crate::{assert_jeq, region};
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne};
    use smallvec::smallvec;
    use std::convert::TryInto;

    #[test]
    fn invalid_nested_pi_result_fails() {
        let region = Region::empty();
        let nested_region = region![Universe::finite(); parent = region.downgrade()];
        let y = &nested_region.params()[0];
        let _err = Pi::new(region.clone(), y.clone().try_into().expect("Valid type"))
            .expect_err("This is an invalid function");
    }

    #[test]
    fn constant_bool_pi() {
        let region = Region::empty();
        let pi = Pi::new(region.clone(), Bool.into()).expect("This is a valid function");
        assert_eq!(pi.0.def_region(), &region);
        assert_jeq!(pi.result(), &Bool);
        let pi = ValId::from(pi);
        //TODO: application
        let _ = pi;
    }

    #[test]
    fn pi_partial_application_test() {
        let binary = pi![Bool, Bool => |_| Bool];
        let unary = pi![Bool => |_| Bool];
        let args: [ValId; 2] = [true.into(), false.into()];
        assert_eq!(
            binary
                .apply_to_value(args[..1].iter().peekable())
                .expect("Valid"),
            unary
        );
        assert_eq!(
            binary
                .apply_to_value(args.iter().peekable())
                .expect("Valid"),
            TypeId::from(Bool)
        );
        assert_eq!(
            unary
                .apply_to_value(args[..1].iter().peekable())
                .expect("Valid"),
            TypeId::from(Bool)
        );
        assert!(unary.apply_to_value(args.iter().peekable()).is_err());
        let region = Region::empty();
        let binary_r = pi![Bool, Bool => |_| Bool; parent = region.downgrade()];
        let unary_r = pi![Bool => |_| Bool; parent = region.downgrade()];
        assert_eq!(
            binary_r
                .apply_to_value(args[..1].iter().peekable())
                .expect("Valid"),
            unary_r
        );
        assert_eq!(
            binary_r
                .apply_to_value(args.iter().peekable())
                .expect("Valid"),
            TypeId::from(Bool)
        );
        assert_eq!(
            unary_r
                .apply_to_value(args[..1].iter().peekable())
                .expect("Valid"),
            TypeId::from(Bool)
        );
        assert!(unary_r.apply_to_value(args.iter().peekable()).is_err());
        assert_ne!(binary_r, binary);
        assert_ne!(unary_r, unary);
    }
}
