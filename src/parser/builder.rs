/*!
AST-to-`rain`-IR conversion and related utilities
*/
use super::ast::{
    Expr, Lambda as LambdaExpr, Let, Path, Pattern, Pi as PiExpr, Scope, Sexpr, SimpleAssignment,
    Tuple as TupleExpr, Universe as UniverseExpr,
};
use super::{parse_expr, parse_statement};
use crate::graph::{
    error::IncomparableRegions,
    node::Downgrade,
    region::{ParamTyVec, Region},
};
use crate::util::symbol_table::SymbolTable;
use crate::value::{
    error::{NotAType, ValueError},
    expr::SexprArgs,
    lambda::Lambda,
    parametrized::Parametrized,
    pi::Pi,
    sum::Sum,
    tuple::{Product, Tuple},
    typing::Typed,
    universe::Universe,
    TypeId, ValId, ValueDesc,
};
use nom::error::ErrorKind;
use nom::Err;
use smallvec::{smallvec, SmallVec};
use std::borrow::Borrow;
use std::collections::hash_map::RandomState;
use std::convert::{Infallible, TryInto};
use std::hash::{BuildHasher, Hash};

/// A rain IR builder which consumes an AST and outputs IR, along with handling errors
#[derive(Debug, Clone)]
pub struct Builder<S: Eq + Hash, B: BuildHasher = RandomState> {
    symbols: SymbolTable<S, ValId, B>,
    parents: Vec<Region>,
}

/// Errors building `rain` IR
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum BuilderError<'a> {
    /// A value construction error
    ValueError(ValueError),
    /// Symbol not defined
    SymbolUndefined(&'a str),
    /// An operator is in an invalid position (e.g. a `jeq` outside an `Expr`)
    InvalidOperatorPosition,
    /// A parse error
    ParseError(Err<(&'a str, ErrorKind)>),
    /// Build not implemented
    BuildNotImplemented,
}

impl<'a> From<ValueError> for BuilderError<'a> {
    fn from(err: ValueError) -> BuilderError<'a> {
        BuilderError::ValueError(err)
    }
}

impl<'a> From<NotAType> for BuilderError<'a> {
    fn from(err: NotAType) -> BuilderError<'a> {
        BuilderError::ValueError(err.into())
    }
}

impl<'a> From<IncomparableRegions> for BuilderError<'a> {
    fn from(err: IncomparableRegions) -> BuilderError<'a> {
        BuilderError::ValueError(err.into())
    }
}

impl<'a> From<Infallible> for BuilderError<'a> {
    fn from(err: Infallible) -> BuilderError<'a> {
        match err {}
    }
}

impl<'a> From<Err<(&'a str, ErrorKind)>> for BuilderError<'a> {
    fn from(err: Err<(&'a str, ErrorKind)>) -> BuilderError<'a> {
        BuilderError::ParseError(err)
    }
}

impl<'a> BuilderError<'a> {
    /// Get the builder error corresponding to a given value error
    pub fn val<E: Into<ValueError>>(err: E) -> BuilderError<'a> {
        BuilderError::ValueError(err.into())
    }
}

/// The size of a small vector of definitions
pub const SMALL_DEFINITIONS: usize = 1;

/// A symbol definition, along with the previous definition, if any
#[derive(Debug, Clone)]
pub struct Definition<'a> {
    /// The name of the symbol defined
    pub name: &'a str,
    /// The value it is defined to have
    pub value: ValId,
    /// The old value it had before, if any
    pub previous: Option<ValId>,
}

impl<'a, S: Eq + Hash + From<&'a str>> Builder<S> {
    /// Create a new IR builder
    pub fn new() -> Builder<S> {
        Builder {
            symbols: SymbolTable::default(),
            parents: Vec::new(),
        }
    }
}

impl<'a, S: Eq + Hash + From<&'a str>, B: BuildHasher + Default> Default for Builder<S, B> {
    fn default() -> Builder<S, B> {
        Builder {
            symbols: SymbolTable::default(),
            parents: Vec::new(),
        }
    }
}

impl<'a, S: Eq + Hash + From<&'a str> + Borrow<str>, B: BuildHasher> Builder<S, B> {
    /// Build a let expression, registering the symbols defined in the current scope
    /// Returns a list of symbols defined
    pub fn build_let(
        &mut self,
        let_statement: &Let<'a>,
    ) -> Result<SmallVec<[Definition<'a>; SMALL_DEFINITIONS]>, BuilderError<'a>> {
        self.assign_pattern(&let_statement.pattern, &let_statement.expr)
    }
    /// Assign an expression to a pattern
    /// Returns a list of symbols defined, along with previous definitions (if any)
    pub fn assign_pattern(
        &mut self,
        pattern: &Pattern<'a>,
        expr: &Expr<'a>,
    ) -> Result<SmallVec<[Definition<'a>; SMALL_DEFINITIONS]>, BuilderError<'a>> {
        match pattern {
            Pattern::Simple(simple_assignment) => self
                .build_simple_assign(simple_assignment, expr)
                .map(|assigned| smallvec![assigned]),
        }
    }
    /// Build a simple assignment.
    pub fn build_simple_assign(
        &mut self,
        simple: &SimpleAssignment<'a>,
        expr: &Expr<'a>,
    ) -> Result<Definition<'a>, BuilderError<'a>> {
        //TODO: typing
        let name = simple.name;
        let value = self.build_expr(expr)?;
        let previous = self.symbols.def(S::from(name), value.clone());
        Ok(Definition {
            name,
            value,
            previous,
        })
    }
    /// Build the value for an expression
    pub fn build_expr(&mut self, expr: &Expr<'a>) -> Result<ValId, BuilderError<'a>> {
        use Expr::*;
        let result = match expr {
            Bool(b) => b.to_node()?,
            BoolTy(b) => b.to_node()?,
            LogicalOp(l) => l.to_node()?,
            Path(p) => self.lookup_path(p)?,
            Sexpr(s) => self.build_sexpr(s)?,
            Scope(s) => self.build_scope(s)?,
            Lambda(l) => self.build_lambda(l)?.into(),
            Pi(p) => self.build_pi(p)?.into(),
            Tuple(t) => self.build_tuple(t)?.try_into()?,
            TypeOf(e) => self.build_typeof(&e.0).map(|(_value, ty)| ty.into())?,
            Universe(u) => self.build_universe(*u)?.into(),
            Jeq(_) | Product(_) | Sum(_) => Err(BuilderError::InvalidOperatorPosition)?,
            Finite(f) => (*f).into(), //TODO: consider the function Finite: \mathbb{N} \to \mathcal{U}
            Index(i) => (*i).into(),
            _ => Err(BuilderError::BuildNotImplemented)?,
        };
        Ok(result)
    }
    /// Build the value for a scope
    pub fn build_scope(&mut self, scope: &Scope<'a>) -> Result<ValId, BuilderError<'a>> {
        self.symbols.push(); // Push a new scope
                             // Build definitions
        for def in scope.definitions.iter() {
            self.build_let(def).map_err(|err| {
                self.symbols.pop();
                err
            })?;
        }
        // Build value, if any
        let res = if let Some(value) = &scope.value {
            self.build_expr(value)
        } else {
            //TODO: module return
            Err(BuilderError::BuildNotImplemented)
        };
        self.symbols.pop(); // Pop the scope
        res
    }
    /// Look up a path
    pub fn lookup_path(&self, path: &Path<'a>) -> Result<ValId, BuilderError<'a>> {
        if path.names.len() == 1 {
            let name = path.names[0];
            self.symbols
                .get(name)
                .cloned()
                .ok_or(BuilderError::SymbolUndefined(name))
        } else {
            Err(BuilderError::BuildNotImplemented)
        }
    }
    /// Build the value for an S-expression
    pub fn build_sexpr(&mut self, sexpr: &Sexpr<'a>) -> Result<ValId, BuilderError<'a>> {
        if sexpr.ops.len() > 1 {
            // Check for special tokens
            let nargs = sexpr.ops.len() - 1;
            match sexpr.ops[nargs] {
                Expr::Product(_p) => {
                    let product = self.build_product(&sexpr.ops[..nargs])?.try_into()?;
                    return Ok(product);
                }
                Expr::Sum(_s) => {
                    let sum = self.build_sum(&sexpr.ops[..nargs])?.try_into()?;
                    return Ok(sum);
                }
                Expr::Jeq(_j) => {
                    let (_value, jeq) = self.build_jeq(&sexpr.ops[..nargs])?;
                    return Ok(ValId::from(jeq));
                }
                _ => {}
            }
        }
        let args = self.build_sexpr_args(sexpr)?;
        let sexpr = args.normalize().map_err(BuilderError::ValueError)?;
        Ok(sexpr.to_node()?)
    }
    /// Build the arguments for an S-expression
    pub fn build_sexpr_args(&mut self, sexpr: &Sexpr<'a>) -> Result<SexprArgs, BuilderError<'a>> {
        let ops: Result<_, _> = sexpr.ops.iter().map(|op| self.build_expr(op)).collect();
        Ok(SexprArgs(ops?))
    }
    /// Build a type from an expression. It is an error if this is not a type.
    pub fn build_type(&mut self, expr: &Expr<'a>) -> Result<TypeId, BuilderError<'a>> {
        let value = self.build_expr(expr)?;
        Ok(value.try_into()?)
    }
    /// Build a pi type
    pub fn build_pi(&mut self, pi: &PiExpr<'a>) -> Result<Pi, BuilderError<'a>> {
        let params = &pi.0;
        let args: Result<ParamTyVec, _> = params
            .args
            .iter()
            .map(|(_name, ty)| self.build_type(ty))
            .collect();
        let args = args?;
        let parent = self
            .parents
            .last()
            .map(|r| r.downgrade())
            .unwrap_or_default();
        let region = Region::new_in(args, parent);
        self.symbols.push();
        for ((name, _ty), param) in params.args.iter().zip(region.params().iter()) {
            if let Some(name) = name {
                self.symbols.def(S::from(name), param.clone());
            }
        }
        self.parents.push(region.clone());
        let result = self.build_type(&params.result).map_err(|err| {
            self.symbols.pop();
            err
        })?;
        self.parents.pop();
        Ok(Pi(Parametrized::new(region, result)?))
    }
    /// Build a lambda expression
    pub fn build_lambda(&mut self, lambda: &LambdaExpr<'a>) -> Result<Lambda, BuilderError<'a>> {
        let params = &lambda.0;
        let args: Result<ParamTyVec, _> = params
            .args
            .iter()
            .map(|(_name, ty)| self.build_type(ty))
            .collect();
        let args = args?;
        let parent = self
            .parents
            .last()
            .map(|r| r.downgrade())
            .unwrap_or_default();
        let region = Region::new_in(args, parent);
        self.symbols.push();
        for ((name, _ty), param) in params.args.iter().zip(region.params().iter()) {
            if let Some(name) = name {
                self.symbols.def(S::from(name), param.clone());
            }
        }
        self.parents.push(region.clone());
        let result = self.build_expr(&params.result).map_err(|err| {
            self.symbols.pop();
            err
        })?;
        self.parents.pop();
        Ok(Lambda(Parametrized::new(region, result)?))
    }
    /// Build a tuple
    pub fn build_tuple(&mut self, tuple: &TupleExpr<'a>) -> Result<Tuple, BuilderError<'a>> {
        let tuple_args: Result<_, _> = tuple.0.iter().map(|arg| self.build_expr(arg)).collect();
        tuple_args.map(Tuple)
    }
    /// Build a product type
    pub fn build_product(&mut self, members: &[Expr<'a>]) -> Result<Product, BuilderError<'a>> {
        let members: Result<_, _> = members.iter().map(|arg| self.build_type(arg)).collect();
        members.map(Product)
    }
    /// Build a sum type
    pub fn build_sum(&mut self, members: &[Expr<'a>]) -> Result<Sum, BuilderError<'a>> {
        let members: Result<_, _> = members.iter().map(|arg| self.build_type(arg)).collect();
        members.map(Sum)
    }
    /// Build a universe
    pub fn build_universe(&mut self, universe: UniverseExpr) -> Result<Universe, BuilderError<'a>> {
        match (universe.level(), universe.kind()) {
            (Some(level), Some(kind)) => Ok(Universe::try_new(level, kind).expect("Impossible")),
            _ => unimplemented!(),
        }
    }
    /// Build the type of an expression, after building the expression (which is also returned)
    pub fn build_typeof(&mut self, expr: &Expr<'a>) -> Result<(ValId, TypeId), BuilderError<'a>> {
        let value = self.build_expr(expr)?;
        let ty = value.ty()?;
        Ok((value, ty))
    }
    /// Build a set of expressions and then check if they are all judgementally equal.
    /// If so, return one of them, if the expression list is nonempty.
    pub fn build_jeq(
        &mut self,
        members: &[Expr<'a>],
    ) -> Result<(Option<ValId>, bool), BuilderError<'a>> {
        if members.len() == 0 {
            return Ok((None, true));
        }
        let base = self.build_expr(&members[0])?;
        for expr in members[1..].iter() {
            if self.build_expr(expr)? != base {
                //TODO: is it OK to rely on normalization here?
                return Ok((None, false));
            }
        }
        Ok((Some(base), true))
    }
    /// Get this builder's symbol table
    pub fn symbols(&self) -> &SymbolTable<S, ValId, B> {
        &self.symbols
    }
    /// Mutably get this builder's symbol table
    pub fn symbols_mut(&mut self) -> &mut SymbolTable<S, ValId, B> {
        &mut self.symbols
    }
    /// Parse a `rain` expression
    pub fn parse_expr(&mut self, expr: &'a str) -> Result<(&'a str, ValId), BuilderError<'a>> {
        let (rest, expr) = parse_expr(expr)?;
        Ok((rest, self.build_expr(&expr)?))
    }
    /// Parse a `rain` statement
    pub fn parse_statement(
        &mut self,
        statement: &'a str,
    ) -> Result<(&'a str, SmallVec<[Definition<'a>; SMALL_DEFINITIONS]>), BuilderError<'a>> {
        let (rest, statement) = parse_statement(statement)?;
        Ok((rest, self.build_let(&statement)?))
    }
    /// Parse a collection of `rain` statements
    pub fn parse_statements(
        &mut self,
        mut remainder: &'a str,
    ) -> Result<(&'a str, SmallVec<[Definition<'a>; SMALL_DEFINITIONS]>), BuilderError<'a>> {
        let mut result = SmallVec::new();
        while remainder != "" {
            match self.parse_statement(remainder) {
                Ok((rest, definitions)) => {
                    remainder = rest;
                    result.extend(definitions)
                }
                Err(BuilderError::ParseError(Err::Failure(_)))
                | Err(BuilderError::ParseError(Err::Incomplete(_))) => return Ok((remainder, result)),
                Err(e) => return Err(e)
            }
        }
        Ok((remainder, result))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parser::{parse_expr, parse_statement};
    use crate::value::{primitive::logical::Bool, universe::Universe};
    use crate::{lambda, pi, tuple};
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne};
    use std::convert::TryFrom;

    #[test]
    fn simple_logical_expressions_build_properly() {
        let mut builder = Builder::<&str>::new();
        let t = &ValId::from(true);
        let f = &ValId::from(false);
        let logical_expressions = [
            ("(#and #true #false)", f),
            ("(#or #true #false)", t),
            ("(#not #true)", f),
            ("(#not #false)", t),
            ("(#xor #true #true)", f),
            ("(#or (#or #false #true) (#xor #false #false))", t),
        ];
        for (expr, result) in logical_expressions.iter() {
            let expr = match parse_expr(expr) {
                Ok((rest, expr)) => {
                    assert_eq!(
                        rest, "",
                        "Did not parse all of valid expression \"{}\": remainder = {:?}",
                        expr, rest
                    );
                    expr
                }
                Err(err) => panic!(
                    "Got parse error {:?} for valid expression \"{}\"",
                    err, expr
                ),
            };
            let built = match builder.build_expr(&expr) {
                Ok(built) => built,
                Err(err) => panic!(
                    "Valid expression \"{}\" gives build error: {:#?}",
                    expr, err
                ),
            };
            assert_eq!(&built, *result);
        }
    }

    #[test]
    fn simple_assignments_build_properly() {
        let mut builder = Builder::<&str>::new();
        let program = [
            "let false = (#or #false #false);",
            "let true = (#and #true (#and #true #true));",
            "let x = (#xor true false);",
            "let y = (#xor true true);",
        ];
        for line in program.iter() {
            let l = match parse_statement(line) {
                Ok((rest, l)) => {
                    assert_eq!(
                        rest, "",
                        "Unparsed portion {:?} of valid let \"{}\"",
                        rest, line
                    );
                    l
                }
                Err(err) => panic!("Valid let \"{}\" gives parse error: {:#?}", line, err),
            };
            match builder.build_let(&l) {
                Ok(_) => {}
                Err(err) => panic!("Valid let \"{}\" gives build error: {:#?}", line, err),
            };
        }
        let t = &ValId::from(true);
        let f = &ValId::from(false);
        let variables = [("true", t), ("false", f), ("x", t), ("y", f)];
        for (variable, value) in variables.iter() {
            match builder.build_expr(&parse_expr(variable).expect("Valid identifier").1) {
                Ok(built) => assert_eq!(
                    &built, *value,
                    "Invalid variable assignment {} = {} != {}",
                    variable, built, value
                ),
                Err(err) => panic!("Error looking up variable {}: {:#?}", variable, err),
            }
        }
    }

    #[test]
    fn identity_bool_lambda_builds_properly() {
        let mut builder = Builder::<&str>::new();
        let id = lambda![Bool => |x: &Region| x.params()[0].clone()];
        let (_rest, id_ast) =
            parse_expr("#lambda |x: #bool| { x }").expect("This is a valid lambda function");
        let built = builder
            .build_expr(&id_ast)
            .expect("This is a valid lambda AST");
        assert_eq!(id, built);
    }

    #[test]
    fn binary_bool_pi_builds_properly() {
        let mut builder = Builder::<&str>::new();
        let binary = pi![Bool, Bool => |_| Bool];
        let (_rest, bin_ast) =
            parse_expr("#pi |l: #bool, r: #bool| { #bool }").expect("This is a valid pi type");
        let built = builder
            .build_expr(&bin_ast)
            .expect("This is a valid pi AST");
        assert_eq!(binary, built);
        let (_rest, bin_ast) =
            parse_expr("#pi |_: #bool, _: #bool| { #bool }").expect("This is a valid pi type");
        let built = builder
            .build_expr(&bin_ast)
            .expect("This is a valid pi AST");
        assert_eq!(binary, built);
    }

    #[test]
    fn const_bool_lambda_builds_properly() {
        let mut builder = Builder::<&str>::new();
        let id = lambda![Bool => |_| true];
        let (_rest, id_ast) =
            parse_expr("#lambda |x: #bool| { #true }").expect("This is a valid lambda function");
        let built = builder
            .build_expr(&id_ast)
            .expect("This is a valid lambda AST");
        assert_eq!(id, built);
        let (_rest, id_ast) =
            parse_expr("#lambda |_: #bool| { #true }").expect("This is a valid lambda function");
        let built = builder
            .build_expr(&id_ast)
            .expect("This is a valid lambda AST");
        assert_eq!(id, built);
    }

    #[test]
    fn basic_tuples_build_properly() {
        let mut builder = Builder::<&str>::new();
        let null = tuple![];
        let single = tuple![true];
        let double = tuple![true, false];
        let triple = tuple![false, true, false];
        let (_, null_expr) = parse_expr("[]").unwrap();
        let (_, single_expr) = parse_expr("[ #true ]").unwrap();
        let (_, double_expr) = parse_expr("[#true #false]").unwrap();
        let (_, triple_expr) = parse_expr("[#false #true #false]").unwrap();
        assert_eq!(
            builder.build_expr(&null_expr).unwrap(),
            ValId::try_from(null).unwrap()
        );
        assert_eq!(
            builder.build_expr(&single_expr).unwrap(),
            ValId::try_from(single).unwrap()
        );
        assert_eq!(
            builder.build_expr(&double_expr).unwrap(),
            ValId::try_from(double).unwrap()
        );
        assert_eq!(
            builder.build_expr(&triple_expr).unwrap(),
            ValId::try_from(triple).unwrap()
        );
    }

    #[test]
    fn basic_bool_typeof_expressions_build_properly() {
        let mut builder = Builder::<&str>::new();
        let bsts = ["#typeof(#true)", "#typeof(#false)"];
        let b = ValId::from(Bool);
        let bttsts = ["#typeof(#bool)", "#typeof(#typeof(#true))"];
        let u = ValId::from(Universe::finite());
        for bst in bsts.iter() {
            let (rest, bool_expr) = parse_expr(bst).unwrap();
            assert_eq!(rest, "");
            let bool_value = builder.build_expr(&bool_expr).unwrap();
            assert_eq!(bool_value, b)
        }
        for btst in bttsts.iter() {
            let (rest, bool_expr) = parse_expr(btst).unwrap();
            assert_eq!(rest, "");
            let bool_value = builder.build_expr(&bool_expr).unwrap();
            assert_eq!(bool_value, u)
        }
    }

    #[test]
    fn operator_invalid_position_errors() {
        use crate::parser::ast::{Jeq, Product, Sum};
        let mut builder = Builder::<&str>::new();
        assert_eq!(
            builder.build_expr(&Expr::Jeq(Jeq)),
            Err(BuilderError::InvalidOperatorPosition)
        );
        assert_eq!(
            builder.build_expr(&Expr::Product(Product)),
            Err(BuilderError::InvalidOperatorPosition)
        );
        assert_eq!(
            builder.build_expr(&Expr::Sum(Sum)),
            Err(BuilderError::InvalidOperatorPosition)
        );
    }
    #[test]
    fn build_jeq_test() {
        let mut builder = Builder::<&str>::new();
        assert_eq!(builder.build_jeq(&[]), Ok((None, true)));
        assert_eq!(
            builder.build_jeq(&[parse_expr("#true").unwrap().1]),
            Ok((Some(ValId::from(true)), true))
        );
        assert_eq!(
            builder.build_jeq(&[
                parse_expr("#true").unwrap().1,
                parse_expr("(#implies #false #false)").unwrap().1
            ]),
            Ok((Some(ValId::from(true)), true))
        );
        assert_eq!(
            builder.build_jeq(&[
                parse_expr("#true").unwrap().1,
                parse_expr("(#implies #true #false)").unwrap().1
            ]),
            Ok((None, false))
        );
    }
}
