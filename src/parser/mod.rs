/*!
A simple parser, AST and prettyprinter for a textual representation of `rain` programs
*/
use nom::{
    branch::alt,
    bytes::complete::{is_a, is_not, tag},
    bytes::streaming::take_until,
    character::complete::{digit1, hex_digit1, not_line_ending, oct_digit1},
    character::streaming::multispace0,
    combinator::{complete, map, map_res, opt, recognize},
    multi::{many0, many0_count, separated_list, separated_nonempty_list},
    sequence::{delimited, preceded, separated_pair, terminated, tuple},
    IResult,
};
use num::BigUint;

pub mod ast;
use crate::value::primitive::{
    binary::{BinaryDisplay, Natural},
    logical::{Binary, Bool, LogicalOp, Unary},
    finite::{Finite, Index}
};
use ast::{
    Expr, Gamma, Jeq, Lambda, Let, Parametrized, Path, Pattern, Phi, Pi, Product, Scope, Sexpr,
    SimpleAssignment, Sum, Tuple, TypeOf, Universe,
};
pub mod builder;

macro_rules! special_chars {
    () => {
        " \t\r\n(){}[]|:.=;#,'\""
    };
}
macro_rules! digits {
    () => {
        "0123456789"
    };
}

const LET: &'static str = "let";
const DEFEQ: &'static str = "=";
const TERM: &'static str = ";";

/// Parse a single line `rain` comment
pub fn parse_single_comment(input: &str) -> IResult<&str, &str> {
    preceded(tag("//"), not_line_ending)(input)
}

/// Parse a multi line `rain` comment
pub fn parse_multi_comment(input: &str) -> IResult<&str, &str> {
    preceded(tag("/*"), take_until("*/"))(input)
}

/// Parse a `rain` comment. TODO: distinguish documentation comments, and don't ignore those
pub fn parse_comment(input: &str) -> IResult<&str, &str> {
    alt((parse_single_comment, parse_multi_comment))(input)
}

/// Parse whitespace, including ignored comments. Returns the count of comments.
pub fn whitespace(input: &str) -> IResult<&str, usize> {
    terminated(
        many0_count(preceded(multispace0, parse_comment)),
        multispace0,
    )(input)
}

/// Parse a `rain` identifier, which is composed of any non-special character.
/// Does *not* accept whitespace before the identifier, and does *not* consume whitespace after it.
pub fn parse_ident(input: &str) -> IResult<&str, &str> {
    recognize(tuple((
        is_not(concat!(special_chars!(), digits!())),
        opt(is_not(special_chars!())),
    )))(input)
}

/// Parse a path separator
pub fn path_separator(input: &str) -> IResult<&str, &str> {
    tag(".")(input)
}

/// Parse a `rain` path.
/// Does *not* accept whitespace before the path and does *not* consume whitespace after it.
pub fn parse_path(input: &str) -> IResult<&str, Path> {
    map(
        separated_nonempty_list(path_separator, parse_ident),
        |names| names.into(),
    )(input)
}

/// Parse a binary logical operation
pub fn parse_binary_logical_op(input: &str) -> IResult<&str, Binary> {
    use Binary::*;
    preceded(
        whitespace,
        alt((
            map(tag(And.get_string()), |_| And),
            map(tag(Or.get_string()), |_| Or),
            map(tag(Xor.get_string()), |_| Xor),
            map(tag(Nand.get_string()), |_| Nand),
            map(tag(Nor.get_string()), |_| Nor),
            map(tag(Eq.get_string()), |_| Eq),
            map(tag(Implies.get_string()), |_| Implies),
            map(tag(ImpliedBy.get_string()), |_| ImpliedBy),
        )),
    )(input)
}

/// Parse a unary logical operation
pub fn parse_unary_logical_op(input: &str) -> IResult<&str, Unary> {
    use Unary::*;
    preceded(
        whitespace,
        alt((
            map(tag(Id.get_string()), |_| Id),
            map(tag(Not.get_string()), |_| Not),
            map(tag(Constant(true).get_string()), |_| Constant(true)),
            map(tag(Constant(false).get_string()), |_| Constant(false)),
        )),
    )(input)
}

/// Parse a logical operation
pub fn parse_logical_op(input: &str) -> IResult<&str, LogicalOp> {
    use LogicalOp::*;
    alt((
        map(parse_binary_logical_op, Binary),
        map(parse_unary_logical_op, Unary),
    ))(input)
}

/// Parse the Boolean type
pub fn parse_bool_type(input: &str) -> IResult<&str, Bool> {
    map(preceded(whitespace, tag("#bool")), |_| Bool)(input)
}

/// Parse a boolean
pub fn parse_bool(input: &str) -> IResult<&str, bool> {
    preceded(
        whitespace,
        alt((map(tag("#true"), |_| true), map(tag("#false"), |_| false))),
    )(input)
}

/// Parse a natural number
pub fn parse_natural(input: &str) -> IResult<&str, Natural> {
    use BinaryDisplay::*;
    alt((
        map(preceded(tag("0b"), is_a("01")), |bytes: &str| {
            Natural(BigUint::parse_bytes(bytes.as_bytes(), 2).unwrap(), Bin)
        }),
        map(preceded(tag("0o"), oct_digit1), |bytes: &str| {
            Natural(BigUint::parse_bytes(bytes.as_bytes(), 8).unwrap(), Oct)
        }),
        map(preceded(tag("0x"), hex_digit1), |bytes: &str| {
            Natural(BigUint::parse_bytes(bytes.as_bytes(), 16).unwrap(), Hex)
        }),
        map(digit1, |bytes: &str| {
            Natural(BigUint::parse_bytes(bytes.as_bytes(), 10).unwrap(), Dec)
        }),
    ))(input)
}

/// Parse a tuple of `rain` values
pub fn parse_tuple(input: &str) -> IResult<&str, Tuple> {
    delimited(tag("["), parse_inner_tuple, tag("]"))(input)
}

/// Parse the inside of a tuple of `rain` values
pub fn parse_inner_tuple(input: &str) -> IResult<&str, Tuple> {
    map(
        preceded(whitespace, many0(terminated(parse_atom, whitespace))),
        Tuple,
    )(input)
}

/// Parse a sum token
pub fn parse_sum(input: &str) -> IResult<&str, Sum> {
    map(tag(Sum::SUM), |_| Sum)(input)
}

/// Parse a product token
pub fn parse_product(input: &str) -> IResult<&str, Product> {
    map(tag(Product::PRODUCT), |_| Product)(input)
}

/// Parse a u64
pub fn parse_small_nat(input: &str) -> IResult<&str, u64> {
    alt((
        map_res(preceded(tag("0b"), is_a("01")), |s| {
            u64::from_str_radix(s, 2)
        }),
        map_res(preceded(tag("0o"), oct_digit1), |s| {
            u64::from_str_radix(s, 8)
        }),
        map_res(preceded(tag("0x"), hex_digit1), |s| {
            u64::from_str_radix(s, 16)
        }),
        map_res(digit1, |s| u64::from_str_radix(s, 10)),
    ))(input)
}

/// Parse a typing universe
pub fn parse_universe(input: &str) -> IResult<&str, Universe> {
    alt((
        map(tag(Universe::FINITE_UNIVERSE), |_| Universe::finite()),
        map(tag(Universe::SIMPLE_UNIVERSE), |_| Universe::simple()),
        map_res(
            preceded(
                tag(Universe::UNIVERSE),
                opt(tuple((
                    preceded(tag("("), whitespace),
                    parse_small_nat,
                    opt(preceded(preceded(tag(","), whitespace), parse_small_nat)),
                    preceded(whitespace, tag(")")),
                ))),
            ),
            |levels| {
                if let Some((_, l, k, _)) = levels {
                    Universe::try_new(Some(l as usize), k.map(|k| k as usize))
                } else {
                    Universe::try_new(None, None)
                }
            },
        ),
    ))(input)
}

/// Parse a typeof expression
pub fn parse_typeof(input: &str) -> IResult<&str, TypeOf> {
    map(
        delimited(
            preceded(tag(TypeOf::TYPEOF), tag("(")),
            parse_expr,
            preceded(whitespace, tag(")")),
        ),
        |expr| TypeOf(Box::new(expr)),
    )(input)
}

/// Parse a judgemental equality token
pub fn parse_jeq(input: &str) -> IResult<&str, Jeq> {
    map(tag(Jeq::JEQ), |_| Jeq)(input)
}

/// Parse a finite `rain` type
pub fn parse_finite(input: &str) -> IResult<&str, Finite> {
    map(
        delimited(
            preceded(tag(Finite::FINITE), tag("(")),
            preceded(whitespace, parse_small_nat),
            preceded(whitespace, tag(")"))
        ),
        |n| Finite(n)
    )(input)
}

/// Parse an element of a finite `rain` type
pub fn parse_index(input: &str) -> IResult<&str, Index> {
    map_res(
        delimited(
            preceded(tag(Index::INDEX), tag("(")),
            tuple((
                preceded(whitespace, parse_small_nat),
                preceded(delimited(whitespace, tag(","), whitespace), parse_small_nat)
            )),
            preceded(whitespace, tag(")"))
        ),
        |(i, t)| Index::try_new(Finite(t), i)
    )(input)
}

/// Parse an atomic `rain` expression
/// Does *not* accept whitespace before the expression, and does *not* consume whitespace after it.
pub fn parse_atom(input: &str) -> IResult<&str, Expr> {
    alt((
        map(parse_small_nat, Expr::SmallNat),   // Small natural values
        map(parse_natural, Expr::Natural),      // Natural values
        map(parse_bool, Expr::Bool),            // Boolean values
        map(parse_logical_op, Expr::LogicalOp), // Binary logical operations
        map(parse_bool_type, Expr::BoolTy),     // Parse the boolean type
        map(parse_phi, Expr::Phi),              // Phis
        map(parse_lambda, Expr::Lambda),        // Lambdas
        map(parse_gamma, Expr::Gamma),          // Match statements
        map(parse_pi, Expr::Pi),                // Pi types
        map(parse_path, Expr::Path),            // Paths
        map(parse_scope, Expr::Scope),          // Scopes
        map(parse_tuple, Expr::Tuple),          // Tuples
        map(parse_sum, Expr::Sum),              // Sum token
        map(parse_product, Expr::Product),      // Product token
        map(parse_typeof, Expr::TypeOf),        // TypeOf parsing
        map(parse_universe, Expr::Universe),    // Universe parsing
        map(parse_jeq, Expr::Jeq),              // Judgemental equality parsing
        map(parse_finite, Expr::Finite),        // Finite parsing
        map(parse_index, Expr::Index),          // Index parsing
        delimited(tag("("), parse_expr, preceded(whitespace, tag(")"))), // S-expressions
    ))(input)
}

/// Parse a `rain` expression, which is either an atom or an application of them
/// Accepts whitespace before the expression
pub fn parse_expr(input: &str) -> IResult<&str, Expr> {
    map(
        tuple((
            preceded(whitespace, parse_atom),
            many0(preceded(complete(whitespace), parse_atom)),
        )),
        |(first, mut ops)| {
            if ops.len() == 0 {
                first
            } else {
                ops.reverse();
                ops.push(first);
                Expr::Sexpr(Sexpr { ops })
            }
        },
    )(input)
}

/// Parse a type bound
pub fn parse_type_bound(input: &str) -> IResult<&str, Expr> {
    preceded(preceded(whitespace, tag(":")), parse_expr)(input)
}

/// Parse a simple assignment. Accepts whitespace before it
pub fn parse_simple_assignment(input: &str) -> IResult<&str, SimpleAssignment> {
    map(
        tuple((whitespace, parse_ident, opt(parse_type_bound))),
        |(_, name, ty)| SimpleAssignment { name, ty },
    )(input)
}

/// Parse a pattern for assignment
/// Accepts whitespace before the pattern
pub fn parse_pattern(input: &str) -> IResult<&str, Pattern> {
    map(
        preceded(whitespace, parse_simple_assignment),
        Pattern::Simple,
    )(input) //TODO: this
}

/// Parse an equality separator, optionally preceded by whitespace
pub fn defeq(input: &str) -> IResult<&str, &str> {
    preceded(whitespace, tag(DEFEQ))(input)
}

/// Parse a terminator, optionally preceded by whitespace
pub fn terminator(input: &str) -> IResult<&str, &str> {
    preceded(whitespace, tag(TERM))(input)
}

/// Parse a `let`-statement
pub fn parse_statement(input: &str) -> IResult<&str, Let> {
    delimited(
        preceded(whitespace, tag(LET)),
        map(
            separated_pair(parse_pattern, defeq, parse_expr),
            |(pattern, expr)| Let { pattern, expr },
        ),
        terminator,
    )(input)
}

/// Parse a scope, consuming all whitespace before it
pub fn parse_scope(input: &str) -> IResult<&str, Scope> {
    map(
        delimited(
            preceded(whitespace, tag("{")),
            tuple((many0(parse_statement), opt(parse_expr))),
            preceded(whitespace, tag("}")),
        ),
        |(definitions, value)| Scope {
            definitions,
            value: value.map(Box::new),
        },
    )(input)
}

/// Parse a phi node, consuming all whitespace beforeit
pub fn parse_phi(input: &str) -> IResult<&str, Phi> {
    map(
        preceded(preceded(whitespace, tag("#phi")), parse_scope),
        Phi,
    )(input)
}

/// Parse an optional ident
pub fn parse_opt_ident(input: &str) -> IResult<&str, Option<&str>> {
    alt((map(tag("_"), |_| None), map(parse_ident, Some)))(input)
}

/// Parse a list of typed identifiers
pub fn parse_typed_idents(input: &str) -> IResult<&str, Vec<(Option<&str>, Expr)>> {
    separated_nonempty_list(
        delimited(whitespace, tag(","), whitespace),
        tuple((parse_opt_ident, parse_type_bound)),
    )(input)
}

/// Parse a list of typed arguments
pub fn parse_typed_args(input: &str) -> IResult<&str, Vec<(Option<&str>, Expr)>> {
    delimited(
        preceded(whitespace, tag("|")),
        parse_typed_idents,
        preceded(whitespace, tag("|")),
    )(input)
}

/// Parse a function type arrow
pub fn parse_type_arrow(input: &str) -> IResult<&str, Expr> {
    preceded(delimited(whitespace, tag("=>"), whitespace), parse_atom)(input)
}

/// Parse a lambda function, consuming all whitespace before it
pub fn parse_lambda(input: &str) -> IResult<&str, Lambda> {
    map(
        preceded(preceded(whitespace, tag("#lambda")), parse_parametrized),
        |p| Lambda(p),
    )(input)
}

/// Parse a pi type, consuming all whitespace before it
pub fn parse_pi(input: &str) -> IResult<&str, Pi> {
    map(
        preceded(preceded(whitespace, tag("#pi")), parse_parametrized),
        |p| Pi(p),
    )(input)
}

/// Parse a parametrized expression
pub fn parse_parametrized(input: &str) -> IResult<&str, Parametrized> {
    map(
        tuple((parse_typed_args, opt(parse_type_arrow), parse_expr)),
        |(args, ret_ty, result)| Parametrized {
            args,
            result: Box::new(result),
            ret_ty: ret_ty.map(|r| Box::new(r)),
        },
    )(input)
}

/// Parse a gamma node, consuming all whitespace before it
pub fn parse_gamma(input: &str) -> IResult<&str, Gamma> {
    map(
        preceded(preceded(whitespace, tag("#match")), parse_pattern_matches),
        |branches| Gamma { branches },
    )(input)
}

/// Parse a set of pattern matches
pub fn parse_pattern_matches(input: &str) -> IResult<&str, Vec<(Pattern, Expr)>> {
    delimited(
        preceded(whitespace, tag("{")),
        separated_list(preceded(whitespace, tag(",")), parse_pattern_match),
        preceded(delimited(whitespace, opt(tag(",")), whitespace), tag("}")),
    )(input)
}

/// Parse a pattern match
pub fn parse_pattern_match(input: &str) -> IResult<&str, (Pattern, Expr)> {
    separated_pair(parse_pattern, preceded(whitespace, tag("=>")), parse_expr)(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! assert_parses {
        (make $parser:expr, $string:expr, $correct:expr, $tail:expr) => {{
            let parser = $parser;
            let string: &str = $string;
            let correct: Option<_> = $correct;
            let tail: Option<&str> = $tail;
            let (rest, parsed) = match parser(string) {
                Ok(result) => result,
                Err(err) => panic!("Error {:?} parsing input string {:?}", err, string),
            };
            if let Some(correct) = correct {
                assert_eq!(parsed, correct, "Input parses wrong!");
            }
            if let Some(tail) = tail {
                assert_eq!(rest, tail, "Invalid tail on input!");
            }
            let displayed = format!("{}", parsed);
            let (rest, d_parsed) = match parser(&displayed) {
                Ok(result) => result,
                Err(err) => panic!(
                    "Error {:?} parsing display string {:?} (input = {:?})",
                    err, displayed, string
                ),
            };
            assert_eq!(
                d_parsed, parsed,
                "Display output parses to the wrong result! (input = {:?}, displayed = {:?})",
                string, displayed
            );
            assert_eq!(
                rest, "",
                "Unparsed display output (input = {:?}, displayed = {:?})!",
                string, displayed
            );
            assert_eq!(displayed, format!("{}", d_parsed));
        }};
        ($parser:expr, $string:expr, $correct:expr, $tail:expr) => {
            assert_parses!(make $parser, $string, Some($correct), $tail)
        };
        ($parser:expr, $string:expr, $correct:expr) => {
            assert_parses!(make $parser, $string, Some($correct), Some(""))
        };
        ($parser:expr, $string:expr) => {
            assert_parses!(make $parser, $string, None, Some(""))
        };
    }

    #[test]
    fn idents_parse_properly() {
        assert_parses!(parse_ident, "hello world", "hello", Some(" world"));
        assert_parses!(parse_ident, "h3110 w0rld", "h3110", Some(" w0rld"));
        assert_parses!(parse_ident, "helloworld", "helloworld");
        assert_parses!(parse_ident, "hello() world", "hello", Some("() world"));
        assert!(parse_ident(" helloworld").is_err());
        assert!(parse_ident(".helloworld").is_err());
        assert!(parse_ident(".12345").is_err());
        assert!(parse_ident("").is_err());
    }

    #[test]
    fn nested_exprs_dont_merge_improperly() {
        assert_eq!(
            &(format!("{:?}", parse_expr("a (b c) (d e)").unwrap())),
            "(\"\", (a (b c) (d e)))"
        );
    }

    #[test]
    fn paths_parse_properly() {
        assert_parses!(
            parse_path,
            "hello world",
            Path::ident("hello"),
            Some(" world")
        );
        assert_parses!(
            parse_path,
            "hello.world",
            Path::from(vec!["hello", "world"])
        );
        assert_parses!(parse_path, "hello.", Path::ident("hello"), Some("."));
        assert_parses!(
            parse_path,
            "h3110.w0rld",
            Path::from(vec!["h3110", "w0rld"])
        );
        assert!(parse_path(" helloworld").is_err());
        assert!(parse_path(".helloworld").is_err());
        assert!(parse_path(".12345").is_err());
        assert!(parse_path("").is_err());
    }

    #[test]
    fn atoms_parse_properly() {
        assert_parses!(parse_atom, "x y", Expr::ident("x"), Some(" y"));
        assert_parses!(parse_atom, "x.y", Expr::Path(Path::from(vec!["x", "y"])));
        assert_parses!(parse_atom, "(x) y", Expr::ident("x"), Some(" y"));
        assert_parses!(
            parse_atom,
            "(x.y) z",
            Expr::Path(Path::from(vec!["x", "y"])),
            Some(" z")
        );
    }

    #[test]
    fn simple_exprs_parse_properly() {
        assert_parses!(
            parse_expr,
            "(x y)",
            Expr::Sexpr(vec![Expr::ident("y").into(), Expr::ident("x").into()].into())
        );
        assert_parses!(parse_expr, "(x.y)", Expr::Path(Path::from(vec!["x", "y"])));
        assert_parses!(
            parse_expr,
            "((x) y)",
            Expr::Sexpr(vec![Expr::ident("y").into(), Expr::ident("x").into()].into())
        );
        assert_parses!(
            parse_expr,
            "((x.y) z)",
            Expr::Sexpr(
                vec![
                    Expr::ident("z").into(),
                    Expr::Path(Path::from(vec!["x", "y"])).into()
                ]
                .into()
            )
        );
    }

    #[test]
    fn nested_exprs_parse_properly() {
        let yz = Expr::Sexpr(vec![Expr::ident("z").into(), Expr::ident("y").into()].into());
        let xyz = Expr::Sexpr(
            vec![
                Expr::Sexpr(vec![Expr::ident("z").into(), Expr::ident("y").into()].into()).into(),
                Expr::ident("x").into(),
            ]
            .into(),
        );
        assert_parses!(
            parse_expr,
            "(x (y z) (x (y z)) ((y z) w))",
            Expr::Sexpr(
                vec![
                    Expr::Sexpr(vec![Expr::ident("w").into(), yz.clone()].into()).into(),
                    xyz,
                    yz.clone(),
                    Expr::ident("x").into()
                ]
                .into()
            )
        )
    }

    #[test]
    fn simple_let_statements_parse_properly() {
        let statements = [
            "let x = y;",
            "let x = x.y;",
            "let hello = (world y) z;",
            "let z = 43 (54 2);",
        ];
        for statement in statements.iter() {
            assert_parses!(parse_statement, statement)
        }
    }

    #[test]
    fn simple_scopes_parse_properly() {
        let scopes = [
            "{}",
            "{ /* my variable x*/ x }",
            "{ let x = y; x }",
            "{ let hello = (world y) z; let x = world hello; hello x }",
        ];
        for scope in scopes.iter() {
            assert_parses!(parse_scope, scope)
        }
    }

    #[test]
    fn lambda_arguments_parse_properly() {
        let args = ["|x : A|", "|x : A|", "|y : B, z : C|", "|world: F, y: C|"];
        for arg in args.iter() {
            assert!(parse_typed_args(arg).is_ok(), "Failed to parse {}", arg)
        }
    }

    #[test]
    fn type_arrows_parse_properly() {
        let args = ["=> x"];
        for arg in args.iter() {
            match parse_type_arrow(arg) {
                Ok(_) => {}
                Err(err) => panic!("Failed to parse {:?}, got {:?}", arg, err),
            }
        }
    }

    #[test]
    fn simple_functions_parse_properly() {
        let fns = [
            "#lambda |x : A| {}",
            "#lambda |x : A| x",
            "#lambda |_ : A| x",
            "#lambda |x : A| /*some comment*/ x",
            "#lambda |x : A| { /* my variable x*/ x }",
            "#lambda |y : B, z : C| { let x = y; x }",
            "#lambda |world: F, y: C| { let hello = (world y) z; let x = world hello; hello x }",
            "#lambda |world: F, y: C| => T { let hello = (world y) z; let x = world hello; hello x }"
        ];
        for func in fns.iter() {
            assert_parses!(parse_lambda, func)
        }
    }

    #[test]
    fn phi_nodes_parse_properly() {
        let phis = [
            "#phi { let f = #lambda |x : A| { g x }; let g = #lambda |x : A| { f x }; }",
            "#phi { let x = 6; let y = 43; let z = 341; }",
        ];
        for phi in phis.iter() {
            assert_parses!(parse_phi, phi)
        }
    }

    #[test]
    fn gamma_nodes_parse_properly() {
        let gammas = ["#match {}", "#match { x => y }"];
        for gamma in gammas.iter() {
            assert_parses!(parse_gamma, gamma)
        }
    }

    #[test]
    fn tuples_parse_properly() {
        let tuples = [
            "[]",
            "[ #true ]",
            "[#true]",
            "[ #true]",
            "[#true ]",
            "[ #true #false  ]",
            "[#true #true #false]",
            "[[] [] #true]",
        ];
        for tuple in tuples.iter() {
            assert_parses!(parse_tuple, tuple)
        }
    }

    #[test]
    fn universes_parse_properly() {
        let n: Option<usize> = None; //TODO: fix this...
        let universes = [
            ("#universe(0, 0)", Universe::finite()),
            ("#finite_type", Universe::finite()),
            ("#type", Universe::simple()),
            ("#universe", Universe::try_new(n, n).unwrap()),
            ("#universe(5)", Universe::try_new(Some(5), n).unwrap()),
            (
                "#universe(5, 3)",
                Universe::try_new(Some(5), Some(3)).unwrap(),
            ),
        ];
        for (s, universe) in universes.iter() {
            assert_parses!(parse_universe, s, *universe);
            assert_parses!(parse_expr, s, Expr::Universe(*universe));
        }
    }

    #[test]
    fn small_nats_parse_properly() {
        let integers = [
            ("53", 53),
            ("0x53", 0x53),
            ("0o4242", 0o4242),
            ("0b1011", 0b1011),
        ];
        for (input, result) in integers.iter() {
            assert_eq!(parse_small_nat(input).unwrap().1, *result);
            assert_eq!(parse_expr(input).unwrap().1, Expr::SmallNat(*result));
        }
    }

    #[test]
    fn finite_types_parse_properly() {
        let finite = [
            ("#finite(32)", Finite(32)),
            ("#finite(0x45a)", Finite(0x45a)),
            ("#finite(0o452)", Finite(0o452)),
            ("#finite(0b110110)", Finite(0b110110))
        ];
        for (input, result) in finite.iter() {
            assert_eq!(parse_finite(input).unwrap().1, *result);
            assert_eq!(parse_expr(input).unwrap().1, Expr::Finite(*result));
        }
    }

    #[test]
    fn finite_indices_parse_properly() {
        let indices = [
            ("#ix(0, 1)", Index::try_new(Finite(1), 0).unwrap()),
            ("#ix(1, 2)", Index::try_new(Finite(2), 1).unwrap()),
            ("#ix(5, 7)", Index::try_new(Finite(7), 5).unwrap()),
            ("#ix(0x5, 0xa)", Index::try_new(Finite(10), 5).unwrap())
        ];
        for (input, result) in indices.iter() {
            assert_eq!(parse_index(input).unwrap().1, *result);
            assert_eq!(parse_expr(input).unwrap().1, Expr::Index(*result));
        }
        let bad_indices = [
            "#ix(1, 1)",
            "#ix(7, 3)",
            "#ix(0, 0)"
        ];
        for input in bad_indices.iter() {
            assert!(parse_index(input).is_err());
            assert!(parse_expr(input).is_err());
        }
    }
}
