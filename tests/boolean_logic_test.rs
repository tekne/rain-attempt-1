/*!
Test the implementation of boolean logic in `rain`
 */

use rain_lang::parser::builder::Builder;
use rain_lang::value::ValId;

#[test]
fn basic_logical_operations() {
    let mut builder = Builder::<&str>::new();
    let mux_program = "
        let mux = #lambda |s: #bool, h: #bool, l: #bool| (#or (#and s h) (#and (#not s) l));
        let pi1 = #lambda |s: (#product #bool #bool)| (s #ix(0, 2));
        let pi2 = #lambda |s: (#product #bool #bool)| (s #ix(1, 2));
        let mux4 = #lambda |s: (#product #bool #bool), zero: #bool, one: #bool, two: #bool, three: #bool| {
            let s_lo = pi1 s;
            let s_hi = pi2 s;
            let one_or_three = mux s_hi three one;
            let zero_or_two = mux s_hi two zero;
            mux s_lo one_or_three zero_or_two
        };
    ";
    builder
        .parse_statements(mux_program)
        .expect("This is a valid program");
    let mux_tests = [
        ("mux #false #false #false", false),
        ("mux #false #false #true", true),
        ("mux #false #true #false", false),
        ("mux #false #true #true", true),
        ("mux #true #false #false", false),
        ("mux #true #false #true", false),
        ("mux #true #true #false", true),
        ("mux #true #true #true", true),
        ("pi1 [#false #false]", false),
        ("pi1 [#false #true]", false),
        ("pi1 [#true #false]", true),
        ("pi1 [#true #true]", true),
        ("pi2 [#false #false]", false),
        ("pi2 [#false #true]", true),
        ("pi2 [#true #false]", false),
        ("pi2 [#true #true]", true),
        ("mux4 [#false #true] #false #false #true #false", true),
        ("mux4 [#true #true] #false #false #true #false", false),
    ];
    for (expr, value) in mux_tests.iter() {
        let desired_result = ValId::from(*value);
        assert_eq!(
            builder
                .parse_expr(expr)
                .expect("This is a valid expression"),
            ("", desired_result),
            "Expected {} == {}",
            expr,
            value
        );
    }
}
