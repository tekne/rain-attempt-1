# Bug Notes

This document contains a list of potentially buggy behaviour to address, so I don't forget it.

## Hash-consing and deduplication
- Check how deduplication interacts with regions: deduplicating a node with an artificially high region can introduce region errors into the rain-graph
